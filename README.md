# Cinderella
============================

### Required

- `composer`
- `node.js` -> `>= 7.4.0`
- `nodemon` -> for launch socket.io servers. installation `npm install -g nodemon`. usage example `nodemon app.js`

### Components

- `./socket` -> socket.io server for online statuses and chat

### Configs

- `params-local.php`
- `db-local.php`
- `web-local.php`