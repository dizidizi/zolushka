<?php
/**
 * Created by PhpStorm.
 * User: JimmDiGriz
 * Date: 3/18/2017
 * Time: 4:47 PM
 */

namespace app\assets;


use yii\web\AssetBundle;

class AdminAsset extends AssetBundle
{
    public $basePath = '@webroot';
    public $baseUrl = '@web';
    
    public $css = [
        'frontend/build/css/lib/bootstrap/bootstrap.min.css',
        'frontend/css/lib/lobipanel/lobipanel.min.css',
        'frontend/css/separate/vendor/lobipanel.min.css',
        'frontend/css/lib/jqueryui/jquery-ui.min.css',
        'frontend/css/separate/pages/widgets.min.css',
        'frontend/css/separate/vendor/tags_editor.min.css',
        'frontend/css/separate/vendor/bootstrap-select/bootstrap-select.min.css',
        'frontend/css/separate/vendor/select2.min.css',
        'frontend/css/separate/pages/profile-2.min.css',
        'frontend/css/lib/font-awesome/font-awesome.min.css',
        'frontend/build/css/separate/main.min.css',
		'frontend/build/css/separate/pages/users-list.min.css',
        'css/vendor/jquery.cropbox.min.css',
		'frontend/css/lib/ion-range-slider/ion.rangeSlider.css',
		'frontend/css/lib/ion-range-slider/ion.rangeSlider.skinHTML5.css',
		'frontend/build/css/lib/swipebox.min.css',
		'frontend/build/css/lib/medium-editor/medium-editor.min.css',
		'frontend/build/css/lib/medium-editor/default.min.css',

		// Elements
		'frontend/build/css/separate/elements/user-gallery.min.css',
		'frontend/build/css/separate/elements/chat.min.css',

		//Start UI
		'frontend/build/css/separate/pages/others.min.css',

    ];

    public $js = [
        //'frontend/js/lib/jquery/jquery-3.2.0.min.js',

		/* vendor libs: */

        'frontend/js/lib/tether/tether.min.js',
        'frontend/js/lib/bootstrap/bootstrap.min.js',
        'frontend/build/js/plugins.js',
        'frontend/js/lib/jqueryui/jquery-ui.min.js',
        'frontend/js/lib/jquery-tag-editor/jquery.caret.min.js',
        'frontend/js/lib/lobipanel/lobipanel.min.js',
        'frontend/js/lib/bootstrap-select/bootstrap-select.min.js',
        'frontend/js/lib/match-height/jquery.matchHeight.min.js',
        'frontend/js/lib/select2/select2.full.min.js',
		'frontend/js/lib/ion-range-slider/ion.rangeSlider.js',
		'frontend/build/js/lib/medium-editor.min.js',

		/* Other vendor  */

        'js/vendor/jquery.cropbox.min.js',
        //'https://cdnjs.cloudflare.com/ajax/libs/require.js/2.3.3/require.min.js',
        'js/vendor/vue.min.js',
        'https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js',
        'https://oss.maxcdn.com/respond/1.4.2/respond.min.js',
		//'js/vendor/vue-upload/demo-src.js',
		'frontend/build/js/lib/swipebox/jquery.swipebox.min.js',


		'js/app/users-list.js',
		'js/editFormHandler.js',


		'frontend/build/js/app.js',
		'frontend/VUE/dev/bundle.js'
		//'frontend/VUE/build/dev.js'
		
    ];
    
    public $depends = [
//        'omnilight\assets\VueJsAsset',
        'yii\web\YiiAsset',
//        'yii\bootstrap\BootstrapAsset',
    ];
}