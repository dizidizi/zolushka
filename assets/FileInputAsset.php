<?php
/**
 * Created by PhpStorm.
 * User: JimmDiGriz
 * Date: 3/26/2017
 * Time: 9:28 PM
 */

namespace app\assets;


use yii\web\AssetBundle;

class FileInputAsset extends AssetBundle
{
    public $sourcePath = '@vendor/kartik-v/bootstrap-fileinput';
    
    public $css = [
        'css/fileinput.css',
    ];
    public $js = [
        'js/fileinput.js',
    ];

    public $depends = [
    'app\assets\FrontendAsset',
    //        'yii\web\YiiAsset',
    //        'yii\bootstrap\BootstrapAsset',
    ];
}