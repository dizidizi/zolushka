<?php
/**
 * Created by PhpStorm.
 * User: JimmDiGriz
 * Date: 3/19/2017
 * Time: 11:40 PM
 */

namespace app\assets;


use yii\web\AssetBundle;

class FrontendAsset extends AssetBundle
{
    public $basePath = '@webroot';
    public $baseUrl = '@web';

    public $css = [
        'frontend/css/separate/vendor/fancybox.min.css',
        'frontend/css/separate/pages/profile-2.min.css',
        'frontend/css/separate/elements/ribbons.min.css',
        'frontend/css/separate/pages/user.min.css',
//        '@vendor/kartik-v/bootstrap-fileinput/css/fileinput.css',
    ];

    public $js = [
        'frontend/js/lib/salvattore/salvattore.min.js',
        'frontend/js/lib/ion-range-slider/ion.rangeSlider.js',
        'frontend/js/lib/fancybox/jquery.fancybox.pack.js',

//        '@vendor/kartik-v/bootstrap-fileinput/js/fileinput.js',
        'js/gallery-temp.js',
    ];

    public $depends = [
        'app\assets\AdminAsset',
//        'yii\web\YiiAsset',
//        'yii\bootstrap\BootstrapAsset',
    ];
}