<?php
/**
 * Created by PhpStorm.
 * User: JimmDiGriz
 * Date: 3/17/2017
 * Time: 9:03 PM
 */

namespace app\commands;


use app\core\components\AvatarManager;
use app\modules\users\models\User;
use yii\console\Controller;
use yii\data\ActiveDataProvider;
use yii\helpers\VarDumper;

class AvatarsController extends Controller
{
    public function actionIndex()
    {
        foreach (User::find()->all() as $user) {
            echo $user->id . "\n";
            
            $user->avatar_src = AvatarManager::getAvatarForUser($user);

            $user->setScenario(User::SCENARIO_AVATAR_UPLOAD);

            $user->save();
        }
    }
}