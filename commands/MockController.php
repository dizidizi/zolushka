<?php
/**
 * Created by PhpStorm.
 * User: JimmDiGriz
 * Date: 3/17/2017
 * Time: 9:03 PM
 */

namespace app\commands;

use app\modules\geo\models\Country;
use app\modules\geo\models\Region;
use app\modules\users\models\User;
use app\modules\users\models\UserPurposes;
use app\modules\users\models\UserSexTypes;
use app\modules\users\values\Complexion;
use app\modules\users\values\EyesColor;
use app\modules\users\values\Gender;
use app\modules\users\values\HairColor;
use app\modules\users\values\Look;
use app\modules\users\values\Purposes;
use app\modules\users\values\SexPeriodicity;
use app\modules\users\values\SexRole;
use app\modules\users\values\SexTypes;
use app\modules\users\values\Zodiac;
use app\rbac\Roles;
use yii\console\Controller;

class MockController extends Controller
{
    public function actionFill()
    {
        $femaleName = 'Random Alice';
        $maleName = 'Random John';
        $emailSuffix = '@mail.ru';
        $password = '123456';
        
        $user = User::find()->orderById()->one();
        
        $lastId = 1; //?
        
        if ($user) {
            $lastId = $user->id;
        }
        
        for ($i = 1; $i <= 1000; $i++) {
            $lastId++;
            
            $user = new User(['scenario' => User::SCENARIO_REGISTER]);
            
            $user->age = mt_rand(20, 40);
            $user->email = "{$lastId}{$emailSuffix}";
            $user->password = $password;
            
            $user->gender = mt_rand(0, 1) == 0 ? Gender::FEMALE : Gender::MALE;
            
            $user->name = $user->gender == Gender::FEMALE ? $femaleName : $maleName;
            
            if ($user->gender == Gender::FEMALE) {
                $user->bust_size = mt_rand(1, 5);
            }
            
            $user->height = mt_rand(160, 190);
            $user->weight = mt_rand(40, 100);
            
            $user->hair_color = HairColor::$list[mt_rand(0, count(HairColor::$list) - 1)];
            $user->eyes_color = EyesColor::$list[mt_rand(0, count(EyesColor::$list) - 1)];
            $user->zodiac = Zodiac::$list[mt_rand(0, count(Zodiac::$list) - 1)];
            $user->sex_role = SexRole::$list[mt_rand(0, count(SexRole::$list) - 1)];
            $user->sex_periodicity = SexPeriodicity::$list[mt_rand(0, count(SexPeriodicity::$list) - 1)];
            $user->complexion = Complexion::$list[mt_rand(0, count(Complexion::$list) - 1)];
            $user->look = Look::$list[mt_rand(0, count(Look::$list) - 1)];

            if (!$user->save()) {
                echo "UserNotSave\n";
                continue;
            }
            
            $purposesCount = mt_rand(1, count(Purposes::$list));
            $sexTypesCount = mt_rand(1, count(SexTypes::$list));
            
            for ($i = 0; $i < $purposesCount; $i++) {
                $purpose = new UserPurposes();
                
                $purpose->setScenario(UserPurposes::SCENARIO_DEFAULT);
                
                $purpose->user_id = $user->id;
                $purpose->purpose = Purposes::$list[mt_rand(0, count(Purposes::$list) - 1)];
                
                $purpose->save();
            }
            
            for ($i = 0; $i < $sexTypesCount; $i++) {
                $type = new UserSexTypes();

                $type->setScenario(UserSexTypes::SCENARIO_DEFAULT);

                $type->user_id = $user->id;
                $type->type = SexTypes::$list[mt_rand(0, count(SexTypes::$list) - 1)];

                $type->save();
            }
        }
    }
    
    public function actionCreateAdmin()
    {
        $user = new User(['scenario' => User::SCENARIO_REGISTER]);
        
        $user->name = 'admin';
        $user->password = '123456';
        $user->email = 'admin@admin.ru';
        $user->gender = Gender::MALE;
        $user->age = 25;
        $user->group = Roles::ADMIN;
        
        $user->save();
    }
    
    public function actionGeo()
    {
        if (Country::find()->count() != 0) {
            return;
        }
        
        $country = new Country();
        
        $country->name = 'Russia';
        
        $country->save();
        
        $this->addRegionsToCountry(['Moscow', 'Saint Petersburg', 'Ivanovo', 'Vladimir'], $country->id);

        $country = new Country();

        $country->name = 'Ukraine';

        $country->save();

        $this->addRegionsToCountry(['Kiev', 'Lviv', 'Kharkiv', 'Odessa'], $country->id);
    }
    
    /**
     * @param array $regions
     * @param int $countryId
     */
    private function addRegionsToCountry($regions, $countryId)
    {
        foreach ($regions as $name) {
            $region = new Region();

            $region->country_id = $countryId;
            $region->name = $name;

            $region->save();
        }
    }
}