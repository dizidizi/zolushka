<?php
/**
 * Created by PhpStorm.
 * User: JimmDiGriz
 * Date: 16.03.2017
 * Time: 14:39
 */

namespace app\commands;

use app\rbac\Permissions;
use app\rbac\Roles;
use app\rbac\UserGroupRule;
use yii\console\Controller;

class RbacController extends Controller
{
    public function actionInit()
    {
        $authManager = \Yii::$app->authManager;

        $authManager->removeAll();

        /**Create roles*/
        $user = $authManager->createRole(Roles::USER);
        $redactor = $authManager->createRole(Roles::REDACTOR);
        $moderator = $authManager->createRole(Roles::MODERATOR);
        $admin = $authManager->createRole(Roles::ADMIN);

        $banned = $authManager->createRole(Roles::BANNED);

        /**Create simple permissions*/
        $login = $authManager->createPermission(Permissions::LOGIN);
//        $view = $authManager->createPermission(Permissions::VIEW);
//        $update = $authManager->createPermission(Permissions::UPDATE);
//        $delete = $authManager->createPermission(Permissions::DELETE);

        $adminAccess = $authManager->createPermission(Permissions::ADMIN_ACCESS);


        /**Add permissions*/
        $authManager->add($login);
//        $authManager->add($view);
//        $authManager->add($update);
//        $authManager->add($delete);
        $authManager->add($adminAccess);

        /**Add group rule*/
        $userGroupRule = new UserGroupRule();
        $authManager->add($userGroupRule);

        $user->ruleName = $userGroupRule->name;
        $redactor->ruleName = $userGroupRule->name;
        $moderator->ruleName = $userGroupRule->name;
        $admin->ruleName = $userGroupRule->name;
        $banned->ruleName = $userGroupRule->name;

        $authManager->add($user);
        $authManager->add($redactor);
        $authManager->add($moderator);
        $authManager->add($admin);
        $authManager->add($banned);

        /**Add permissions*/
        $authManager->addChild($user, $login);

        $authManager->addChild($redactor, $user);
        $authManager->addChild($moderator, $redactor);

        $authManager->addChild($admin, $redactor);
        $authManager->addChild($admin, $adminAccess);
    }
}