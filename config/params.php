<?php

return [
    'adminEmail' => 'admin@example.com',
    'salt' => 'asvburbfcguwcrtgn8synercvixcvbiiwue45',
    
    'default_male_avatar' => '/frontend/img/avatar-1-298.jpg',
    'default_female_avatar' => '/frontend/img/avatar-3-298.jpg',
    
    'default_cover' => '/frontend/img/profile-bg.jpg',
];
