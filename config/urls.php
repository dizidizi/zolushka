<?php
/**
 * Created by PhpStorm.
 * User: JimmDiGriz
 * Date: 16.03.2017
 * Time: 13:23
 */

return [
    'class' => 'yii\web\UrlManager',
    'showScriptName' => false,
    'enablePrettyUrl' => true,
//    'enableStrictParsing' => true,
    'rules' => [
        '<controller:\w+>/<id:\d+>' => '<controller>/view',
        '<controller:\w+>/<action:\w+>/<id:\d+>' => '<controller>/<action>',
        '<controller:\w+>/<action:\w+>' => '<controller>/<action>',

        /**@todo: fix alises prefix*/
//        'gii' => 'zolushka-yii/basic/web/gii',
//        'gii/<alias:\w+>' => 'zolushka-yii/basic/web/gii/<alias>',

//        ['class' => 'yii\rest\UrlRule', 'controller' => 'users/api'],
    ],
];
