<?php

$params = require(__DIR__ . '/params.php');

$paramsLocal = require(__DIR__ . '/params-local.php');
$webLocal = require(__DIR__ . '/web-local.php');

$config = \yii\helpers\ArrayHelper::merge([
    'id' => 'cinderella',
    'basePath' => dirname(__DIR__),
    'bootstrap' => ['log'],
    'components' => [
        'request' => [
            // !!! insert a secret key in the following (if it is empty) - this is required by cookie validation
            'cookieValidationKey' => 'K1kqRpXGM9dnsvN20kKmHoUGYGyiRvXr',
            'parsers' => [
                'application/json' => 'yii\web\JsonParser',
            ]
//            'baseUrl' => 'zolushka-yii/basic/web',
        ],
        'cache' => [
            /**@todo: use redis cache in future*/
            'class' => 'yii\caching\FileCache',
        ],
        'user' => [
            'class' => 'app\core\WebUser',
            'identityClass' => 'app\modules\users\models\User',
            'enableAutoLogin' => true,
        ],
        'errorHandler' => [
            'errorAction' => 'site/error',
        ],
        'mailer' => [
            'class' => 'yii\swiftmailer\Mailer',
            // send all mails to a file by default. You have to set
            // 'useFileTransport' to false and configure a transport
            // for the mailer to send real emails.
            'useFileTransport' => true,
        ],
        'log' => [
            'traceLevel' => YII_DEBUG ? 3 : 0,
            'targets' => [
                [
                    'class' => 'yii\log\FileTarget',
                    'levels' => ['error', 'warning'],
                ],
            ],
        ],
        'authManager' => [
            'class' => 'yii\rbac\PhpManager',
            'defaultRoles' => ['user', 'redactor', 'moderator', 'admin'],
            'itemFile' => '@app/rbac/items.php',
            'assignmentFile' => '@app/rbac/assignments.php',
            'ruleFile' => '@app/rbac/rules.php'
        ],
        'i18n' => [
            'translations' => [
                'app*' => [
                    'class' => 'yii\i18n\PhpMessageSource',
                    'basePath' => '@app/messages',
                    'sourceLanguage' => 'en-US',
                    'forceTranslation' => true,
                    'fileMap' => [
                        'app' => 'main.php'
                    ],
                ],
            ],
        ],
        'db' => require(__DIR__ . '/db.php'),
        'urlManager' => require(__DIR__ . '/urls.php'),
    ],
    'modules' => [
        'admin' => [
            'class' => 'app\modules\admin\AdminModule',
        ],
        'users' => [
            'class' => 'app\modules\users\UsersModule'
        ],
        'photos' => [
            'class' => 'app\modules\photos\PhotosModule',
        ],
        'geo' => [
            'class' => 'app\modules\geo\GeoModule',
        ],
        'chat' => [
            'class' => 'app\modules\chat\ChatModule',
        ],
    ],
    'params' => \yii\helpers\ArrayHelper::merge($params, $paramsLocal),
], $webLocal);

if (YII_ENV_DEV) {
    // configuration adjustments for 'dev' environment
    $config['bootstrap'][] = 'debug';
    $config['modules']['debug'] = [
        'class' => 'yii\debug\Module',
        // uncomment the following to add your IP if you are not connecting from localhost.
        'allowedIPs' => ['*.*.*.*'],
    ];

    $config['bootstrap'][] = 'gii';
    $config['modules']['gii'] = [
        'class' => 'yii\gii\Module',
        // uncomment the following to add your IP if you are not connecting from localhost.
        'allowedIPs' => ['176.110.178.2', '46.160.34.46', '127.0.0.1'],
    ];
}

return $config;
