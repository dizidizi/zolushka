<?php
/**
 * Created by PhpStorm.
 * User: JimmDiGriz
 * Date: 3/20/2017
 * Time: 9:38 PM
 */

namespace app\core;


use app\rbac\Roles;
use yii\web\User;

class WebUser extends User
{
    /**
     * @param string $role
     * 
     * @return bool
     */
    public function is($role)
    {
        if (!\Yii::$app->user->isGuest) {
            if ($role == Roles::ADMIN) {
                return $this->isAdmin();
            }

            if ($role == Roles::MODERATOR) {
                return $this->isModerator();
            }

            if ($role == Roles::REDACTOR) {
                return $this->isRedactor();
            }

            if ($role == Roles::USER) {
                return $this->isUser();
            }
        }

        /**guests can nothing*/
        return false;
    }

    /**
     * @return bool
     */
    protected function isAdmin()
    {
        return $this->identity->group == Roles::ADMIN;
    }

    /**
     * @return bool
     */
    protected function isModerator()
    {
        return $this->isAdmin() || $this->identity->group == Roles::MODERATOR;
    }

    /**
     * @return bool
     */
    protected function isRedactor()
    {
        return $this->isModerator() || $this->identity->group == Roles::REDACTOR;
    }

    /**
     * @return bool
     */
    protected function isUser()
    {
        return $this->isRedactor() || $this->identity->group == Roles::USER;
    }
}