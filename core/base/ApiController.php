<?php
/**
 * Created by PhpStorm.
 * User: JimmDiGriz
 * Date: 3/20/2017
 * Time: 9:17 PM
 */

namespace app\core\base;


use yii\base\InvalidParamException;
use yii\rest\ActiveController;

class ApiController extends ActiveController
{
    const PER_PAGE = 20;
    
    /**
     * @param array $required
     * @param bool $throws
     * 
     * @return array
     */
    protected function getParams($required, $throws = true)
    {
        $method = "get";
        
        if (\Yii::$app->request->method != "GET") {
            $method = "post";
        }
        
        $params = [];
        $missing = [];
        
        foreach ($required as $key => $strong) {
            $value = \Yii::$app->request->{$method}($key, false);
            
            if (!$value && $strong) {
                $missing[] = $key;
                continue;
            } else if (!$value) {
                continue;
            }
            
            $params[$key] = $value;
        }
        
        if ($throws && count($missing) > 0) {
            throw new InvalidParamException('Missing Params: ' . implode(',', $missing));
        }
        
        return $params;
    }
}