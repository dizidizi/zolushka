<?php
/**
 * Created by PhpStorm.
 * User: JimmDiGriz
 * Date: 3/17/2017
 * Time: 2:30 AM
 */

namespace app\core\base;


use yii\db\ActiveRecord;

/**
 * Base class for future models.
 * May contains every global extensions.
 */
class CinderellaModel extends ActiveRecord
{
    
}