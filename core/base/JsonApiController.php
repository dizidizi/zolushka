<?php
/**
 * Created by PhpStorm.
 * User: JimmDiGriz
 * Date: 3/17/2017
 * Time: 10:38 PM
 */

namespace app\core\base;


use yii\filters\ContentNegotiator;
use yii\rest\ActiveController;
use yii\web\Response;

abstract class JsonApiController extends ApiController
{
    public function behaviors()
    {
        return [
            [
                'class' => ContentNegotiator::className(),
//                'only' => ['index', 'view'],
                'formats' => [
                    'application/json' => Response::FORMAT_JSON,
                ],
            ],
        ];
    }
}