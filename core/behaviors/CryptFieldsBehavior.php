<?php
/**
 * Created by PhpStorm.
 * User: JimmDiGriz
 * Date: 17.03.2017
 * Time: 10:38
 */

namespace app\core\behaviors;

use yii\base\Behavior;
use yii\db\ActiveRecord;
use yii\log\Logger;

class CryptFieldsBehavior extends Behavior
{
    public $fields;
    public $salt;

    public function events()
    {
        return [
            ActiveRecord::EVENT_BEFORE_INSERT => 'beforeSave',
        ];
    }

    public function beforeSave($event)
    {
        if ($this->owner->isNewRecord) {
            $this->cryptFields();
        }
    }

    public function cryptFields()
    {
//        \Yii::getLogger()->log('CryptFields call', Logger::LEVEL_INFO);
        if (!is_array($this->fields)) {
            return;
        }

        foreach ($this->fields as $field) {
            $this->owner->{$field} = crypt($this->owner->{$field}, $this->salt);
        }
    }
}