<?php
/**
 * Created by PhpStorm.
 * User: JimmDiGriz
 * Date: 3/20/2017
 * Time: 1:32 AM
 */

namespace app\core\components;


class AvatarManager
{
    /**
     * @param \app\modules\users\models\User $user
     * 
     * @return string
     */
    public static function getAvatarForUser($user)
    {
        $avatarSrc = $user->getAvatarSrc();
        
        if ($avatarSrc) {
            return $avatarSrc;
        }
        
        if ($user->isMale()) {
            return \Yii::$app->params['default_male_avatar'];
        } else {
            return \Yii::$app->params['default_female_avatar'];
        }
    }

    /**
     * @param \app\modules\users\models\User $user
     *
     * @return string
     */
    public static function getCoverForUser($user)
    {
        $coverSrc = $user->getCoverSrc();

        if ($coverSrc) {
            return $coverSrc;
        }
        
        return \Yii::$app->params['default_cover'];
    }
}