<?php
/**
 * Created by PhpStorm.
 * User: JimmDiGriz
 * Date: 17.03.2017
 * Time: 12:31
 */

return [
    /**zodiac*/
    'twins' => 'близнецы',
    'goat-horned' => 'козерог',
    'ram' => 'овен',
    'bull' => 'телец',
    'crab' => 'рак',
    'lion' => 'лев',
    'maiden' => 'дева',
    'scales' => 'весы',
    'scorpion' => 'скорпион',
    'archer' => 'стрелец',
    'water-bearer' => 'водолей',
    'fishes' => 'рыбы',

    /**user fields*/
    'email' => 'Email',
    'password' => 'Пароль',
    'group' => 'Группа',
    'name' => 'Имя',
    'age' => 'Возраст',
    'gender' => 'Пол',
    'zodiac' => 'Знак Зодиака',
    'look' => 'Внешность',
    'purpose' => 'Цель',
    'preferences' => 'Предпочтения',
    'country' => 'Страна',
    'region' => 'Регион',
    'information' => 'Информация',
    'height' => 'Рост',
    'weight' => 'Вес',
    'eyes_color' => 'Цвет глаз',
    'hair_color' => 'Цвет волос',
    'bust_size' => 'Размер груди',

    /**fields*/
    'rememberMe' => 'Запомнить меня',
    'Login' => 'Войти',
];