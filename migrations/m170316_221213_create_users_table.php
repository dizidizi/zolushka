<?php

use yii\db\Migration;

/**
 * Handles the creation of table `users`.
 */
class m170316_221213_create_users_table extends Migration
{
    private $table = 'users';
    
    /**
     * @inheritdoc
     */
    public function up()
    {
        $this->createTable($this->table, [
            'id' => $this->primaryKey(),
            'email' => $this->string()->notNull(),
            'password' => $this->string()->notNull(),
            'access_token' => $this->string()->null(),
            'group' => $this->string()->notNull()->defaultValue('user'),
            'name' => $this->string()->notNull(),
            'age' => $this->integer()->null(),
            'gender' => $this->integer()->notNull()->defaultValue(0),
            'zodiac' => $this->integer()->null(),
            'look' => $this->string()->null(),
            'purpose' => $this->string()->null(),
            'preferences' => $this->string()->null(),
            'country' => $this->string()->null(),
            'region' => $this->string()->null(),
            'information' => $this->text()->null(),
            'height' => $this->integer()->null(),
            'weight' => $this->integer()->null(),
            'eyes_color' => $this->string()->null(),
            'hair_color' => $this->string()->null(),
            'bust_size' => $this->integer()->null(),
            'created_at' => $this->timestamp(),
            'updated_at' => $this->timestamp(),
        ]);
        
        $this->createIndex('user_email_idx', $this->table, 'email', true);
        $this->createIndex('user_gender_idx', $this->table, 'gender');
        $this->createIndex('user_age_idx', $this->table, 'age');
        
        /**enough for now*/
    }

    /**
     * @inheritdoc
     */
    public function down()
    {
        $this->dropIndex('user_email_idx', $this->table);
        $this->dropIndex('user_gender_idx', $this->table);
        $this->dropIndex('user_age_idx', $this->table);
        
        $this->dropTable($this->table);
    }
}
