<?php

use yii\db\Migration;

class m170318_163335_alter_users_table extends Migration
{
    private $table = 'users';
    
    public function up()
    {
        $this->dropColumn($this->table, 'preferences');
        $this->dropColumn($this->table, 'purpose');
        
        $this->addColumn($this->table, 'complexion', $this->string()->null());
        $this->addColumn($this->table, 'sex_periodicity', $this->string()->null());
        $this->addColumn($this->table, 'sex_role', $this->string()->null());
        
        $this->dropColumn($this->table, 'zodiac');
        
        $this->addColumn($this->table, 'zodiac', $this->string()->null());
    }

    public function down()
    {
        $this->addColumn($this->table, 'preferences', $this->string());
        $this->addColumn($this->table, 'purpose', $this->string());

        $this->dropColumn($this->table, 'complexion');
        $this->dropColumn($this->table, 'sex_periodicity');
        $this->dropColumn($this->table, 'sex_role');

        $this->dropColumn($this->table, 'zodiac');

        $this->addColumn($this->table, 'zodiac', $this->integer()->null());
    }

    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}
