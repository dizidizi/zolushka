<?php

use yii\db\Migration;

class m170318_164306_create_user_sex_types extends Migration
{
    private $table = 'user_sex_types';
    
    public function up()
    {
        $this->createTable($this->table, [
            'user_id' => $this->integer()->notNull(),
            'type' => $this->string()->notNull(),
        ]);
        
        $this->createIndex('user_id_idx', $this->table, 'user_id');
    }

    public function down()
    {
        $this->dropIndex('user_id_idx', $this->table);
        
        $this->dropTable($this->table);
    }

    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}
