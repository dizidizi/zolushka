<?php

use yii\db\Migration;

class m170318_183339_change_gender_field_type extends Migration
{
    private $table = 'users';
    
    public function up()
    {
        $this->dropColumn($this->table, 'gender');
        
        $this->addColumn($this->table, 'gender', $this->string()->notNull());
    }

    public function down()
    {
        $this->dropColumn($this->table, 'gender');

        $this->addColumn($this->table, 'gender', $this->integer()->notNull());
    }

    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}
