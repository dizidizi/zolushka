<?php

use yii\db\Migration;

class m170319_165608_add_avatar_id_to_user extends Migration
{
    private $table = 'users';

    public function up()
    {
        $this->addColumn($this->table, 'avatar_id', $this->integer()->null());
    }

    public function down()
    {
        $this->dropColumn($this->table, 'avatar_id');
    }

    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}
