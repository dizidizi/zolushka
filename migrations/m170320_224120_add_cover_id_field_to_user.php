<?php

use yii\db\Migration;

class m170320_224120_add_cover_id_field_to_user extends Migration
{
    private $table = 'users';

    public function up()
    {
        $this->addColumn($this->table, 'cover_id', $this->integer()->null());
    }

    public function down()
    {
        $this->dropColumn($this->table, 'cover_id');
    }

    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}
