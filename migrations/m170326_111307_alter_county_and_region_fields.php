<?php

use yii\db\Migration;

class m170326_111307_alter_county_and_region_fields extends Migration
{
    private $table = 'users';

    public function up()
    {
        $this->dropColumn($this->table, 'country');
        $this->dropColumn($this->table, 'region');

        $this->addColumn($this->table, 'country_id', $this->integer()->null());
        $this->addColumn($this->table, 'region_id', $this->integer()->null());
    }

    public function down()
    {
        $this->dropColumn($this->table, 'country_id');
        $this->dropColumn($this->table, 'region_id');

        $this->addColumn($this->table, 'country', $this->string()->null());
        $this->addColumn($this->table, 'region', $this->string()->null());
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m170326_111307_alter_county_and_region_fields cannot be reverted.\n";

        return false;
    }
    */
}
