<?php

use yii\db\Migration;

/**
 * Handles the creation of table `country`.
 */
class m170326_111319_create_country_table extends Migration
{
    private $table = 'countries';

    public function up()
    {
        $this->createTable($this->table, [
            'id' => $this->primaryKey(),
            'name' => $this->string()->notNull(),
        ]);

        $this->createIndex('name_idx', $this->table, 'name');
    }

    public function down()
    {
        $this->dropIndex('name_idx', $this->table);

        $this->dropTable($this->table);
    }
}
