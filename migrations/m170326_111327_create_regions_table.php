<?php

use yii\db\Migration;

/**
 * Handles the creation of table `regions`.
 */
class m170326_111327_create_regions_table extends Migration
{
    private $table = 'regions';

    public function up()
    {
        $this->createTable($this->table, [
            'id' => $this->primaryKey(),
            'country_id' => $this->integer()->notNull(),
            'name' => $this->string()->notNull(),
        ]);

        $this->createIndex('name_idx', $this->table, 'name');
    }

    public function down()
    {
        $this->dropIndex('name_idx', $this->table);

        $this->dropTable($this->table);
    }
}
