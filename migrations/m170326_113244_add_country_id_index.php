<?php

use yii\db\Migration;

class m170326_113244_add_country_id_index extends Migration
{
    private $table = 'regions';

    public function up()
    {
        $this->createIndex('country_idx', $this->table, 'country_id');
    }

    public function down()
    {
        $this->dropIndex('country_idx', $this->table);
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m170326_113244_add_country_id_index cannot be reverted.\n";

        return false;
    }
    */
}
