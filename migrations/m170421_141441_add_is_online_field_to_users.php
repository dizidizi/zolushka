<?php

use yii\db\Migration;

class m170421_141441_add_is_online_field_to_users extends Migration
{
    private $table = 'users';

    public function up()
    {
        $this->addColumn($this->table, 'is_online', $this->boolean()->defaultValue(false));
    }

    public function down()
    {
        $this->dropColumn($this->table, 'is_online');
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m170421_141441_add_is_online_field_to_users cannot be reverted.\n";

        return false;
    }
    */
}
