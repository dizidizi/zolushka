<?php

use yii\db\Migration;

/**
 * Handles the creation of table `chats`.
 */
class m170421_141456_create_chats_table extends Migration
{
    private $table = 'chats';

    /**
     * @inheritdoc
     */
    public function up()
    {
        $this->createTable($this->table, [
            'id' => $this->primaryKey(),
            'created_at' => $this->timestamp()->notNull()->defaultValue(new \yii\db\Expression("NOW()")),
            'type' => $this->integer()->notNull(),
            'settings' => $this->text()->null()
        ]);
    }

    /**
     * @inheritdoc
     */
    public function down()
    {
        $this->dropTable($this->table);
    }
}
