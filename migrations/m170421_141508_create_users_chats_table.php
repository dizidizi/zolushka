<?php

use yii\db\Migration;

/**
 * Handles the creation of table `users_chats`.
 */
class m170421_141508_create_users_chats_table extends Migration
{
    private $table = 'users_chats';

    /**
     * @inheritdoc
     */
    public function up()
    {
        $this->createTable($this->table, [
            'user_id' => $this->integer()->notNull(),
            'chat_id' => $this->integer()->notNull(),
            'mark' => $this->string()->null(),
        ]);
    }

    /**
     * @inheritdoc
     */
    public function down()
    {
        $this->dropTable($this->table);
    }
}
