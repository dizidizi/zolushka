<?php

use yii\db\Migration;

/**
 * Handles the creation of table `chat_messages`.
 */
class m170421_141516_create_chat_messages_table extends Migration
{
    private $table = 'chat_messages';

    /**
     * @inheritdoc
     */
    public function up()
    {
        $this->createTable($this->table, [
            'user_id' => $this->integer()->notNull(),
            'chat_id' => $this->integer()->notNull(),
            'created_at' => $this->timestamp()->notNull()->defaultValue(new \yii\db\Expression("NOW()")),
            'message' => $this->text()->notNull(),
        ]);
    }

    /**
     * @inheritdoc
     */
    public function down()
    {
        $this->dropTable($this->table);
    }
}
