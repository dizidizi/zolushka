<?php

use yii\db\Migration;

/**
 * Handles the creation of table `chat_messages_attachments`.
 */
class m170421_141527_create_chat_messages_attachments_table extends Migration
{
    private $table = 'chat_message_attachments';

    /**
     * @inheritdoc
     */
    public function up()
    {
        $this->createTable($this->table, [
            'message_id' => $this->integer()->notNull(),
            'target' => $this->string()->notNull(),
            'target_id' => $this->integer()->notNull(),
        ]);
    }

    /**
     * @inheritdoc
     */
    public function down()
    {
        $this->dropTable($this->table);
    }
}
