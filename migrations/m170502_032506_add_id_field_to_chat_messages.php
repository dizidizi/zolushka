<?php

use yii\db\Migration;

class m170502_032506_add_id_field_to_chat_messages extends Migration
{
    private $table = 'chat_messages';

    public function up()
    {
        $this->addColumn($this->table, 'id', $this->primaryKey());
    }

    public function down()
    {
        $this->dropColumn($this->table, 'id');
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m170502_032506_add_id_field_to_chat_messages cannot be reverted.\n";

        return false;
    }
    */
}
