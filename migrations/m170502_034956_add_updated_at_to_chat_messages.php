<?php

use yii\db\Migration;

class m170502_034956_add_updated_at_to_chat_messages extends Migration
{
    private $table = 'chat_messages';

    public function up()
    {
        $this->addColumn($this->table, 'updated_at', $this->timestamp()->notNull()->defaultValue(new \yii\db\Expression("NOW()")));
    }

    public function down()
    {
        $this->dropColumn($this->table, 'updated_at');
    }
    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m170502_034956_add_updated_at_to_chat_messages cannot be reverted.\n";

        return false;
    }
    */
}
