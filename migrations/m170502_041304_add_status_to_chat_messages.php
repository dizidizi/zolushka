<?php

use yii\db\Migration;

class m170502_041304_add_status_to_chat_messages extends Migration
{
    private $table = 'chat_messages';

    public function up()
    {
        $this->addColumn($this->table, 'status', $this->integer()->notNull()->defaultValue(\app\modules\chat\values\MessageStatuses::UNREAD));
    }

    public function down()
    {
        $this->dropColumn($this->table, 'status');
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m170502_041304_add_status_to_chat_messages cannot be reverted.\n";

        return false;
    }
    */
}
