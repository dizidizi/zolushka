<?php

use yii\db\Migration;

class m170509_141541_add_updated_at_to_chats extends Migration
{
    private $table = 'chats';

    public function up()
    {
        $this->addColumn($this->table, 'updated_at', $this->timestamp()->notNull()->defaultValue(new \yii\db\Expression("NOW()")));
    }

    public function down()
    {
        $this->dropColumn($this->table, 'updated_at');
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m170509_141541_add_updated_at_to_chats cannot be reverted.\n";

        return false;
    }
    */
}
