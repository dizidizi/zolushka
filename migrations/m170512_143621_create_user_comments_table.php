<?php

use yii\db\Migration;

/**
 * Handles the creation of table `user_comments`.
 */
class m170512_143621_create_user_comments_table extends Migration
{
    private $table = 'user_comments';
    
    /**
     * @inheritdoc
     */
    public function up()
    {
        $this->createTable($this->table, [
            'id' => $this->primaryKey(),
            'from_user' => $this->integer()->notNull(),
            'to_user' => $this->integer()->notNull(),
            'comment' => $this->text()->notNull()
        ]);
    }

    /**
     * @inheritdoc
     */
    public function down()
    {
        $this->dropTable($this->table);
    }
}
