<?php

use yii\db\Migration;

class m170514_125642_add_avatar_src_field_to_users extends Migration
{
    private $table = 'users';

    public function up()
    {
        $this->addColumn($this->table, 'avatar_src', $this->string());
    }

    public function down()
    {
        $this->dropColumn($this->table, 'avatar_src');
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m170514_125642_add_avatar_src_field_to_users cannot be reverted.\n";

        return false;
    }
    */
}
