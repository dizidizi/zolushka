<?php

use yii\db\Migration;

/**
 * Handles the creation of table `user_folders`.
 */
class m170517_155155_create_user_folders_table extends Migration
{
    private $table = 'user_folders';

    /**
     * @inheritdoc
     */
    public function up()
    {
        $this->createTable($this->table, [
            'id' => $this->primaryKey(),
            'user_id' => $this->integer()->notNull(),
            'name' => $this->string()->notNull()
        ]);
    }

    /**
     * @inheritdoc
     */
    public function down()
    {
        $this->dropTable($this->table);
    }
}
