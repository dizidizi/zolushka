<?php

use yii\db\Migration;

/**
 * Handles the creation of table `user_chat_folders`.
 */
class m170517_155209_create_user_chat_folders_table extends Migration
{
    private $table = 'user_chat_folders';

    /**
     * @inheritdoc
     */
    public function up()
    {
        $this->createTable($this->table, [
            'id' => $this->primaryKey(),
            'user_id' => $this->integer()->notNull(),
            'folder_id' => $this->integer()->notNull(),
            'chat_id' => $this->integer()->notNull(),
        ]);
    }

    /**
     * @inheritdoc
     */
    public function down()
    {
        $this->dropTable($this->table);
    }
}
