<?php

namespace app\modules\admin;
use app\rbac\Roles;
use yii\web\ForbiddenHttpException;

/**
 * admin module definition class
 */
class AdminModule extends \yii\base\Module
{
    /**
     * @inheritdoc
     */
    public $controllerNamespace = 'app\modules\admin\controllers';

    /**
     * @inheritdoc
     */
    public function init()
    {
        parent::init();

        // custom initialization code goes here
    }

    public function beforeAction($action)
    {
        /**@todo: make checkRole in User*/
        if (\Yii::$app->user->isGuest || \Yii::$app->user->identity->group != Roles::ADMIN) {
            throw new ForbiddenHttpException();
        }
        
        return parent::beforeAction($action);
    }
}
