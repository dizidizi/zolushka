<?php

namespace app\modules\admin\controllers;

use app\modules\geo\models\Country;
use app\modules\photos\models\forms\UploadForm;
use app\modules\users\helpers\Values;
use app\modules\users\models\User;
use app\modules\users\models\UserPurposes;
use app\modules\users\models\UserSexTypes;
use app\modules\users\values\Complexion;
use app\modules\users\values\EyesColor;
use app\modules\users\values\Gender;
use app\modules\users\values\HairColor;
use app\modules\users\values\Look;
use app\modules\users\values\Purposes;
use app\modules\users\values\SexPeriodicity;
use app\modules\users\values\SexRole;
use app\modules\users\values\SexTypes;
use app\modules\users\values\Zodiac;
use app\rbac\Roles;
use yii\data\ActiveDataProvider;
use yii\helpers\ArrayHelper;
use yii\helpers\Url;
use yii\web\Controller;
use yii\web\NotFoundHttpException;

/**
 * Default controller for the `admin` module
 */
class DefaultController extends Controller
{
    public $layout = 'main';

    public function beforeAction($action) { 
        $this->enableCsrfValidation = false; 
        return parent::beforeAction($action); 
    }


    /**
     * Renders the index view for the module
     * @return string
     */
    public function actionIndex()
    {
        $query = User::find()
            ->with(['avatar'])
            ->newestRegister();


        $dataProvider = new ActiveDataProvider([
            'query' => $query,
            'pagination' => [
                'pageSize' => 20,
            ],
        ]);

        return $this->render('index', [
            'dataProvider' => $dataProvider,
        ]);
    }
    
    public function actionCreate()
    {
        if (\Yii::$app->request->post()) {
            return User::saveFromPost(new User(['scenario' => User::SCENARIO_ADMIN_CREATE]));
        }
        
        return $this->render('create', Values::mergeWith([
            'countries' => Country::map(Country::find()->all()),
        ]));
    }
    
    public function actionEdit($userId)
    {
        $user = User::find()
            ->byId($userId)
            ->with(['avatar', 'purposes', 'sexTypes', 'country', 'region'])
            ->one();
        
        if (!$user) {
            throw new NotFoundHttpException();
        }
        
        if (\Yii::$app->request->post()) {
            return User::saveFromPost($user);
        }
        
        return $this->render('edit', Values::mergeWith([
            'user' => $user, 
            'avatarUploadModel' => new UploadForm(),
            'countries' => Country::map(Country::find()->all()),
        ]));
    }
    
    public function actionDelete($userId)
    {
        $user = User::find()->byId($userId)->one();
        
        if ($user && $user->group != Roles::ADMIN) {
            $user->delete();
        }
        
        return $this->redirect(Url::to(['/admin/default/index']));
    }
}
