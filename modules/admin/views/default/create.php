<?php
/**
 * Created by PhpStorm.
 * User: JimmDiGriz
 * Date: 3/19/2017
 * Time: 6:48 PM
 */

use kartik\widgets\DepDrop;
use kartik\widgets\Select2;
use yii\helpers\Url;

/**@var array $hairList*/
/**@var array $eyesList*/
/**@var array $zodiacList*/
/**@var array $lookList*/
/**@var array $complexionList*/
/**@var array $purposesList*/
/**@var array $sexTypesList*/
/**@var array $sexRoleList*/
/**@var array $sexPeriodicityList*/
/**@var array $genderList*/
/**@var array $groupList*/
/**@var \app\modules\geo\models\Country[] $countries*/

?>

<div class="container-fluid">
    <header class="section-header">
        <div class="tbl">
            <div class="tbl-row">
                <div class="tbl-cell">
                    <h3>User Create</h3>
                    <ol class="breadcrumb breadcrumb-simple">
                        <li><a href="<?= Url::to(['/admin/default/index'])?>">Users</a></li>
                        <li class="active">User Create</li>
                    </ol>
                </div>
            </div>
        </div>
    </header>
    <section class="card">
        <div class="card-block">
            <form id="edit-form">
                <input type="hidden" name="_csrf" value="<?= Yii::$app->request->getCsrfToken()?>"/>
                <div class="col-md-6">
                    <h5 class="m-t-lg with-border">User Data</h5>
                    <div class="row">
                        <fieldset class="form-group col-sm-12">
                            <label class="form-label semibold" for="name">Name</label>
                            <input type="text" class="form-control" id="name" name="name" placeholder="Name">
                        </fieldset>

                        <fieldset class="form-group  col-sm-12">
                            <label class="form-label" for="email">Email address</label>
                            <input type="email" class="form-control" id="email" name="email" placeholder="Enter email"">
                        </fieldset>
					</div>
					<div class="row">
                        <div class="col-lg-6">
                            <fieldset class="form-group">
                                <label class="form-label semibold" for="group">Group</label>
                                <select class="select2" id="group" name="group">
                                    <option selected></option>
                                    <? foreach ($groupList as $group): ?>
                                        <option><?= $group ?></option>
                                    <? endforeach; ?>
                                </select>
                            </fieldset>
                        </div>
						
						<div class="col-lg-6">
							<fieldset class="form-group">
								<label class="form-label" for="password">Password</label>
								<input type="password" class="form-control" name="password" id="password" placeholder="Password">
							</fieldset>
						</div>
                    </div><!--.row-->
                </div>

                <div class="col-md-6">
                    <h5 class="m-t-lg with-border">Basic Information</h5>
                    <div class="row">
                        <div class="col-lg-4">
                            <fieldset class="form-group">
                                <label class="form-label semibold" for="gender">Gender</label>
                                <select class="select2" id="gender" name="gender">
                                    <? foreach ($genderList as $gender): ?>
                                        <option data-content='<span class="fa fa-lg fa-<?= $gender ?>"></span>&nbsp;<?= $gender ?>'><?= $gender ?></option>
                                    <? endforeach; ?>
                                </select>
                            </fieldset>
                        </div>
                        <div class="col-lg-4">
                            <fieldset class="form-group">
                                <label class="form-label" for="age">Age</label>
                                <input type="text" class="form-control" id="age" name="age" placeholder="">
                            </fieldset>
                        </div>
                        <div class="col-lg-4">
                            <fieldset class="form-group">
                                <label class="form-label" for="zodiac">Zodiac</label>
                                <select class="select2" id="zodiac" name="zodiac">
                                    <option selected></option>
                                    <? foreach ($zodiacList as $zodiac): ?>
                                        <option><?= $zodiac ?></option>
                                    <? endforeach; ?>
                                </select>
                            </fieldset>
                        </div>
					</div>
                    <div class="row">
                        <div class="col-lg-6">
                            <fieldset class="form-group">
                                <label class="form-label" for="country">Country</label>
                                <?=
                                Select2::widget([
                                    'id' => 'country-id',
                                    'name' => 'country_id',
                                    'data' => $countries,
                                    'value' => $user->country_id,
                                    'options' => [
                                        'placeholder' => 'Select country...',
                                        'class' => 'select2',
                                    ],
                                    'theme' =>'default'
                                ]);
                                ?>
                            </fieldset>
                        </div>
                        <div class="col-lg-6">
                             <label class="form-label" for="country">City</label>
                            <?=
                            DepDrop::widget([
                                'type'=>DepDrop::TYPE_SELECT2,
                                'name' => 'region_id',
                                'class' => 'select2',
                                'select2Options'=>[
                                        'pluginOptions'=>['allowClear'=>true],
                                        'theme' =>'default'
                                 ],
                                'pluginOptions'=> [
                                    'depends'=> ['country-id'],
                                    'placeholder' => 'Select region...',
                                    'url'=> Url::to(['/geo/default/regions', 'selectedId' => $user->region_id]),
                                    'initialize' => true,
                                ],
                            ]);
                            ?>
                        </div>
                    </div>
                </div>
				<div class="col-xs-12">
					<fieldset class="form-group">
						<label class="form-label" for="about">About</label>
						<textarea rows="4" class="form-control editable" placeholder="Press Enter" id="information" name="information" data-autosize></textarea>
					</fieldset>
				</div>

                <div class="col-md-12">
                    <h5 class="m-t-lg with-border">Look</h5>
                    <div class="row">
                        <fieldset class="form-group col-md-3">
                            <label class="form-label" for="height">Height</label>
                            <input type="text" class="form-control" id="height" name="height" placeholder="">
                        </fieldset>
                        <fieldset class="form-group col-md-3">
                            <label class="form-label" for="weight">Weight</label>
                            <input type="text" class="form-control" id="weight" name="weight" placeholder="">
                        </fieldset>
                        <fieldset class="form-group col-md-3">
                            <label class="form-label" for="complexion">Complexion</label>
                            <select class="select2" id="complexion" name="complexion">
                                <option selected></option>
                                <? foreach ($complexionList as $complexion): ?>
                                    <option><?= $complexion ?></option>
                                <? endforeach; ?>
                            </select>
                        </fieldset>
                    </div>
                    <div class="row">
                        <fieldset class="form-group col-md-4">
                            <label class="form-label" for="hair">Hair Color</label>
                            <select class="select2" id="hair" name="hair_color">
                                <option selected></option>
                                <? foreach ($hairList as $hair): ?>
                                    <option><?= $hair ?></option>
                                <? endforeach; ?>
                            </select>
                        </fieldset>
                        <fieldset class="form-group col-md-4">
                            <label class="form-label" for="eyes">Eyes Color</label>
                            <select class="select2" id="eyes" name="eyes_color">
                                <option selected></option>
                                <? foreach ($eyesList as $color): ?>
                                    <option><?= $color ?></option>
                                <? endforeach; ?>
                            </select>
                        </fieldset>
                    </div>
                </div>

                <div class="col-md-12">
                    <h5 class="m-t-lg with-border">Sexual Preferences</h5>
                    <div class="row">
                        <fieldset class="form-group col-md-4">
                            <label class="form-label" for="periodicity">Sex Periodicity</label>
                            <select class="select2" id="periodicity" name="sex_periodicity">
                                <option selected></option>
                                <? foreach ($sexPeriodicityList as $periodicity): ?>
                                    <option><?= $periodicity ?></option>
                                <? endforeach; ?>
                            </select>
                        </fieldset>
                        <fieldset class="form-group col-md-4">
                            <label class="form-label" for="role">Sex Role</label>
                            <select class="select2" id="role" name="sex_role">
                                <option selected></option>
                                <? foreach ($sexRoleList as $role): ?>
                                    <option><?= $role ?></option>
                                <? endforeach; ?>
                            </select>
                        </fieldset>
                        <fieldset class="form-group col-md-4">
                            <label class="form-label" for="types">Sex Types</label>
                            <select class="select2" multiple="multiple" id="types" name="types[]">
                                <? foreach ($sexTypesList as $type): ?>
                                    <option><?= $type ?></option>
                                <? endforeach; ?>
                            </select>
                        </fieldset>
                    </div>
                </div>
                <div class="col-md-12">
                    <div class="row">
<!--                        <section class="profile-side-user">-->
<!--                            <button type="button" class="avatar-preview avatar-preview-128">-->
<!--                                <img src="/zolushka-yii/basic/web/frontend/img/avatar-1-256.png" alt=""/>-->
<!--                                <!--                                <img src="img/avatar-1-256.png" alt=""/>-->
<!--	  								<span class="update">-->
<!--	  									<i class="font-icon font-icon-picture-double"></i>-->
<!--	  									Update photo-->
<!--	  								</span>-->
<!--                                <input type="file"/>-->
<!--                            </button>-->
<!--                            <div class="bottom-txt">Users Avatar</div>-->
<!--                        </section>-->
                        
                    </div>
					<button type="button" class="btn btn-inline btn-success" id="edit-submit">Save</button>
					<a href="<?= Url::to(['/admin/default/index'])?>" class="btn btn-inline btn-secondary">Cancel</a>
                </div>
            </form>
        </div>
    </section>
</div> <!-- Container Fluid -->