<?php
/**
 * Created by PhpStorm.
 * User: JimmDiGriz
 * Date: 17.03.2017
 * Time: 15:10
 */
use yii\data\ActiveDataProvider;
use yii\widgets\Pjax;

/**@var ActiveDataProvider $dataProvider*/

$this->title = 'Users';
?>
<header class="page-content-header">
	  <div class="container-fluid">
	    <div class="tbl">
	      <div class="tbl-row">
	        <div class="tbl-cell">
			  <h3><?= $this->title?></h3>
	        </div>
	        <div class="tbl-cell tbl-cell-action">
	          <a href="<?= \yii\helpers\Url::to(['/admin/default/create'])?>" class="btn btn-round">Create</a>
	        </div> 
	      </div>
	    </div>
	  </div>
	</header><!--.page-content-header-->
<div class="container-fluid">
    <section class="box-typical">
        <header class="box-typical-header">
            <div class="tbl-row">
                <div class="tbl-cell tbl-cell-title">
                    <h3><?= $dataProvider->getTotalCount() ?> users</h3>
                </div>
            </div>
        </header>
        <div class="box-typical-body">
            <div class="table-responsive">
                <? Pjax::begin(); ?>
                <table class="table table-hover">
                    <thead>
                    <tr>
                        <th class="table-check">
                            <div class="checkbox checkbox-only">
                                <input type="checkbox" id="table-check-head"/>
                                <label for="table-check-head"></label>
                            </div>
                        </th>
                        <th>Name</th>
                        <th>Email</th>
                        <th>Age</th>
<!--                        <th class="table-icon-cell">-->
<!--                            <i class="font-icon font-icon-heart"></i>-->
<!--                        </th>-->
<!--                        <th class="table-icon-cell">-->
<!--                            <i class="font-icon font-icon-comment"></i>-->
<!--                        </th>-->
                        <th>Date Created</th>
                        <th></th>
                        <th></th>
                    </tr>
                    </thead>
                    <tbody>
                        <?= \yii\widgets\ListView::widget([
                            'dataProvider' => $dataProvider,
                            'itemView' => 'items/_user_list_item',
                            'summary' => '',
//                            'pageCssClass' => 'page-item',
//                            'linkOptions' => ['class' => 'page-link'],
                            'layout' => "{items}\n</tbody></table>{pager}",
//                            'viewParams' => [
//                                'page' => $dataProvider->getPagination()->getPage(),
//                            ],
                            'itemOptions' => [
                                'tag' => 'div',
                                'class' => 'news-item',
                            ],
                        ]) ?>
                    <? Pjax::end(); ?>
                    
            </div>
        </div><!--.box-typical-body-->
    </section><!--.box-typical-->
</div><!--.container-fluid-->


