<?php
/**
 * Created by PhpStorm.
 * User: JimmDiGriz
 * Date: 17.03.2017
 * Time: 16:41
 */
use app\modules\users\models\User;

/**@var User $model*/

?>

<tr>
    <td class="table-check">
        <div class="checkbox checkbox-only">
            <input type="checkbox" id="table-check-<?= $index ?>"/>
            <label for="table-check-<?= $index ?>"></label>
        </div>
    </td>
    <td>
        <a href="<?= \yii\helpers\Url::toRoute(['/users/default/view', 'userId' => $model->id]);?>" target="_blank"><?= $model->name ?></a>
    </td>
    <td class="color-blue-grey-lighter"><?= $model->email ?></td>
    <td class="color-blue-grey-lighter"><?= $model->age ?></td>
<!--    <td class="table-icon-cell">-->
<!--        <i class="font-icon font-icon-heart"></i>-->
<!--        5-->
<!--    </td>-->
<!--    <td class="table-icon-cell">-->
<!--        <i class="font-icon font-icon-comment"></i>-->
<!--        24-->
<!--    </td>-->
    <td class="table-date"> <?= $model->created_at ?> <i class="font-icon font-icon-clock"></i></td>
    <td class="table-photo">
        <img src="<?= \app\core\components\AvatarManager::getAvatarForUser($model) ?>" alt="" data-toggle="tooltip" data-placement="bottom" title="<?= $model->name ?>">
    </td>
    <td>
        <div class="tabledit-toolbar btn-toolbar" style="text-align: left;">
            <div class="btn-group btn-group-sm" style="float: none;">
                <a href = "<?= \yii\helpers\Url::toRoute(['/admin/default/edit', 'userId' => $model->id])?>" class="tabledit-edit-button btn btn-sm btn-default" style="float: none;">
                    <span class="glyphicon glyphicon-pencil"></span>
                </a>
                <? if ($model->group != \app\rbac\Roles::ADMIN): ?>
                <a href = "<? if ($model->group != \app\rbac\Roles::ADMIN): ?><?= \yii\helpers\Url::toRoute(['/admin/default/delete', 'userId' => $model->id])?> <? endif; ?>" class="tabledit-delete-button btn btn-sm btn-default" style="float: none;">
                    <span class="glyphicon glyphicon-trash"></span>
                </a>
                <? endif; ?>
            </div>
    </td>
</tr>
