<?php
/**
 * Created by PhpStorm.
 * User: JimmDiGriz
 * Date: 3/18/2017
 * Time: 5:47 PM
 */

use app\assets\AdminAsset;
use yii\helpers\Html;
use yii\helpers\Url;

AdminAsset::register($this);

$this->title = 'Admin';

?>

<?php $this->beginPage() ?>
<!DOCTYPE html>
<html lang="<?= Yii::$app->language ?>">
<head>
    <meta charset="<?= Yii::$app->charset ?>">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <?= Html::csrfMetaTags() ?>
    <title><?= Html::encode($this->title) ?></title>
    <?php $this->head() ?>
</head>
<body class="with-side-menu theme-picton-blue-white-ebony chrome-browser">
<?php $this->beginBody() ?>

<header class="site-header">
    <div class="container-fluid">
        <a href="<?= \yii\helpers\Url::to(['/admin/default/index']) ?>" class="site-logo">
            <span style="font-size: 18pt; color: white; font-weight: bold;">Cinderella</span>
        </a>
        <button class="hamburger hamburger--htla">
            <span>toggle menu</span>
        </button>
        <div class="site-header-content">
            <div class="site-header-content-in">
                <div class="mobile-menu-right-overlay"></div>
				<div class="site-header-shown">
					<? if (!Yii::$app->user->isGuest): ?>
							<?if (Yii::$app->user->identity->group == \app\rbac\Roles::ADMIN): ?>
								<div class="dropdown dropdown-notification messages" style="margin-top: 5px; margin-right: 20px;">
									<a href="<?= \yii\helpers\Url::to(['/users/default/index']) ?>">Site</a>
								</div>
							<? endif; ?> 
                            <div class="dropdown user-menu">
                                <button class="dropdown-toggle" id="dd-user-menu" type="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                    <img src="<?=\app\core\components\AvatarManager::getAvatarForUser(Yii::$app->user->identity) ?>" alt="">
                                </button>
                                <div class="dropdown-menu dropdown-menu-right" aria-labelledby="dd-user-menu">
                                    <a class="dropdown-item" href="<?= Url::to(['/users/default/view', 'userId' => \Yii::$app->user->identity->id])?>"><span class="font-icon glyphicon glyphicon-user"></span>Profile</a>
                                    <a class="dropdown-item" href="<?= Url::to(['/users/default/settings'])?>"><span class="font-icon glyphicon glyphicon-cog"></span>Settings</a>
                                    <div class="dropdown-divider"></div>
                                    <a class="dropdown-item" href="<?=  \yii\helpers\Url::to(['/users/default/logout']) ?>"><span class="font-icon glyphicon glyphicon-log-out"></span>Logout</a>
                                </div>
                            </div>
							
                    <? else: ?>
                        <a href="<?= \yii\helpers\Url::to(['/users/default/login']) ?>">Login</a>
                        <a href="<?= \yii\helpers\Url::to(['/users/default/register']) ?>">Register</a>
                    <? endif; ?>
				</div>
                <div class="site-header-collapsed">
                    <div class="site-header-collapsed-in">
                    </div><!--.site-header-collapsed-in-->
                </div><!--.site-header-collapsed-->
            </div><!--site-header-content-in-->
        </div><!--.site-header-content-->
    </div><!--.container-fluid-->
</header><!--.site-header-->

<div class="mobile-menu-left-overlay"></div>
<nav class="side-menu">
    <ul class="side-menu-list">
        <li class="blue">
            <a href="<?= \yii\helpers\Url::to(['/admin/default/index']) ?>">
                <i class="font-icon font-icon-user"></i>
                <span class="lbl">Users</span>
            </a>
        </li>
    </ul>
</nav><!--.side-menu-->

<div class="page-content">
    <?= $content ?>
</div>

<?php $this->endBody() ?>
</body>
</html>
<?php $this->endPage() ?>


