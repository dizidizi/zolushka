<?php
/**
 * Created by PhpStorm.
 * User: JimmDiGriz
 * Date: 02.05.2017
 * Time: 7:14
 */

namespace app\modules\chat\controllers;


use app\core\base\JsonApiController;
use app\core\components\AvatarManager;
use app\modules\chat\models\Chat;
use app\modules\chat\models\ChatMessage;
use app\modules\chat\models\UserChatFolder;
use app\modules\chat\models\UserChats;
use app\modules\chat\models\UserFolder;
use app\modules\chat\values\MessageStatuses;
use app\modules\users\models\User;
use yii\data\ActiveDataProvider;
use yii\filters\AccessControl;
use yii\helpers\ArrayHelper;
use yii\helpers\VarDumper;
use yii\web\ForbiddenHttpException;
use yii\web\HttpException;
use yii\web\NotFoundHttpException;

class ApiController extends JsonApiController
{
    public $modelClass = ChatMessage::class;
    
    public function behaviors()
    {
        return ArrayHelper::merge(parent::behaviors(), [
            'private' => [
                'class' => AccessControl::className(),
                'only' => ['send', 'edit', 'delete-messages', 'read'],
                'rules' => [
                    [
                        'allow' => true,
                        'ips' => ['127.0.0.1'],
                    ],
                ],
            ],
            'public' => [
                'class' => AccessControl::className(),
                'only' => ['chats', 'messages', 'create-chat', 'folders', 
                    'create-folder', 'edit-folder', 'delete-folder', 'add-to-folder', 'remove-from-folder'],
                'rules' => [
                    [
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ]
        ]);
    }

    public function actions() 
    {
        $actions = parent::actions();

        $custom_actions = [
            'update' => [
                'class' => 'app\controllers\actions\WhateverAction',
                'modelClass' => $this->modelClass,
                'checkAccess' => [$this, 'checkAccess'],
                'scenario' => $this->updateScenario,
                'params' => \Yii::$app->request->bodyParams,
            ],
        ];

        return array_merge($actions, $custom_actions);
    }

    public function verbs()
    {
        $verbs = parent::verbs();

        $verbs["send"] = ['POST'];
        $verbs["create-chat"] = ['POST'];
        $verbs["edit"] = ['PUT'];
        $verbs["delete-messages"] = ['DELETE'];
        $verbs["read"] = ['GET'];
        $verbs["folders"] = ['GET'];
        $verbs["create-folder"] = ['POST'];
        $verbs["edit-folder"] = ['PUT'];
        $verbs["delete-folder"] = ['DELETE'];
        $verbs["add-to-folder"] = ['POST'];
        $verbs["remove-from-folder"] = ['DELETE'];

        return $verbs;
    }
    
    /**
     * @throws \Exception
     * 
     * @throws ForbiddenHttpException
     * 
     * @return Chat
     */
    public function actionCreateChat()
    {
        $required = [
            'userId' => true,
        ];

        $params = $this->getParams($required);

        $exists = Chat::find()
            ->byUsers([$params['userId'], \Yii::$app->user->id])
            ->exists();
        
        if ($exists) {
            throw new ForbiddenHttpException($exists);
        }
        
        $transaction = \Yii::$app->db->beginTransaction();
        
        try {
            $chat = Chat::create($params['userId']);
            
            if (!$chat) {
                throw new \Exception('ChatCreationFailed');
            }
            
            $transaction->commit();
        } catch (\Exception $e) {
            $transaction->rollBack();
            
            throw $e;
        }
        
        $chat->refresh();

        return $chat;
    }

    /**
     * @param string $mark
     * 
     * @return Chat[]
     */
    public function actionChats($mark = null)
    {
        $this->modelClass = Chat::className();
        
        if (!$mark) {
            /**
             * @todo: replace by caching addition data in chat fields
             */
            
            $chats = Chat::find()
                ->with(['users'])
                ->byUser(\Yii::$app->user->id)
                ->newest();
            
//            return array_map()
            
            $provider = new ActiveDataProvider([
                'query' => $chats,
                'pagination' => false,
            ]);
            
            return $provider;
        }
        
        return \Yii::$app->user->identity->getChatsByMark($mark);
    }

    /**
     * @param int $chatId
     * 
     * @throws ForbiddenHttpException|NotFoundHttpException
     * 
     * @return ChatMessage[]
     */
    public function actionMessages($chatId)
    {
        $chat = Chat::find()->byId($chatId)->one();
        
        if (!$chat) {
            throw new NotFoundHttpException('ChatNotFound');
        }
        
        if (!$chat->isParticipant(\Yii::$app->user->id)) {
            throw new ForbiddenHttpException('UserNotInChat');
        }
        
        $chat->read();
        
        $provider = new ActiveDataProvider([
            'query' => ChatMessage::find()->newest()->byChat($chatId)
        ]);
        
        return $provider;
    }
    
    public function actionFolders()
    {
        $this->modelClass = UserFolder::className();
        
//        $folders = UserFolder::find()->byUser(\Yii::$app->user->id);
//        
//        if (!$folders) {
//            throw new NotFoundHttpException('FoldersNotFound');
//        }

        $provider = new ActiveDataProvider([
            'query' => UserFolder::find()->byUser(\Yii::$app->user->id)
        ]);

        return $provider;
    }
    
    public function actionCreateFolder()
    {
        $this->modelClass = UserFolder::className();
        
        $required = [
            'name' => true,
        ];
        
        $params = $this->getParams($required);
        
        $folder = new UserFolder();
        
        /**name not unique now*/
        
        $folder->user_id = \Yii::$app->user->id;
        $folder->name = $params['name'];
        
        if (!$folder->save()) {
            throw new HttpException(500, json_encode($folder->getErrors()));
        }
        
        return $folder;
    }
    
    public function actionEditFolder()
    {
        $this->modelClass = UserFolder::className();
        
        $required = [
            'id' => true,
            'name' => true,
        ];
        
        $params = $this->getParams($required);
        
        $folder = UserFolder::find()
            ->byId($params['id'])
            ->one();
        
        if (!$folder) {
            throw new NotFoundHttpException('FolderNotFound');
        }
        
        if ($folder->user_id != \Yii::$app->user->id) {
            throw new ForbiddenHttpException('OtherUserOwnThisFolder');
        }
        
        $folder->name = $params['name'];

        if (!$folder->save()) {
            throw new HttpException(500, json_encode($folder->getErrors()));
        }

        return $folder;
    }
    
    public function actionDeleteFolder($folderId)
    {
        $folder = UserFolder::find()
            ->byId($folderId)
            ->one();

        if (!$folder) {
            throw new NotFoundHttpException('FolderNotFound');
        }

        if ($folder->user_id != \Yii::$app->user->id) {
            throw new ForbiddenHttpException('OtherUserOwnThisFolder');
        }

        if (!$folder->delete()) {
            throw new HttpException(500, json_encode($folder->getErrors()));
        }
        
        return true;
    }
    
    public function actionAddToFolder()
    {
        $this->modelClass = UserChatFolder::className();
        
        $required = [
            'chat_id' => true,
            'folder_id' => true,
        ];
        
        $params = $this->getParams($required);
        
        $chat = Chat::find()
            ->byId($params['chat_id'])
            ->one();
        
        if (!$chat) {
            throw new NotFoundHttpException('ChatNotFound');
        }
        
        if (!$chat->isParticipant(\Yii::$app->user->id)) {
            throw new ForbiddenHttpException('OthersChat');
        }
        
        $folder = UserFolder::find()
            ->byId($params['folder_id'])
            ->one();
        
        if (!$folder) {
            throw new NotFoundHttpException('FolderBotFound');
        }
        
        if ($folder->user_id != \Yii::$app->user->id) {
            throw new ForbiddenHttpException('OthersFolder');
        }
        
        $connection = new UserChatFolder();
        
        $connection->user_id = \Yii::$app->user->id;
        $connection->folder_id = $folder->id;
        $connection->chat_id = $chat->id;

        if (!$connection->save()) {
            throw new HttpException(500, json_encode($connection->getErrors()));
        }
        
        return $connection;
    }
    
    public function removeFromFolder($chatId, $folderId)
    {
        $chat = Chat::find()
            ->byId($chatId)
            ->one();
        
        if (!$chat) {
            throw new NotFoundHttpException('ChatNotFound');
        }
        
        if (!$chat->isParticipant(\Yii::$app->user->id)) {
            throw new ForbiddenHttpException('OthersChat');
        }
        
        $folder = UserFolder::find()
            ->byId($folderId)
            ->one();
        
        if (!$folder) {
            throw new NotFoundHttpException('FolderNotFound');
        }
        
        if ($folder->user_id != \Yii::$app->user->id) {
            throw new ForbiddenHttpException('OthersFolder');
        }
        
        $connection = UserChatFolder::find()
            ->byUser(\Yii::$app->user->id)
            ->byFolder($folder->id)
            ->byChat($chat->id)
            ->one();
        
        if (!$connection) {
            throw new NotFoundHttpException('ChatNotFoundInThisFolder');
        }

        if (!$connection->delete()) {
            throw new HttpException(500, json_encode($connection->getErrors()));
        }
        
        return true;
    }
    
    /**
     * @throws NotFoundHttpException|ForbiddenHttpException|HttpException
     * 
     * @return ChatMessage
     */
    public function actionSend()
    {
        $required = [
            'senderId' => true,
            'chatId' => true,
            'message' => true,
        ];
        
        $params = $this->getParams($required);
        
        $sender = User::find()
            ->byId($params['senderId'])
            ->one();
        
        if (!$sender) {
            throw new NotFoundHttpException("UserWithId:[{$params['senderId']}]NotFound");
        }
        
        $chat = Chat::find()
            ->byId($params['chatId'])
            ->one();
        
        if (!$chat) {
            throw new NotFoundHttpException("ChatWithId:[{$params['chatId']}]NotFound");
        }
        
        if (!$chat->isParticipant($params['senderId'])) {
            throw new ForbiddenHttpException();
        }
        
        $chatMessage = new ChatMessage();

        $chatMessage->message = $params['message'];
        $chatMessage->user_id = $sender->id;
        $chatMessage->chat_id = $chat->id;
        
        $chatMessage->setScenario(ChatMessage::SCENARIO_CREATE);
        
        if (!$chatMessage->save()) {
            throw new HttpException(500, json_encode($chatMessage->getErrors()));
        }
        
        $chatMessage->refresh();
        
        $chat->touch('updated_at');
        
        return [
            'model' => $chatMessage,
            'user' => $chatMessage->user,
            'chat' => $chatMessage->chat
        ];
    }
    
    /**
     * @throws NotFoundHttpException|ForbiddenHttpException|HttpException
     *
     * @return ChatMessage
     */
    public function actionEdit()
    {
        $required = [
            'userId' => true,
            'messageId' => true,
            'message' => true,
        ];
        
        $params = $this->getParams($required);
        
        $user = User::find()
            ->byId($params['userId'])
            ->one();

        if (!$user) {
            throw new NotFoundHttpException("UserWithId:[{$params['userId']}]NotFound");
        }
        
        $chatMessage = ChatMessage::find()
            ->byId($params['messageId'])
            ->one();
        
        if (!$chatMessage) {
            throw new NotFoundHttpException("MessageWithId:[{$params['messageId']}]NotFound");
        }
        
        $chatMessage->setScenario(ChatMessage::SCENARIO_EDIT);
        
        $chatMessage->message = $params['message'];

        if (!$chatMessage->save()) {
            throw new HttpException(500, json_encode($chatMessage->getErrors()));
        }

        return [
            'model' => $chatMessage,
            'users' => $chatMessage->chat->users
        ];
    }
    
    /**
     * @param int $userId
     * @param int $messageIds
     * 
     * @throws NotFoundHttpException|ForbiddenHttpException|HttpException
     * 
     * @return bool
     */
    public function actionDeleteMessages($userId, $messageIds)
    {
//        $required = [
//            'userId' => true,
//            'messageIds' => true,
//        ];
        
        $user = User::find()
            ->byId($userId)
            ->one();

        if (!$user) {
            throw new NotFoundHttpException("UserWithId:[{$userId}]NotFound");
        }
        
        $messageIds = explode(',', $messageIds);

        $chatMessages = ChatMessage::find()
            ->byIds($messageIds)
            ->all();
        
        if (!$chatMessages) {
            throw new NotFoundHttpException("MessagesNotFound");
        }
        
        $chatId = $chatMessages[0]->chat_id;
        $chatUsers = $chatMessages[0]->chat->users;
        
        foreach ($chatMessages as $message) {
            $message->delete();
        }

        return [
            'chatId' => $chatId,
            'chatUsers' => $chatUsers,
            'ids' => $messageIds
        ];
    }

    /**
     * @param int $userId
     * 
     * @param int $chatId
     * 
     * @throws NotFoundHttpException|ForbiddenHttpException|HttpException
     * 
     * @return ChatMessage
     */
    public function actionRead($userId, $chatId)
    {
        $user = User::find()
            ->byId($userId)
            ->one();

        if (!$user) {
            throw new NotFoundHttpException("UserWithId:[{$userId}]NotFound");
        }
        
        $messages = ChatMessage::find()
            ->byChat($chatId)
            ->andWhere('user_id != :user_id',[
                ':user_id' => $userId
            ])
            ->andWhere(['status' => MessageStatuses::UNREAD])
            ->all();
        
        foreach ($messages as $message) {
            $message->setScenario(ChatMessage::SCENARIO_READ);
            
            $message->status = MessageStatuses::READ;
            
            $message->save();
        }
        
        return [
            'success' => true,
        ];
    }
}