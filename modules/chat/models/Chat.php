<?php

namespace app\modules\chat\models;

use app\modules\chat\values\ChatTypes;
use app\modules\chat\values\MessageStatuses;
use app\modules\users\models\User;
use app\modules\users\models\UserComment;
use Yii;
use yii\behaviors\TimestampBehavior;
use yii\db\Expression;

/**
 * This is the model class for table "chats".
 *
 * @property int $id
 * @property string $created_at
 * @property string $updated_at
 * @property int $type
 * @property string $settings
 *
 * @property \app\modules\users\models\User[] $users
 */
class Chat extends \yii\db\ActiveRecord
{
    const SCENARIO_CREATE = 'chatCreate';

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'chats';
    }

    public function scenarios()
    {
        return [
            self::SCENARIO_CREATE => ['created_at', 'type', 'settings'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['created_at', 'updated_at'], 'safe'],
            [['type'], 'required'],
            [['type'], 'integer'],
            [['type'], 'in', 'range' => ChatTypes::$list],
            [['settings'], 'string'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'created_at' => Yii::t('app', 'Created At'),
            'updated_at' => Yii::t('app', 'Updated At'),
            'type' => Yii::t('app', 'Type'),
            'settings' => Yii::t('app', 'Settings'),
        ];
    }

    public function behaviors()
    {
        return [
            'timestampBehavior' => [
                'class' => TimestampBehavior::className(),
                'createdAtAttribute' => 'created_at',
                'value' => new Expression('NOW()'),
            ],
        ];
    }

    public function extraFields()
    {
        return [
            'users',
            'unread',
            'folders',
//            'comment'
        ];
    }

    public function getUsers()
    {
        return $this->hasMany(User::className(), ['id' => 'user_id'])
            ->viaTable('users_chats', ['chat_id' => 'id'])
            ->with('avatar');
    }
    
    public function getUnread()
    {
        return (int) ChatMessage::find()
            ->byChat($this->id)
            ->andWhere('user_id != :user_id', [
                ':user_id' => Yii::$app->user->id
            ])
            ->andWhere(['status' => MessageStatuses::UNREAD])
            ->count();
    }

    public function getFolders()
    {
        return $this->hasMany(UserFolder::className(), ['id' => 'folder_id'])
            ->viaTable('user_chat_folders', ['chat_id' => 'id'])
            ->andWhere(['user_id' => Yii::$app->user->id]);
    }
    
    public function getComment()
    {
        $currentId = Yii::$app->user->id;
        $participantId = null;
        
        foreach ($this->users as $user) {
            if ($user->id != $currentId) {
                $participantId = $user->id;
                break;
            }
        }
        
        return $this->hasOne(UserComment::className(), ['from_user' => $currentId])
            ->andOnCondition(['to_user' => $participantId]);
    }

    /**
     * @inheritdoc
     * @return ChatsQuery the active query used by this AR class.
     */
    public static function find()
    {
        return new ChatsQuery(get_called_class());
    }
    
    /**
     * @param int $userId
     * 
     * @return bool
     */
    public function isParticipant($userId)
    {
        $record = UserChats::find()
            ->byChat($this->id)
            ->byUser($userId)
            ->one();
        
        return $record ? true : false;
    }
    
    public function getParticipantId()
    {
        
    }
    
    public function read()
    {
        $messages = ChatMessage::find()
            ->byChat($this->id)
            ->andWhere('user_id != :user_id', [
                ':user_id' => Yii::$app->user->id
            ])
            ->andWhere(['status' => MessageStatuses::UNREAD])
            ->all();
        
        foreach ($messages as $message) {
            $message->setScenario(ChatMessage::SCENARIO_READ);
            
            $message->status = MessageStatuses::READ;
            
            $message->save();
        }
    }
    
    /**
     * @param int $userId
     * @param $type
     * @param $settings
     * 
     * @return bool|Chat
     */
    public static function create($userId, $type = ChatTypes::NORMAL, $settings = null)
    {
        $chat = new Chat(['scenario' => Chat::SCENARIO_CREATE]);
        
        $chat->type = $type;
        $chat->settings = $settings;
        
        if (!$chat->save()) {
            return false;
        }
        
        $ids = [$userId, Yii::$app->user->id];
        
        foreach ($ids as $id) {
            $userChat = new UserChats(['scenario' => UserChats::SCENARIO_CREATE]);
            
            $userChat->chat_id = $chat->id;
            $userChat->user_id = $id;
            
            if (!$userChat->save()) {
                return false;
            }
        }
        
        return $chat;
    }
}
