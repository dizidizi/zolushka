<?php

namespace app\modules\chat\models;

use app\modules\chat\values\MessageStatuses;
use app\modules\users\models\User;
use Yii;
use yii\behaviors\TimestampBehavior;
use yii\db\Expression;

/**
 * This is the model class for table "chat_messages".
 *
 * @property int $id
 * @property int $user_id
 * @property int $chat_id
 * @property string $created_at
 * @property string $updated_at
 * @property string $message
 * @property int $status
 *
 * @property \app\modules\users\models\User $user
 * @property \app\modules\chat\models\Chat $chat
 */
class ChatMessage extends \yii\db\ActiveRecord
{
    const SCENARIO_EDIT = 'editChatMessage';
    const SCENARIO_READ = 'readChatMessage';
    const SCENARIO_CREATE = 'createChatMessage';
    
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'chat_messages';
    }

    public function scenarios()
    {
        return [
            self::SCENARIO_EDIT => ['updated_at', 'message'],
            self::SCENARIO_READ => ['status'],
            self::SCENARIO_CREATE => ['user_id', 'chat_id', 'message', 'created_at', 'updated_at', 'status'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['user_id', 'chat_id', 'message'], 'required'],
            [['user_id', 'chat_id'], 'integer'],
            [['created_at', 'updated_at'], 'safe'],
            [['message'], 'string'],
            [['status'], 'in', 'range' => MessageStatuses::$list],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'user_id' => Yii::t('app', 'User ID'),
            'chat_id' => Yii::t('app', 'Chat ID'),
            'created_at' => Yii::t('app', 'Created At'),
            'updated_at' => Yii::t('app', 'Created At'),
            'message' => Yii::t('app', 'Message'),
            'status' => Yii::t('app', 'Status'),
        ];
    }

    public function behaviors()
    {
        return [
            'timestampBehavior' => [
                'class' => TimestampBehavior::className(),
                'createdAtAttribute' => 'created_at',
                'updatedAtAttribute' => 'updated_at',
                'value' => new Expression('NOW()'),
            ],
        ];
    }

    public function extraFields()
    {
        return [
            'chat',
            'user',
        ];
    }

    /**
     * @return \app\modules\chat\models\Chat
     */
    public function getChat()
    {
        return $this->hasOne(Chat::className(), ['id' => 'chat_id']);
    }

    /**
     * @return \app\modules\users\models\User
     */
    public function getUser()
    {
        return $this->hasOne(User::className(), ['id' => 'user_id']);
    }

    /**
     * @inheritdoc
     * @return ChatMessagesQuery the active query used by this AR class.
     */
    public static function find()
    {
        return new ChatMessagesQuery(get_called_class());
    }
}
