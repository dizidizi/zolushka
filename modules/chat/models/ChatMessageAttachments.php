<?php

namespace app\modules\chat\models;

use Yii;

/**
 * This is the model class for table "chat_message_attachments".
 *
 * @property int $message_id
 * @property string $target
 * @property int $target_id
 */
class ChatMessageAttachments extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'chat_message_attachments';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['message_id', 'target', 'target_id'], 'required'],
            [['message_id', 'target_id'], 'integer'],
            [['target'], 'string', 'max' => 255],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'message_id' => Yii::t('app', 'Message ID'),
            'target' => Yii::t('app', 'Target'),
            'target_id' => Yii::t('app', 'Target ID'),
        ];
    }

    /**
     * @inheritdoc
     * @return ChatMessageAttachmentsQuery the active query used by this AR class.
     */
    public static function find()
    {
        return new ChatMessageAttachmentsQuery(get_called_class());
    }
}
