<?php

namespace app\modules\chat\models;

/**
 * This is the ActiveQuery class for [[ChatMessageAttachments]].
 *
 * @see ChatMessageAttachments
 */
class ChatMessageAttachmentsQuery extends \yii\db\ActiveQuery
{
    /*public function active()
    {
        return $this->andWhere('[[status]]=1');
    }*/

    /**
     * @inheritdoc
     * @return ChatMessageAttachments[]|array
     */
    public function all($db = null)
    {
        return parent::all($db);
    }

    /**
     * @inheritdoc
     * @return ChatMessageAttachments|array|null
     */
    public function one($db = null)
    {
        return parent::one($db);
    }
}
