<?php

namespace app\modules\chat\models;

/**
 * This is the ActiveQuery class for [[ChatMessage]].
 *
 * @see ChatMessage
 */
class ChatMessagesQuery extends \yii\db\ActiveQuery
{
    /*public function active()
    {
        return $this->andWhere('[[status]]=1');
    }*/

    /**
     * @inheritdoc
     * @return ChatMessage[]|array
     */
    public function all($db = null)
    {
        return parent::all($db);
    }

    /**
     * @inheritdoc
     * @return ChatMessage|array|null
     */
    public function one($db = null)
    {
        return parent::one($db);
    }

    /**
     * @param int $id
     *
     * @return ChatMessagesQuery
     */
    public function byId($id)
    {
        return $this->andWhere(["id" => $id]);
    }

    /**
     * @param int[] $ids
     *
     * @return ChatMessagesQuery
     */
    public function byIds($ids)
    {
        return $this->andWhere(["id" => $ids]);
    }

    /**
     * @param int $chatId
     *
     * @return ChatMessagesQuery
     */
    public function byChat($chatId)
    {
        return $this->andWhere(["chat_id" => $chatId]);
    }

    /**
     * @param int $userId
     *
     * @return ChatMessagesQuery
     */
    public function byUser($userId)
    {
        return $this->andWhere(["user_id" => $userId]);
    }

    /**
     * @param int $status
     *
     * @return ChatMessagesQuery
     */
    public function byStatus($status)
    {
        return $this->andWhere(["status" => $status]);
    }

    /**
     * @return ChatMessagesQuery
     */
    public function newest()
    {
        return $this->addOrderBy("created_at desc");
    }

    /**
     * @return ChatMessagesQuery
     */
    public function oldest()
    {
        return $this->addOrderBy("created_at");
    }
}
