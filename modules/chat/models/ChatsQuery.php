<?php

namespace app\modules\chat\models;

/**
 * This is the ActiveQuery class for [[Chat]].
 *
 * @see Chat
 */
class ChatsQuery extends \yii\db\ActiveQuery
{
    /*public function active()
    {
        return $this->andWhere('[[status]]=1');
    }*/

    /**
     * @inheritdoc
     * @return Chat[]|array
     */
    public function all($db = null)
    {
        return parent::all($db);
    }

    /**
     * @inheritdoc
     * @return Chat|array|null
     */
    public function one($db = null)
    {
        return parent::one($db);
    }

    /**
     * @param int $id
     * 
     * @return ChatsQuery
     */
    public function byId($id)
    {
        return $this->andWhere(['id' => $id]);
    }

    /**
     * @return ChatsQuery
     */
    public function newest()
    {
        return $this->addOrderBy("updated_at desc");
    }

    /**
     * @return ChatsQuery
     */
    public function oldest()
    {
        return $this->addOrderBy("created_at");
    }

    /**
     * @param int $type
     *
     * @return ChatsQuery
     */
    public function byType($type)
    {
        return $this->andWhere(["type" => $type]);
    }

    /**
     * @param int $userId
     *
     * @return ChatsQuery
     */
    public function byUser($userId)
    {
        return $this->joinWith(['users'])
            ->andWhere(['users_chats.user_id' => $userId]);
    }

    /**
     * @param int $ids
     *
     * @return ChatsQuery
     */
    public function byUsers($ids)
    {
        return $this->andWhere('(select count(*) from users_chats uc where uc.chat_id = chats.id and uc.user_id in (:ids)) > 1', [
            ':ids' => implode(','. $ids),
        ]);
    }
}
