<?php

namespace app\modules\chat\models;

use app\core\base\CinderellaModel;
use app\modules\users\models\User;
use Yii;

/**
 * This is the model class for table "user_chat_folders".
 *
 * @property int $id
 * @property int $user_id
 * @property int $folder_id
 * @property int $chat_id
 * 
 * @property \app\modules\users\models\User $user
 * @property \app\modules\chat\models\UserFolder $folder
 * @property \app\modules\chat\models\Chat $chat
 */
class UserChatFolder extends CinderellaModel
{
    const SCENARIO_CREATE = 'createUserChatFolder';
    
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'user_chat_folders';
    }

    public function scenarios()
    {
        return [
            self::SCENARIO_CREATE => ['user_id', 'folder_id', 'chat_id'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['user_id', 'folder_id', 'chat_id'], 'required'],
            [['user_id', 'folder_id', 'chat_id'], 'integer'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'user_id' => Yii::t('app', 'User ID'),
            'folder_id' => Yii::t('app', 'Folder ID'),
            'chat_id' => Yii::t('app', 'Chat ID'),
        ];
    }

    public function extraFields()
    {
        return [
            'user',
            'folder',
            'chat'
        ];
    }

    public function getUser()
    {
        return $this->hasOne(User::className(), ['id' => 'user_id']);
    }

    public function getFolder()
    {
        return $this->hasOne(UserFolder::className(), ['id' => 'folder_id']);
    }

    public function getChat()
    {
        return $this->hasOne(Chat::className(), ['id' => 'chat_id']);
    }

    /**
     * @inheritdoc
     * @return UserChatFolderQuery the active query used by this AR class.
     */
    public static function find()
    {
        return new UserChatFolderQuery(get_called_class());
    }
}
