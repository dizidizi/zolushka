<?php

namespace app\modules\chat\models;

/**
 * This is the ActiveQuery class for [[UserChatFolder]].
 *
 * @see UserChatFolder
 */
class UserChatFolderQuery extends \yii\db\ActiveQuery
{
    /*public function active()
    {
        return $this->andWhere('[[status]]=1');
    }*/

    /**
     * @inheritdoc
     * @return UserChatFolder[]|array
     */
    public function all($db = null)
    {
        return parent::all($db);
    }

    /**
     * @inheritdoc
     * @return UserChatFolder|array|null
     */
    public function one($db = null)
    {
        return parent::one($db);
    }

    /**
     * @param int $id
     *
     * @return UserChatFolderQuery
     */
    public function byId($id)
    {
        return $this->andWhere(['id' => $id]);
    }

    /**
     * @param int $id
     *
     * @return UserChatFolderQuery
     */
    public function byUser($id)
    {
        return $this->andWhere(['user_id' => $id]);
    }

    /**
     * @param int $id
     *
     * @return UserChatFolderQuery
     */
    public function byFolder($id)
    {
        return $this->andWhere(['folder_id' => $id]);
    }

    /**
     * @param int $id
     *
     * @return UserChatFolderQuery
     */
    public function byChat($id)
    {
        return $this->andWhere(['chat_id' => $id]);
    }
}
