<?php

namespace app\modules\chat\models;

use app\modules\users\models\User;
use Yii;

/**
 * This is the model class for table "users_chats".
 *
 * @property int $user_id
 * @property int $chat_id
 * @property string $mark
 *
 * @property \app\modules\chat\models\Chat $chat
 * @property \app\modules\users\model\User $user
 */
class UserChats extends \yii\db\ActiveRecord
{
    const SCENARIO_CREATE = 'createUserChat';
    
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'users_chats';
    }

    public function scenarios()
    {
        return [
            self::SCENARIO_CREATE => ['user_id', 'chat_id', 'mark'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['user_id', 'chat_id'], 'required'],
            [['user_id', 'chat_id'], 'integer'],
            [['mark'], 'string', 'max' => 255],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'user_id' => Yii::t('app', 'User ID'),
            'chat_id' => Yii::t('app', 'Chat ID'),
            'mark' => Yii::t('app', 'Mark'),
        ];
    }

    public function extraFields()
    {
        return [
            'chat',
            'user',
        ];
    }

    /**
     * @return \app\modules\chat\models\Chat
     */
    public function getChat()
    {
        return $this->hasOne(Chat::className(), ['id' => 'chat_id']);
    }

    /**
     * @return \app\modules\users\models\User
     */
    public function getUser()
    {
        return $this->hasOne(User::className(), ['id' => 'user_id']);
    }

    /**
     * @inheritdoc
     * @return UsersChatsQuery the active query used by this AR class.
     */
    public static function find()
    {
        return new UsersChatsQuery(get_called_class());
    }
}
