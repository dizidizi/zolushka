<?php

namespace app\modules\chat\models;

use app\core\base\CinderellaModel;
use app\modules\users\models\User;
use Yii;

/**
 * This is the model class for table "user_folders".
 *
 * @property int $id
 * @property int $user_id
 * @property string $name
 * 
 * @property \app\modules\users\models\User $user
 */
class UserFolder extends CinderellaModel
{
    const SCENARIO_CREATE = 'createFolder';
    const SCENARIO_EDIT = 'editFolder';
    
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'user_folders';
    }

    public function scenarios()
    {
        return [
            self::SCENARIO_CREATE => ['user_id', 'name'],
            self::SCENARIO_EDIT => ['name'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['user_id', 'name'], 'required'],
            [['user_id'], 'integer'],
            [['name'], 'string', 'max' => 255],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'user_id' => Yii::t('app', 'User ID'),
            'name' => Yii::t('app', 'Name'),
        ];
    }

    public function extraFields()
    {
        return [
            'user'
        ];
    }

    public function getUser()
    {
        return $this->hasOne(User::className(), ['id' => 'user_id']);
    }

    /**
     * @inheritdoc
     * @return UserFolderQuery the active query used by this AR class.
     */
    public static function find()
    {
        return new UserFolderQuery(get_called_class());
    }
}
