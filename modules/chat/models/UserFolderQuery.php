<?php

namespace app\modules\chat\models;

/**
 * This is the ActiveQuery class for [[UserFolder]].
 *
 * @see UserFolder
 */
class UserFolderQuery extends \yii\db\ActiveQuery
{
    /*public function active()
    {
        return $this->andWhere('[[status]]=1');
    }*/

    /**
     * @inheritdoc
     * @return UserFolder[]|array
     */
    public function all($db = null)
    {
        return parent::all($db);
    }

    /**
     * @inheritdoc
     * @return UserFolder|array|null
     */
    public function one($db = null)
    {
        return parent::one($db);
    }

    /**
     * @param int $id
     *
     * @return UserFolderQuery
     */
    public function byId($id)
    {
        return $this->andWhere(['id' => $id]);
    }

    /**
     * @param int $id
     *
     * @return UserFolderQuery
     */
    public function byUser($id)
    {
        return $this->andWhere(['user_id' => $id]);
    }
}
