<?php

namespace app\modules\chat\models;

/**
 * This is the ActiveQuery class for [[UserChats]].
 *
 * @see UserChats
 */
class UsersChatsQuery extends \yii\db\ActiveQuery
{
    /*public function active()
    {
        return $this->andWhere('[[status]]=1');
    }*/

    /**
     * @inheritdoc
     * @return UserChats[]|array
     */
    public function all($db = null)
    {
        return parent::all($db);
    }

    /**
     * @inheritdoc
     * @return UserChats|array|null
     */
    public function one($db = null)
    {
        return parent::one($db);
    }

    /**
     * @param int $userId
     *
     * @return UsersChatsQuery
     */
    public function byUser($userId)
    {
        return $this->andWhere(["user_id" => $userId]);
    }

    /**
     * @param int[] $userIds
     *
     * @return UsersChatsQuery
     */
    public function byUsers($userIds)
    {
        return $this->andWhere(["user_id" => $userIds]);
    }

    /**
     * @param int $chatId
     *
     * @return UsersChatsQuery
     */
    public function byChat($chatId)
    {
        return $this->andWhere(["chat_id" => $chatId]);
    }

    /**
     * @param string $mark
     *
     * @return UsersChatsQuery
     */
    public function byMark($mark)
    {
        return $this->andWhere(["mark" => $mark]);
    }
    
    /**
     * @return UsersChatsQuery
     */
    public function newest()
    {
        return $this->joinWith(['chat as c'])
            ->addOrderBy("c.created_at desc");
    }
}
