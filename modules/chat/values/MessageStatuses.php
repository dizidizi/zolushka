<?php
/**
 * Created by PhpStorm.
 * User: JimmDiGriz
 * Date: 02.05.2017
 * Time: 8:07
 */

namespace app\modules\chat\values;


class MessageStatuses
{
    public static $list = [
        self::READ,
        self::UNREAD
    ];
    
    const READ = 1;
    const UNREAD = 2;
}