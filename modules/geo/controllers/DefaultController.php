<?php

namespace app\modules\geo\controllers;

use app\modules\geo\models\Region;
use yii\helpers\Json;
use yii\web\Controller;

/**
 * Default controller for the `geo` module
 */
class DefaultController extends Controller
{
    public function actionRegions($selectedId = null) {
        if (isset($_POST['depdrop_parents'])) {
            $parents = $_POST['depdrop_parents'];
            if ($parents != null) {
                $countryId = $parents[0];
                $out = Region::find()->byCountry($countryId)->all();
                
                echo Json::encode(['output' => $out, 'selected' => $selectedId ? $selectedId : '']);
                return;
            }
        }
        
        echo Json::encode(['output' => '', 'selected' => '']);
    }
}
