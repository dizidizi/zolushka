<?php

namespace app\modules\geo\models;

/**
 * This is the ActiveQuery class for [[Region]].
 *
 * @see Region
 */
class RegionsQuery extends \yii\db\ActiveQuery
{
    /*public function active()
    {
        return $this->andWhere('[[status]]=1');
    }*/

    /**
     * @inheritdoc
     * @return Region[]|array
     */
    public function all($db = null)
    {
        return parent::all($db);
    }

    /**
     * @inheritdoc
     * @return Region|array|null
     */
    public function one($db = null)
    {
        return parent::one($db);
    }
    
    /**
     * @param int $countryId
     * 
     * @return RegionsQuery
     */
    public function byCountry($countryId)
    {
        return $this->andWhere(['country_id' => $countryId]);
    }
}
