<?php
/**
 * Created by PhpStorm.
 * User: JimmDiGriz
 * Date: 3/20/2017
 * Time: 8:22 PM
 */

namespace app\modules\photos\controllers;


use app\core\base\JsonApiController;
use app\core\components\AvatarManager;
use app\modules\photos\models\Photos;
use app\modules\users\models\User;
use app\rbac\Roles;
use yii\base\ErrorException;
use yii\base\InvalidParamException;
use yii\filters\AccessControl;
use yii\helpers\ArrayHelper;
use yii\helpers\VarDumper;
use yii\web\ForbiddenHttpException;
use yii\web\NotFoundHttpException;
use yii\db\Expression;

class ApiController extends JsonApiController
{
    public $modelClass = Photos::class;
    public $documentPath = 'upload/original/';
    
    private $allowedTypes = [
        'image/jpeg',
        'image/png',
    ];

    public function behaviors()
    {
        return ArrayHelper::merge(parent::behaviors(), [
            'access' => [
                'class' => AccessControl::className(),
                'only' => ['upload', 'move', 'delete'],
                'rules' => [
                    [
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
        ]);
    }

    public function verbs()
    {
        $verbs = parent::verbs();
        $verbs["upload"] = ['POST'];
        $verbs["move"] = ['PUT'];
        $verbs["delete"] = ['DELETE'];
        return $verbs;
    }
    
    public function actionGallery($userId)
    {
        $user = User::find()->byId($userId)->one();
        
        if (!$user || count($user->photos) == 0) {
            throw new NotFoundHttpException();
        }
        
        return array_map(function ($item) {
            $item->filename = $item->getUrl();
            return $item;
        }, $user->photos);
    }

    public function actionUpload()
    {
        $required = [
            'user_id' => true,
            'set_avatar' => false,
            'set_cover' => false,
        ];
        
        $params = $this->getParams($required);
        
        $user = User::find()->byId($params['user_id'])->one();
        
        if (!$user) {
            throw new NotFoundHttpException();
        }
        
        if (!\Yii::$app->user->is(Roles::ADMIN) && $user->id != \Yii::$app->user->identity->id) {
            throw new ForbiddenHttpException();
        }
        
        if ((isset($params['set_avatar']) || isset($params['set_cover'])) && count($_FILES) > 1) {
            throw new InvalidParamException();
        }
        
        if (!isset($params['set_avatar']) && !isset($params['set_cover']) && $user->getPhotos()->count() >= 10) {
            throw new ForbiddenHttpException();
        }
        
        if (isset($_FILES['file'])) {
            $files = [];
            
            $count = count($_FILES['file']['name']);
            
            foreach ($_FILES['file'] as $name => $value) {
                for ($i = 0; $i < $count; $i++) {
                    $files[$i][$name] = $_FILES['file'][$name][$i];
                }
            }
            
            $_FILES = $files;
        }
        
        $models = [];
        
        $order = 0;
        
        $i = 0;
        foreach ($_FILES as $file) {
            if (!in_array($file['type'], $this->allowedTypes)) {
                continue;
            }
            
            $extension = substr($file['name'], strrpos($file['name'], '.'));

            $name = md5(time() . $params['user_id'] . $i) .$extension;

            $path = $this->documentPath . $name;
            
            copy($file['tmp_name'], $path);

            $photo = new Photos(Photos::SCENARIO_DEFAULT);

            $photo->user_id = $user->id;
            $photo->filename = $name;

            /**@var \app\modules\photos\models\Photos[] $photos*/
            if ($order == 0) {
                $order = Photos::find()->byUser($user->id)->max('`order`');
            }
            
            $order += 1;
    
            $photo->order = $order;

            $photo->created_at = new Expression('NOW()');
    
            if (!$photo->save()) {
                throw new ErrorException(json_encode($photo->getErrors()));
            }

            if (isset($params['set_avatar'])) {
                $user->avatar_id = $photo->id;
                $user->avatar_src = AvatarManager::getAvatarForUser($user);
                
                $user->setScenario(User::SCENARIO_AVATAR_UPLOAD);

                $user->save();
            } else if (isset($params['set_cover'])) {
                $user->cover_id = $photo->id;
                
                $user->setScenario(user::SCENARIO_COVER_UPLOAD);
                
                $user->save();
            }

            $models[] = $photo;
            $i++;
        }
        
        return $models;
    }
    
    public function actionMove()
    {
        $required = [
            'id' => true,
            'position' => true,
        ];
        
        $params = $this->getParams($required);
        
        $photo = Photos::find()->byId($params['id'])->one();
        
        if (!$photo) {
            throw new NotFoundHttpException('PhotoNotFound');
        }

        if (!\Yii::$app->user->is(Roles::ADMIN) && $photo->user_id != \Yii::$app->user->identity->id) {
            throw new ForbiddenHttpException();
        }
        
        $targetPhoto = Photos::find()->byUser($photo->user_id)->byPosition($params['position'])->one();
        
        if (!$targetPhoto) {
            throw new NotFoundHttpException('TargetPhotoNotFound');
        }
        
        $tempPosition = $targetPhoto->order;
        
        if ($targetPhoto->order > $photo->order) {
            $targetPhoto->order--;
        } else if ($targetPhoto->order < $photo->order) {
            $targetPhoto->order++;
        }
        
        $photo->order = $tempPosition;
        
        $photo->setScenario(Photos::SCENARIO_MOVE);
        $targetPhoto->setScenario(Photos::SCENARIO_MOVE);
        
        Photos::updateAllCounters(['order' => 1], 'user_id = :user_id and `order` > :order', [
            ':user_id' => $photo->user_id,
            ':order' => $tempPosition
        ]);
        
        return $targetPhoto->save() && $photo->save();
    }
    
    public function actionDelete()
    {
        $required = [
            'id' => true,
        ];
        
        $params = $this->getParams($required);
        
        $photo = Photos::find()->byId($params['id'])->one();
        
        if ($photo) {
            if (!\Yii::$app->user->is(Roles::ADMIN) && $photo->user_id != \Yii::$app->user->identity->id) {
                throw new ForbiddenHttpException();
            }
            
            $photo->delete();
        }
        
        return true;
    }
}