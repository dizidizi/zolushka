<?php

namespace app\modules\photos\models;

use Yii;

/**
 * This is the model class for table "photos".
 *
 * @property int $id
 * @property int $user_id
 * @property string $filename
 * @property string $created_at
 * @property int $order
 */
class Photos extends \app\core\base\CinderellaModel
{
    const SCENARIO_MOVE = 'modePhoto';
    
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'photos';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['user_id', 'filename'], 'required'],
            [['user_id', 'order'], 'integer'],
            [['created_at'], 'safe'],
            [['filename'], 'string', 'max' => 255],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'user_id' => Yii::t('app', 'User ID'),
            'filename' => Yii::t('app', 'Filename'),
            'created_at' => Yii::t('app', 'Created At'),
            'order' => Yii::t('app', 'Order'),
        ];
    }

    public function scenarios()
    {
        return [
            self::SCENARIO_DEFAULT => ['user_id', 'filename', 'order'],
            self::SCENARIO_MOVE => ['order'],
        ];
    }

    /**
     * @inheritdoc
     * @return PhotosQuery the active query used by this AR class.
     */
    public static function find()
    {
        return new PhotosQuery(get_called_class());
    }
    
    public function getUrl()
    {
        if (isset(Yii::$app->params['image_src_prefix'])) {
            return Yii::$app->params['image_src_prefix'] . '/upload/original/' . $this->filename;
        }
        
        return '/upload/original/' . $this->filename;
    }
    
    public function moveTo($position)
    {
        
    }
    
    public static function create($params)
    {
        
    }
}
