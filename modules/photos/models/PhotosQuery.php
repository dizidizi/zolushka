<?php

namespace app\modules\photos\models;

/**
 * This is the ActiveQuery class for [[Photos]].
 *
 * @see Photos
 */
class PhotosQuery extends \yii\db\ActiveQuery
{
    /*public function active()
    {
        return $this->andWhere('[[status]]=1');
    }*/

    /**
     * @inheritdoc
     * @return Photos[]|array
     */
    public function all($db = null)
    {
        return parent::all($db);
    }

    /**
     * @inheritdoc
     * @return Photos|array|null
     */
    public function one($db = null)
    {
        return parent::one($db);
    }

    /**
     * @param int $id
     *
     * @return PhotosQuery
     */
    public function byId($id)
    {
        return $this->andWhere(['id' => $id]);
    }

    /**
     * @param int $id
     *
     * @return PhotosQuery
     */
    public function byUser($id)
    {
        return $this->andWhere(['user_id' => $id]);
    }

    /**
     * @inheritdoc
     * @return PhotosQuery
     */
    public function ordered()
    {
        return $this->addOrderBy('order');
    }

    /**
     * @param int $position
     *
     * @return PhotosQuery
     */
    public function byPosition($position)
    {
        return $this->andWhere(['order' => $position]);
    }
}
