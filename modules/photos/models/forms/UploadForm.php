<?php

/**
 * Created by PhpStorm.
 * User: JimmDiGriz
 * Date: 3/19/2017
 * Time: 8:54 PM
 */

namespace app\modules\photos\models\forms;

use app\modules\photos\models\Photos;
use app\modules\users\models\User;
use Yii;
use yii\base\Model;
use yii\helpers\FileHelper;
use yii\helpers\Json;
use yii\imagine\Image;
use Imagine\Image\Box;
use Imagine\Image\Point;
use yii\web\UploadedFile;

/**All this is temp*/
class UploadForm extends Model
{
    /**
     * @var UploadedFile
     */
    public $file;
    /**@var string $crop_info*/
    public $crop_info;
    /**@var int $userId*/
    public $userId;
    
    /**@var \app\modules\users\models\User $user*/
    private $user;

    public function rules()
    {
        return [
            [['file'], 'image', 'skipOnEmpty' => false, 'extensions' => 'png, jpg', 'mimeTypes' => ['image/jpeg','image/png']],
            ['crop_info', 'safe'],
            ['userId', 'required'],
        ];
    }

    public function upload()
    {
        if (!$this->validate()) {
            return false;
        }
        
        $this->user = User::find()->byId($this->userId)->one();
        
        if (!$this->user) {
            $this->addError('userId', 'NotFound');
            return false;
        }

        return $this->createPhotoModel($this->uploadFile());
    }
    
    private function createPhotoModel($name) 
    {
        $photo = new Photos(Photos::SCENARIO_DEFAULT);
        
        $photo->user_id = $this->userId;
        $photo->filename = $name;
        
        /**@var \app\modules\photos\models\Photos[] $photos*/
        $photos = $this->user->getPhotos()->all();
        $order = 0;
        
        if ($photos && count($photos) > 0) {
            $order = $photos[0]->order + 1;
        }
        
        $photo->order = $order;
        
        if (!$photo->save()) {
            $this->addErrors($photo->getErrors());
            return false;
        }
        
        $this->user->avatar_id = $photo->id;
        
        $this->user->setScenario(User::SCENARIO_AVATAR_UPLOAD);
        
        $this->user->save();
        
        return true;
    }
    
    private function uploadFile()
    {
        $image = Image::getImagine()->open($this->file->tempName);

        $cropInfo = Json::decode($this->crop_info)[0];
        $cropInfo['dWidth'] = (int)$cropInfo['dWidth']; //new width image
        $cropInfo['dHeight'] = (int)$cropInfo['dHeight']; //new height image

        //saving thumbnail
        $newSizeThumb = new Box($cropInfo['dWidth'], $cropInfo['dHeight']);
        $cropSizeThumb = new Box(200, 200); //frame size of crop
        $cropPointThumb = new Point($cropInfo['x'], $cropInfo['y']);
        
        $name =  md5(time() . $this->userId) . '.' . $this->file->getExtension();
        
        $pathThumbImage = 'upload/original/' . $name;

        $image->resize($newSizeThumb)
            ->crop($cropPointThumb, $cropSizeThumb)
            ->save($pathThumbImage, ['quality' => 100]);
        
        return $name;
    }
}