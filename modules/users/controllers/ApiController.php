<?php
/**
 * Created by PhpStorm.
 * User: ICS Developer User
 * Date: 17.03.2017
 * Time: 15:07
 */

namespace app\modules\users\controllers;


use app\core\base\JsonApiController;
use app\core\components\AvatarManager;
use app\modules\users\models\User;
use app\modules\users\models\UserComment;
use yii\data\ActiveDataProvider;
use yii\filters\AccessControl;
use yii\helpers\ArrayHelper;
use yii\web\ForbiddenHttpException;
use yii\web\HttpException;
use yii\web\NotFoundHttpException;

class ApiController extends JsonApiController
{
    public $modelClass = 'app\modules\users\models\User';

    public function behaviors()
    {
        return ArrayHelper::merge(parent::behaviors(), [
            'private' => [
                'class' => AccessControl::className(),
                'only' => ['online', 'offline', 'auth'],
                'rules' => [
                    [
                        'allow' => true,
                        'ips' => ['127.0.0.1'],
                    ],
                ],
            ],
            'public' => [
                'class' => AccessControl::className(),
                'only' => ['view', 'index', 'comment', 'avatar', 'comments'],
                'rules' => [
                    [
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ]
        ]);
    }

    public function verbs()
    {
        $verbs = parent::verbs();
        
        $verbs["online"] = ['PUT'];
        $verbs["offline"] = ['PUT'];
        $verbs["auth"] = ['POST'];
        $verbs["comment"] = ['POST'];
        $verbs["comments"] = ['GET'];
        
        return $verbs;
    }
    
    /**
     * @throws NotFoundHttpException|HttpException
     * 
     * @return bool
     */
    public function actionOnline()
    {
        $required = [
            'userId' => true,
        ];
        
        $params = $this->getParams($required);
        
        $user = User::find()
            ->byId($params['userId'])
            ->one();
        
        if (!$user) {
//            return false;
            throw new NotFoundHttpException("UserWithId[{$params['userId']}]NotFound");
        }
        
        if ($user->is_online) {
            return $user;
        }
        
        $user->setScenario(User::SCENARIO_ONLINE_STATUS);
        
        $user->is_online = true;
        
        if (!$user->save()) {
//            return false;
            throw new HttpException(500, json_encode($user->getErrors()));
        }

        $user->touch('updated_at');
        
        return $user;
    }

    /**
     * @throws NotFoundHttpException|HttpException
     *
     * @return bool
     */
    public function actionOffline()
    {
        $required = [
            'userId' => true,
        ];

        $params = $this->getParams($required);
        
        $user = User::find()
            ->byId($params['userId'])
            ->one();

        if (!$user) {
//            return false;
            throw new NotFoundHttpException("UserWithId[{$params['userId']}]NotFound");
        }

        if (!$user->is_online) {
            return $user;
        }

        $user->setScenario(User::SCENARIO_ONLINE_STATUS);

        $user->is_online = false;

        if (!$user->save()) {
//            return false;
            throw new HttpException(500, json_encode($user->getErrors()));
        }
        
        $user->touch('updated_at');

        return $user;
    }
    
    /**
     * @throws NotFoundHttpException|ForbiddenHttpException
     * 
     * @return bool
     */
    public function actionAuth()
    {
        $required = [
            'id' => true,
            'accessToken' => true,
        ];

        $params = $this->getParams($required);
        
        $user = User::find()
            ->byId($params['id'])
            ->one();

        if (!$user) {
            throw new NotFoundHttpException("UserWithId[{$params['id']}]NotFound");
        }
        
        if (!$user->validateAuthKey($params['accessToken'])) {
            throw new ForbiddenHttpException();
        }
        
        return $user;
    }
    
    public function actionComments($userId = null) 
    {
        $this->modelClass = UserComment::className();
        
        $model = UserComment::find();
        
        if ($userId) {
            $model->byTarget($userId);
        }

        $provider = new ActiveDataProvider([
            'query' => $model,
            'pagination' => false,
        ]);

        return $provider;
    }
    
    public function actionComment()
    {
        $required = [
            'user_id' => true,
            'message' => true,
        ];

        $params = $this->getParams($required);

        $user = User::find()
            ->byId($params['user_id'])
            ->one();

        if (!$user) {
            throw new NotFoundHttpException("UserWithId[{$params['id']}]NotFound");
        }
        
        $result = UserComment::comment($user, $params['message']);
        
        if (!$result) {
            throw new HttpException(500, 'SavingFailed', 500);
        }
        
        return $result;
    }
    
    public function actionAvatar($userId)
    {
        $user = User::find()
            ->byId($userId)
            ->one();

        if (!$user) {
            throw new NotFoundHttpException("UserWithId[{$userId}]NotFound");
        }
        
        return [
            'avatar' => AvatarManager::getAvatarForUser($user),
        ];
    }
}