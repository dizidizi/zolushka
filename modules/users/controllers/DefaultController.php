<?php

namespace app\modules\users\controllers;

use app\modules\geo\models\Country;
use app\modules\geo\models\Region;
use app\modules\users\helpers\Values;
use app\modules\users\models\forms\LoginForm;
use app\modules\users\models\User;
use app\modules\users\models\UsersSearch;
use app\modules\users\values\Gender;
use Yii;
use yii\data\ActiveDataProvider;
use yii\filters\AccessControl;
use yii\helpers\ArrayHelper;
use yii\helpers\VarDumper;
use yii\web\Controller;
use yii\web\NotFoundHttpException;

/**
 * Default controller for the `users` module
 */
class DefaultController extends Controller
{
    public function behaviors()
    {
        return ArrayHelper::merge(parent::behaviors(), [
            'access' => [
                'class' => AccessControl::className(),
                'only' => ['settings', 'logout'],
                'rules' => [
                    [
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
        ]);
    }
    
    public function actionIndex()
    {
//        VarDumper::export(Yii::$app->user->isGuest); die();
        $query = User::find()->joinWith(['avatar']);

        if (!Yii::$app->user->isGuest && Yii::$app->user->identity->gender == Gender::FEMALE) {
            $query->male();
        } else {
            $query->female();
        }

        $query->newestRegister();

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
            'pagination' => [
                'pageSize' => 20,
            ],
        ]);

        return $this->render('index', Values::mergeWith([
            'dataProvider' => $dataProvider,
            'countries' => Country::map(Country::find()->all()),
        ]));
    }

    public function actionSearch()
    {
        $dataProvider = (new UsersSearch())->search(Yii::$app->request->get());

        $dataProvider->setPagination([
            'pageSize' => 20,
        ]);

        return $this->render('search', Values::mergeWith([
            'dataProvider' => $dataProvider,
            'searchState' => Yii::$app->request->get(),
            'countries' => Country::map(Country::find()->all()),
        ]));
    }
    
    public function actionView($userId)
    {
        $user = User::find()
            ->active()
            ->byId($userId)
            ->with(['avatar', 'cover', 'purposes', 'sexTypes', 'country', 'region'])
            ->one();
        
        if (!$user) {
            throw new NotFoundHttpException();
        }
        
        return $this->render('view', [
            'user' => $user,
        ]);
    }
    
    public function actionSettings()
    {
        if (\Yii::$app->request->post()) {
            return User::saveFromPost(Yii::$app->user->identity);
        }
        
        return $this->render('settings', Values::mergeWith([
            'user' => Yii::$app->user->identity,
            'countries' => Country::map(Country::find()->all()),
        ]));
    }

    public function actionRegister()
    {
        /**@todo: passwords saves incorrect, fix*/
        if (!Yii::$app->user->isGuest) {
            return $this->goHome();
        }

        $model = new User(['scenario' => User::SCENARIO_REGISTER]);

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            Yii::$app->user->login($model, 3600*24*30);

            return $this->goBack();
        }

        return $this->render('register', [
            'model' => $model,
        ]);
    }

    public function actionLogin()
    {
        if (!Yii::$app->user->isGuest) {
            return $this->goHome();
        }

        $model = new LoginForm();

        if ($model->load(Yii::$app->request->post()) && $model->login()) {
            return $this->goBack();
        }

        return $this->render('login', [
            'model' => $model,
        ]);
    }

    /**
     * Logout action.
     *
     * @return string
     */
    public function actionLogout()
    {
        Yii::$app->user->logout();

        return $this->goHome();
    }
}
