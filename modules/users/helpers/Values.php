<?php
/**
 * Created by PhpStorm.
 * User: JimmDiGriz
 * Date: 22.03.2017
 * Time: 11:37
 */

namespace app\modules\users\helpers;


use app\modules\users\values\Complexion;
use app\modules\users\values\EyesColor;
use app\modules\users\values\Gender;
use app\modules\users\values\HairColor;
use app\modules\users\values\Look;
use app\modules\users\values\Purposes;
use app\modules\users\values\SexPeriodicity;
use app\modules\users\values\SexRole;
use app\modules\users\values\SexTypes;
use app\modules\users\values\Zodiac;
use app\rbac\Roles;
use yii\helpers\ArrayHelper;

class Values
{
    private static $values = null;

    /**
     * @return array
     */
    public static function getAllValues()
    {
        if (!self::$values) {
            self::$values = [
                'hairList' => HairColor::$list,
                'zodiacList' => Zodiac::$list,
                'eyesList' => EyesColor::$list,
                'lookList' => Look::$list,
                'complexionList' => Complexion::$list,
                'purposesList' => Purposes::$list,
                'sexTypesList' => SexTypes::$list,
                'sexRoleList' => SexRole::$list,
                'sexPeriodicityList' => SexPeriodicity::$list,
                'genderList' => [Gender::FEMALE, Gender::MALE],
                'groupList' => Roles::$roles,
            ];
        }

        return self::$values;
    }

    /**
     * @param array $source
     *
     * @return array
     */
    public static function mergeWith($source)
    {
        return ArrayHelper::merge($source, self::getAllValues());
    }
}