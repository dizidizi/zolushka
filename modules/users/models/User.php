<?php

namespace app\modules\users\models;

use app\core\base\CinderellaModel;
use app\core\behaviors\CryptFieldsBehavior;
use app\modules\chat\models\Chat;
use app\modules\geo\models\Country;
use app\modules\geo\models\Region;
use app\modules\photos\models\Photos;
use app\modules\users\values\Complexion;
use app\modules\users\values\EyesColor;
use app\modules\users\values\Gender;
use app\modules\users\values\HairColor;
use app\modules\users\values\Look;
use app\modules\users\values\SexPeriodicity;
use app\modules\users\values\SexRole;
use app\modules\users\values\Zodiac;
use app\rbac\Roles;
use Yii;
use yii\behaviors\TimestampBehavior;
use yii\db\Expression;
use yii\helpers\VarDumper;
use yii\web\IdentityInterface;

/**
 * This is the model class for table "users".
 *
 * @property int $id
 * @property string $email
 * @property string $password
 * @property string $access_token
 * @property string $group
 * @property string $name
 * @property int $age
 * @property string $gender
 * @property string $zodiac
 * @property string $look
 * @property int $country_id
 * @property int $region_id
 * @property string $information
 * @property int $height
 * @property int $weight
 * @property string $eyes_color
 * @property string $hair_color
 * @property int $bust_size
 * @property int $created_at
 * @property int $updated_at
 * @property string $complexion
 * @property string $sex_periodicity
 * @property string $sex_role
 * @property int $avatar_id
 * @property int $cover_id
 * @property bool $is_online
 * @property string $avatar_src
 *
 * @property \app\modules\photos\models\Photos $avatar
 * @property \app\modules\photos\models\Photos $cover
 * @property \app\modules\photos\models\Photos[] $photos
 * @property \app\modules\users\models\UserPurposes[] $purposes
 * @property \app\modules\users\models\UserSexTypes[] $sexTypes
 * @property \app\modules\geo\models\Country $country
 * @property \app\modules\geo\models\Region $region
 * @property \app\modules\chat\models\Chat[] $chats
 */
class User extends CinderellaModel implements IdentityInterface
{
    const SCENARIO_REGISTER = 'register';
    const SCENARIO_ADMIN_EDIT_FEMALE = 'adminEitFemale';
    const SCENARIO_ADMIN_EDIT_MALE = 'adminEditMale';
    const SCENARIO_ADMIN_CREATE = 'adminCreate';
    const SCENARIO_AVATAR_UPLOAD = 'avatarUpload';
    const SCENARIO_COVER_UPLOAD = 'coverUpload';
    const SCENARIO_ONLINE_STATUS = 'onlineStatusScenario';

    /**@var string $authKey*/
    private $authKey;
    
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'users';
    }
    
    public function fields() {
        $fields = array_flip($this->attributes());
        
        unset($fields['password'], $fields['access_token']);
        
        return array_keys($fields);;
    }

    public function scenarios()
    {
        return [
            self::SCENARIO_REGISTER => ['email', 'password', 'gender', 'name', 'age'],
            self::SCENARIO_ADMIN_EDIT_FEMALE => ['email', 'gender', 'name', 'age', 
                'group', 'zodiac', 'look', 'country_id', 
                'region_id', 'information', 'height', 'weight',
                'eyes_color', 'hair_color', 'bust_size', 'complexion', 'sex_periodicity',
                'sex_role'],
            self::SCENARIO_ADMIN_EDIT_MALE => ['email', 'gender', 'name', 'age',
                'group', 'zodiac', 'country_id', 'region_id', 'information',
                'height', 'weight', 'eyes_color', 'hair_color', 'complexion',
                'sex_periodicity', 'sex_role'],
            self::SCENARIO_ADMIN_CREATE => [
                'email', 'gender', 'name', 'age', 'group', 'password',
                'zodiac', 'country_id', 'region_id', 'information', 'height',
                'weight', 'eyes_color', 'hair_color', 'complexion',
                'sex_periodicity', 'sex_role',
            ],
            self::SCENARIO_AVATAR_UPLOAD => ['avatar_id', 'avatar_src'],
            self::SCENARIO_COVER_UPLOAD => ['cover_id'],
            self::SCENARIO_ONLINE_STATUS => ['is_online'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['email', 'password', 'name'], 'required'],
            [['age', 'height', 'weight', 'bust_size', 'avatar_id', 'cover_id', 'country_id', 'region_id'], 'integer'],
            [['information'], 'string'],
            [['email', 'password', 'access_token', 'group', 'name', 'look', 'purpose', 'preferences', 'eyes_color', 'hair_color'], 'string', 'max' => 255],
            [['email'], 'unique'],
            [['age'], 'number', 'min' => 18, 'max' => 100],
            [['gender'], 'in', 'range' => [Gender::FEMALE, Gender::MALE]],
            [['zodiac'], 'in', 'range' => Zodiac::$list],
            [['group'], 'in', 'range' => Roles::$roles],
            [['email'], 'email'],
            [['height'], 'number', 'min' => 150, 'max' => 220],
            [['wight'], 'number', 'min' => 30, 'max' => 200],
            [['bust_size'], 'number', 'min' => 1, 'max' => 5],
            [['look'], 'in', 'range' => Look::$list],
            [['eyes_color'], 'in', 'range' => EyesColor::$list],
            [['hair_color'], 'in', 'range' => HairColor::$list],
            [['complexion'], 'in', 'range' => Complexion::$list],
            [['sex_role'], 'in', 'range' => SexRole::$list],
            [['sex_periodicity'], 'in', 'range' => SexPeriodicity::$list],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'email' => 'Email',
            'password' => 'Password',
            'access_token' => 'Access Token',
            'group' => 'Group',
            'name' => 'Name',
            'age' => 'Age',
            'gender' => 'Gender',
            'zodiac' => 'Zodiac',
            'look' => 'Look',
            'purpose' => 'Purpose',
            'preferences' => 'Preferences',
//            'country_id' => 'Country Id',
//            'region_id' => 'Region Id',
            'information' => 'Information',
            'height' => 'Height',
            'weight' => 'Weight',
            'eyes_color' => 'Eyes Color',
            'hair_color' => 'Hair Color',
            'bust_size' => 'Bust Size',
            'created_at' => 'Created At',
            'updated_at' => 'Updated At',
            'is_online' => 'Is Online',
            'avatar_src' => 'Avatar Src',
        ];
    }

    public function behaviors()
    {
        return [
            /**@todo: behavior touch created_at field on every update request, whyyyyy?*/
            'timestampBehavior' => [
                'class' => TimestampBehavior::className(),
                'updatedAtAttribute' => 'updated_at',
                'createdAtAttribute' => 'created_at',
                'value' => new Expression('NOW()'),
            ],
            'cryptBehavior' => [
                'class' => CryptFieldsBehavior::className(),
                'fields' => ['password'],
                'salt' => Yii::$app->params['salt'],
            ],
        ];
    }

    /**
     * @return \app\modules\users\models\UsersQuery
     */
    public static function find()
    {
        return new UsersQuery(get_called_class());
    }

    /**
     * Finds an identity by the given ID.
     * @param string|int $id the ID to be looked for
     * @return IdentityInterface the identity object that matches the given ID.
     * Null should be returned if such an identity cannot be found
     * or the identity is not in an active state (disabled, deleted, etc.)
     */
    public static function findIdentity($id)
    {
        return User::find()
            ->active()
            ->byId($id)
            ->one();
    }

    /**
     * Finds an identity by the given token.
     * @param mixed $token the token to be looked for
     * @param mixed $type the type of the token. The value of this parameter depends on the implementation.
     * For example, [[\yii\filters\auth\HttpBearerAuth]] will set this parameter to be `yii\filters\auth\HttpBearerAuth`.
     * @return IdentityInterface the identity object that matches the given token.
     * Null should be returned if such an identity cannot be found
     * or the identity is not in an active state (disabled, deleted, etc.)
     */
    public static function findIdentityByAccessToken($token, $type = null)
    {
        return User::find()
            ->active()
            ->byToken($token)
            ->one();
    }

    /**
     * Returns an ID that can uniquely identify a user identity.
     * @return string|int an ID that uniquely identifies a user identity.
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Returns a key that can be used to check the validity of a given identity ID.
     *
     * The key should be unique for each individual user, and should be persistent
     * so that it can be used to check the validity of the user identity.
     *
     * The space of such keys should be big enough to defeat potential identity attacks.
     *
     * This is required if [[User::enableAutoLogin]] is enabled.
     * @return string a key that is used to check the validity of a given identity ID.
     * @see validateAuthKey()
     */
    public function getAuthKey()
    {
        if (!$this->authKey) {
            $this->authKey = crypt("{$this->id}-{$this->email}", Yii::$app->params['salt']);
        }
        
        return $this->authKey;
    }

    /**
     * Validates the given auth key.
     *
     * This is required if [[User::enableAutoLogin]] is enabled.
     * @param string $authKey the given auth key
     * @return bool whether the given auth key is valid.
     * @see getAuthKey()
     */
    public function validateAuthKey($authKey)
    {
        return $this->getAuthKey() == $authKey;
    }

    /**
     * @param string $password
     *
     * @return bool
     */
    public function validatePassword($password)
    {
        return crypt($password, Yii::$app->params['salt']) == $this->password;
    }
    
    public function getAvatarSrc()
    {
        $avatar = $this->getAvatarModel();
        
        return $avatar ? $avatar->getUrl() : '';
    }

    public function getCoverSrc()
    {
        $cover = $this->getCoverModel();

        return $cover ? $cover->getUrl() : '';
    }

    public function extraFields()
    {
        return [
            'purposes',
            'sexTypes',
            'avatar',
            'cover',
            'photos',
            'country',
            'region',
            'chats',
        ];
    }
    
    /**
     * @return UserPurposesQuery
     */
    public function getPurposes()
    {
        return $this->hasMany(UserPurposes::className(), ['user_id' => 'id']);
    }

    /**
     * @return UserSexTypesQuery
     */
    public function getSexTypes()
    {
        return $this->hasMany(UserSexTypes::className(), ['user_id' => 'id']);
    }

    /**
     * @return \app\modules\photos\models\PhotosQuery
     */
    public function getPhotos()
    {
        $ids = [];
        
        if ($this->avatar_id) {
            $ids[] = $this->avatar_id;
        }
        
        if ($this->cover_id) {
            $ids[] = $this->cover_id;
        }
        
        return $this->hasMany(Photos::className(), ['user_id' => 'id'])
            ->orderBy('order')
            ->andWhere(['not in', 'id', $ids]);
    }

    /**
     * @return \app\modules\chat\models\Chat[]
     */
    public function getChats()
    {
        return $this->hasMany(Chat::className(), ['id' => 'chat_id'])
            ->viaTable('users_chats', ['user_id' => 'id']);
    }

    /**
     * @param string $mark
     * @return \app\modules\chat\models\Chat[]
     */
    public function getChatsByMark($mark)
    {
        return $this->hasMany(Chat::className(), ['id' => 'chat_id'])
            ->viaTable('users_chats', ['user_id' => 'id'])
            ->andOnCondition(['mark' => $mark]);
    }

    /**
     * @return \app\modules\geo\models\CountriesQuery
     */
    public function getCountry()
    {
        return $this->hasOne(Country::className(), ['id' => 'country_id']);
    }

    /**
     * @return \app\modules\geo\models\RegionsQuery
     */
    public function getRegion()
    {
        return $this->hasOne(Region::className(), ['id' => 'region_id']);
    }

    /**
     * @return \app\modules\photos\models\PhotosQuery
     */
    public function getAvatar()
    {
        return $this->hasOne(Photos::className(), ['id' => 'avatar_id']);
    }
    
    /**
     * @return Photos
     */
    public function getAvatarModel()
    {
        if (!$this->avatar_id) {
            return null;
        }

        if ($this->avatar) {
            return $this->avatar;
        }
        
        return Photos::find()->byId($this->avatar_id)->one();
    }

    /**
     * @return Photos
     */
    public function getCoverModel()
    {
        if (!$this->cover_id) {
            return null;
        }

        if ($this->cover) {
            return $this->cover;
        }

        return Photos::find()->byId($this->cover_id)->one();
    }

    /**
     * @return \app\modules\photos\models\PhotosQuery
     */
    public function getCover()
    {
        return $this->hasOne(Photos::className(), ['id' => 'cover_id']);
    }
    
    /**
     * @return bool
     */
    public function isFemale()
    {
        return $this->gender == Gender::FEMALE;
    }

    /**
     * @return bool
     */
    public function isMale()
    {
        return $this->gender == Gender::MALE;
    }

    /**
     * @param string $type
     * 
     * @return bool
     */
    public function containsSexType($type)
    {
        /**@var \app\modules\users\models\UserSexTypes[] $types*/
        $types = $this->sexTypes;

        if (!$types) {
            return false;
        }
        
        foreach ($types as $item) {
            if ($item->type == $type) {
                return true;
            }
        }
        
        return false;
    }

    /**
     * @param string $purpose
     * 
     * @return bool
     */
    public function containsPurpose($purpose)
    {
        /**@var \app\modules\users\models\UserPurposes[] $purposes*/
        $purposes = $this->purposes;

        if (!$purposes) {
            return false;
        }

        foreach ($purposes as $item) {
            if ($item->purpose == $purpose) {
                return true;
            }
        }

        return false;
    }
    
    /**
     * @param \app\modules\users\models\User $user
     * 
     * @return string|bool
     */
    public static function saveFromPost($user)
    {
        if ($user->isFemale()) {
            $user->setScenario(User::SCENARIO_ADMIN_EDIT_FEMALE);
        } else {
            $user->setScenario(User::SCENARIO_ADMIN_EDIT_MALE);
        }

        $post = array_map(function($item) {
            if ($item) {
                return $item;
            }
        }, \Yii::$app->request->post());

        $post = array_merge($user->attributes, $post);

        $user->setAttributes($post, false);

        if (isset(\Yii::$app->request->post()['password']) && !$user->isNewRecord) {
            $user->cryptFields();

            if (!$user->validate() || !$user->save()) {
                return json_encode($user->errors);
            }
        }

        UserPurposes::deleteAll(['user_id' => $user->id]);

        if ($post['purposes']) {
            foreach ($post['purposes'] as $purpose) {
                $model = new UserPurposes();

                $model->user_id = $user->id;
                $model->purpose = $purpose;

                $model->save();
            }
        }

        UserSexTypes::deleteAll(['user_id' => $user->id]);

        if ($post['types']) {
            foreach ($post['types'] as $type) {
                $model = new UserSexTypes();

                $model->user_id = $user->id;
                $model->type = $type;

                $model->save();
            }
        }

        if (!$user->validate() || !$user->save()) {
            return json_encode($user->errors);
        }

        return true;
    }
    
    /**
     * @return string
     */
    public function getTitle()
    {
        $result = $this->name;
        
        $result .= ", {$this->age} years";
        
        if ($this->zodiac) {
            $result .= ", {$this->zodiac}";
        }
        
        if ($this->region_id) {
            $region = $this->getRegion()->one()->name;
            
            $result .= ", {$region}";
        }
        
        return $result;
    }
}
