<?php

namespace app\modules\users\models;

use Yii;

/**
 * This is the model class for table "user_comments".
 *
 * @property int $id
 * @property int $from_user
 * @property int $to_user
 * @property string $comment
 * 
 * @property \app\modules\users\models\User $from
 * @property \app\modules\users\models\User $to
 */
class UserComment extends \yii\db\ActiveRecord
{
    const SCENARIO_CREATE = 'createUserComment';
    const SCENARIO_EDIT = 'editUserComment';
    
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'user_comments';
    }

    public function scenarios()
    {
        return [
            self::SCENARIO_CREATE => ['from_user', 'to_user', 'comment'],
            self::SCENARIO_EDIT => ['comment'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['from_user', 'to_user', 'comment'], 'required'],
            [['from_user', 'to_user'], 'integer'],
            [['comment'], 'string'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'from_user' => Yii::t('app', 'From User'),
            'to_user' => Yii::t('app', 'To User'),
            'comment' => Yii::t('app', 'Comment'),
        ];
    }

    /**
     * @inheritdoc
     * @return UserCommentsQuery the active query used by this AR class.
     */
    public static function find()
    {
        return new UserCommentsQuery(get_called_class());
    }

    public function extraFields()
    {
        return [
            'from',
            'to',
        ];
    }

    /**
     * @return \app\modules\users\models\User
     */
    public function getFrom()
    {
        return $this->hasOne(User::className(), ['id' => 'from_user']);
    }

    /**
     * @return \app\modules\users\models\User
     */
    public function getTo()
    {
        return $this->hasOne(User::className(), ['id' => 'to_user']);
    }
    
    /**
     * @param \app\modules\users\models\User $to
     * @param string $message
     * 
     * @return bool
     */
    public static function comment($to, $message) 
    {
        $comment = UserComment::find()
            ->byUser(Yii::$app->user->id)
            ->byTarget($to->id)
            ->one();
        
        if (!$comment) {
            $comment = new UserComment();
            
            $comment->from_user = Yii::$app->user->id;
            $comment->to_user = $to->id;
            
            $comment->setScenario(UserComment::SCENARIO_CREATE);
        } else {
            $comment->setScenario(UserComment::SCENARIO_EDIT);
        }
        
        $comment->comment = $message;
        
        return $comment->save();
    }
}
