<?php

namespace app\modules\users\models;

/**
 * This is the ActiveQuery class for [[UserComment]].
 *
 * @see UserComment
 */
class UserCommentsQuery extends \yii\db\ActiveQuery
{
    /*public function active()
    {
        return $this->andWhere('[[status]]=1');
    }*/

    /**
     * @inheritdoc
     * @return UserComment[]|array
     */
    public function all($db = null)
    {
        return parent::all($db);
    }

    /**
     * @inheritdoc
     * @return UserComment|array|null
     */
    public function one($db = null)
    {
        return parent::one($db);
    }

    /**
     * @param int $id
     *
     * @return UserCommentsQuery
     */
    public function byId($id)
    {
        return $this->andWhere(["id" => $id]);
    }

    /**
     * @param int $userId
     *
     * @return UserCommentsQuery
     */
    public function byUser($userId)
    {
        return $this->andWhere(["from_user" => $userId]);
    }

    /**
     * @param int $userId
     *
     * @return UserCommentsQuery
     */
    public function byTarget($userId)
    {
        return $this->andWhere(["to_user" => $userId]);
    }
}
