<?php

namespace app\modules\users\models;

use app\modules\users\values\Purposes;
use Yii;

/**
 * This is the model class for table "user_purposes".
 *
 * @property int $user_id
 * @property string $purpose
 */
class UserPurposes extends \app\core\base\CinderellaModel
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'user_purposes';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['user_id', 'purpose'], 'required'],
            [['user_id'], 'integer'],
            [['purpose'], 'string', 'max' => 255],
            [['purpose'], 'in', 'range' => Purposes::$list],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'user_id' => Yii::t('app', 'User ID'),
            'purpose' => Yii::t('app', 'Purpose'),
        ];
    }

    /**
     * @inheritdoc
     * @return UserPurposesQuery the active query used by this AR class.
     */
    public static function find()
    {
        return new UserPurposesQuery(get_called_class());
    }
    
    public function getUser()
    {
        return $this->hasOne(User::className(), ['user_id' => 'id']);
    }
}
