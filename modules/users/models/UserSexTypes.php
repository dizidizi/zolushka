<?php

namespace app\modules\users\models;

use app\modules\users\values\SexTypes;
use Yii;

/**
 * This is the model class for table "user_sex_types".
 *
 * @property int $user_id
 * @property string $type
 */
class UserSexTypes extends \app\core\base\CinderellaModel
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'user_sex_types';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['user_id', 'type'], 'required'],
            [['user_id'], 'integer'],
            [['type'], 'string', 'max' => 255],
            [['type'], 'in', 'range' => SexTypes::$list],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'user_id' => Yii::t('app', 'User ID'),
            'type' => Yii::t('app', 'Type'),
        ];
    }

    /**
     * @inheritdoc
     * @return UserSexTypesQuery the active query used by this AR class.
     */
    public static function find()
    {
        return new UserSexTypesQuery(get_called_class());
    }

    public function getUser()
    {
        return $this->hasOne(User::className(), ['user_id' => 'id']);
    }
}
