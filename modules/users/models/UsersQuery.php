<?php

namespace app\modules\users\models;
use app\modules\users\values\Gender;
use app\rbac\Roles;
use yii\db\ActiveQuery;

/**
 * This is the ActiveQuery class for [[User]].
 *
 * @see User
 */
class UsersQuery extends ActiveQuery
{
    private $tableName;

    public function __construct($modelClass, $config = [])
    {
        parent::__construct($modelClass, $config);

        $this->tableName = User::tableName(); //test
    }

    /**
     * @inheritdoc
     * @return UsersQuery
     */
    public function active()
    {
        return $this->andWhere(['<>', "{$this->tableName}.group", Roles::BANNED]);
    }

    /**
     * @inheritdoc
     * @return User[]|array
     */
    public function all($db = null)
    {
        return parent::all($db);
    }

    /**
     * @inheritdoc
     * @return User|array|null
     */
    public function one($db = null)
    {
        return parent::one($db);
    }

    /**
     * @inheritdoc
     * @return UsersQuery
     */
    public function byId($id)
    {
        return $this->andWhere(["{$this->tableName}.id" => $id]);
    }

    /**
     * @inheritdoc
     * @return UsersQuery
     */
    public function byToken($token)
    {
        return $this->andWhere(["{$this->tableName}.access_token" => $token]);
    }

    /**
     * @inheritdoc
     * @return UsersQuery
     */
    public function female()
    {
        return $this->andWhere(["{$this->tableName}.gender" => Gender::FEMALE]);
    }

    /**
     * @inheritdoc
     * @return UsersQuery
     */
    public function male()
    {
        return $this->andWhere(["{$this->tableName}.gender" => Gender::MALE]);
    }

    /**
     * @param string $gender
     *
     * @return UsersQuery
     */
    public function byGender($gender)
    {
        return $this->andWhere(["{$this->tableName}.gender" => $gender]);
    }

    /**
     * @param int $age
     *
     * @return UsersQuery
     */
    public function byAge($age)
    {
        return $this->andWhere(["{$this->tableName}.age" => $age]);
    }

    /**
     * @param int $height
     *
     * @return UsersQuery
     */
    public function byHeight($height)
    {
        return $this->andWhere(["{$this->tableName}.height" => $height]);
    }

    /**
     * @param int $weight
     *
     * @return UsersQuery
     */
    public function byWeight($weight)
    {
        return $this->andWhere(["{$this->tableName}.weight" => $weight]);
    }

    /**
     * @inheritdoc
     * @return UsersQuery
     */
    public function oldestRegister()
    {
        return $this->addOrderBy("{$this->tableName}.created_at");
    }

    /**
     * @inheritdoc
     * @return UsersQuery
     */
    public function orderById()
    {
        return $this->addOrderBy("{$this->tableName}.id desc");
    }

    /**
     * @inheritdoc
     * @return UsersQuery
     */
    public function newestRegister()
    {
        return $this->addOrderBy("{$this->tableName}.created_at desc");
    }

    /**
     * @inheritdoc
     * @return UsersQuery
     */
    public function clientsOnly()
    {
        return $this->andWhere(["{$this->tableName}.group" => [Roles::USER]]);
    }

    /**
     * @param string $email
     *
     * @return UsersQuery
     */
    public function byEmail($email)
    {
        return $this->andWhere(["{$this->tableName}.email" => $email]);
    }

    /**
     * @param int $start
     * @param int $end
     *
     * @return UsersQuery
     */
    public function betweenAge($start, $end)
    {
        return $this
            ->andWhere(['>=', "{$this->tableName}.age", $start])
            ->andWhere(['<=', "{$this->tableName}.age", $end]);
    }

    /**
     * @param int $age
     *
     * @return UsersQuery
     */
    public function youngerThen($age)
    {
        return $this->andWhere(['<=', "{$this->tableName}.age", $age]);
    }

    /**
     * @param int $age
     *
     * @return UsersQuery
     */
    public function olderThen($age)
    {
        return $this->andWhere(['>=', "{$this->tableName}.age", $age]);
    }

    /**
     * @param string $zodiac
     *
     * @return UsersQuery
     */
    public function byZodiac($zodiac)
    {
        return $this->andWhere(["{$this->tableName}.zodiac" => $zodiac]);
    }

    /**
     * @param int $size
     *
     * @return UsersQuery
     */
    public function byBustSize($size)
    {
        return $this->andWhere(["{$this->tableName}.bust_size" => $size]);
    }

    /**
     * @param int $height
     *
     * @return UsersQuery
     */
    public function higherThen($height)
    {
        return $this->andWhere(['>=', "{$this->tableName}.height", $height]);
    }

    /**
     * @param int $height
     *
     * @return UsersQuery
     */
    public function lowerThen($height)
    {
        return $this->andWhere(['<=', "{$this->tableName}.height", $height]);
    }

    /**
     * @param int $start
     * @param int $end
     *
     * @return UsersQuery
     */
    public function heightBetween($start, $end)
    {
        return $this
            ->andWhere(['<=', "{$this->tableName}.height", $end])
            ->andWhere(['>=', "{$this->tableName}.height", $start]);
    }

    /**
     * @param int $weight
     *
     * @return UsersQuery
     */
    public function heavierThen($weight)
    {
        return $this->andWhere(['>=', "{$this->tableName}.weight", $weight]);
    }

    /**
     * @param int $weight
     *
     * @return UsersQuery
     */
    public function easierThen($weight)
    {
        return $this->andWhere(['<=', "{$this->tableName}.weight", $weight]);
    }

    /**
     * @param int $start
     * @param int $end
     *
     * @return UsersQuery
     */
    public function weightBetween($start, $end)
    {
        return $this
            ->andWhere(['<=', "{$this->tableName}.weight", $end])
            ->andWhere(['>=', "{$this->tableName}.weight", $start]);
    }

    /**
     * @param int $look
     *
     * @return UsersQuery
     */
    public function byLook($look)
    {
        return $this->andWhere(["{$this->tableName}.look" => $look]);
    }

    /**
     * @param int $color
     *
     * @return UsersQuery
     */
    public function byEyesColor($color)
    {
        return $this->andWhere(["{$this->tableName}.eyes_color" => $color]);
    }

    /**
     * @param int $color
     *
     * @return UsersQuery
     */
    public function byHairColor($color)
    {
        return $this->andWhere(["{$this->tableName}.hair_color" => $color]);
    }

    /**
     * @param int $complexion
     *
     * @return UsersQuery
     */
    public function byComplexion($complexion)
    {
        return $this->andWhere(["{$this->tableName}.complexion" => $complexion]);
    }

    /**
     * @return UsersQuery
     */
    public function online()
    {
        return $this->andWhere(["{$this->tableName}.is_online" => true]);
    }
}
