<?php
/**
 * Created by PhpStorm.
 * User: ICS Developer User
 * Date: 22.03.2017
 * Time: 14:20
 */

namespace app\modules\users\models;


use yii\base\Model;
use yii\data\ActiveDataProvider;
use yii\db\Query;
use yii\helpers\ArrayHelper;

class UsersSearch extends User
{
    public $purposes_list;
    public $sex_types;

    public function rules()
    {
        return [
            [['bust_size', 'country_id', 'region_id'], 'integer'],
            [['gender'], 'string'],
            [['complexion', 'look', 'zodiac', 'hair_color', 'eyes_color', 'purposes_list', 'sex_periodicity', 'sex_role', 'sex_types'], 'each', 'rule' => ['string']], //more complexity
            [['age', 'height', 'weight'], 'validateRangeInputs'],
        ];
    }

    public function validateRangeInputs($attribute, $params, $validator)
    {
        /**@todo: make adapter*/
        /** expected format xy;xy */
        if (strpos($this->$attribute, ';') === false) {
            return;
        }

        $pair = explode(';', $this->$attribute);

        if (count($pair) != 2) {
            $this->addError($attribute, 'Invalid formatting of range input');
            return;
        }

        $this->$attribute = $pair;
    }

    public function vipValidator($attribute, $params, $validator)
    {
        /**@todo: check access for extra fields*/
    }

    public function scenarios()
    {
        return Model::scenarios();
    }

    /**
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = User::find()->with('avatar')->newestRegister();

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->setAttributes($params);

        if (!$this->validate()) {
//            var_dump($this->getErrors());
            return $dataProvider;
        }

        $query->andFilterWhere([
            'gender' => $this->gender,
            'bust_size' => $this->bust_size,
            'country_id' => $this->country_id,
            'region_id' => $this->region_id,
            'complexion' => $this->complexion,
            'look' => $this->look,
            'zodiac' => $this->zodiac,
            'hair_color' => $this->hair_color,
            'eyes_color' => $this->eyes_color,
            'sex_periodicity' => $this->sex_periodicity,
            'sex_role' => $this->sex_role,
        ]);

        if (is_array($this->age)) {
            $query->betweenAge($this->age[0], $this->age[1]);
        } else if (is_int($this->age)) {
            $query->byAge($this->age);
        }

        if (is_array($this->height)) {
            $query->heightBetween($this->height[0], $this->height[1]);
        } else if (is_int($this->height)) {
            $query->byHeight($this->height);
        }

        if (is_array($this->weight)) {
            $query->weightBetween($this->weight[0], $this->weight[1]);
        } else if (is_int($this->weight)) {
            $query->byWeight($this->weight);
        }

        if ($this->purposes_list) {
            $query->joinWith(['purposes as p'])
                ->andWhere(['p.purpose' => $this->purposes_list]);
        }

        if ($this->sex_types) {
            $query->joinWith(['sexTypes as t'])
                ->andWhere(['t.type' => $this->sex_types]);
        }

        return $dataProvider;
    }
}