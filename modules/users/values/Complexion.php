<?php
/**
 * Created by PhpStorm.
 * User: JimmDiGriz
 * Date: 3/18/2017
 * Time: 8:48 PM
 */

namespace app\modules\users\values;


class Complexion
{
    public static $list = [
        self::LEAN,
        self::NORMAL,
        self::MUSCLE,
        self::ATHLETIC,
        self::DENSE,
        self::BUXOM
    ];
    
    const LEAN = 'lean';
    const NORMAL = 'normal';
    const MUSCLE = 'muscle';
    const ATHLETIC = 'athletic';
    const DENSE = 'dense';
    const BUXOM = 'buxom';
}