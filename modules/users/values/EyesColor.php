<?php
/**
 * Created by PhpStorm.
 * User: JimmDiGriz
 * Date: 3/18/2017
 * Time: 8:46 PM
 */

namespace app\modules\users\values;


class EyesColor
{
    public static $list = [
        self::BLUE,
        self::GREEN,
        self::BROWN,
        self::GREY,
    ];
    
    const BLUE = 'blue';
    const GREEN = 'green';
    const BROWN = 'brown';
    const GREY = 'grey';
}