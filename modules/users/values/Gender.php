<?php
/**
 * Created by PhpStorm.
 * User: JimmDiGriz
 * Date: 3/17/2017
 * Time: 3:06 AM
 */

namespace app\modules\users\values;


class Gender
{
    const FEMALE = 'female';
    const MALE = 'male';
}