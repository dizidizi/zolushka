<?php
/**
 * Created by PhpStorm.
 * User: JimmDiGriz
 * Date: 3/18/2017
 * Time: 8:47 PM
 */

namespace app\modules\users\values;


class HairColor
{
    public static $list = [
        self::BLONDE,
        self::BRUNETTE,
        self::RED_HEAD,
    ];
    
    const BLONDE = 'blonde';
    const BRUNETTE = 'brunette';
    const RED_HEAD = 'redhead';
}