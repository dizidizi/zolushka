<?php
/**
 * Created by PhpStorm.
 * User: JimmDiGriz
 * Date: 3/18/2017
 * Time: 8:42 PM
 */

namespace app\modules\users\values;

class Look 
{
    public static $list = [
        self::EUROPEAN,
        self::ASIAN,
        self::CAUCASIAN,
        self::INDIAN,
        self::MIXED,
        self::MIDDLE_EASTERN,
        self::BLACK,
    ];
    
    const EUROPEAN = 'european';
    const ASIAN = 'asian';
    const CAUCASIAN = 'caucasian';
    const INDIAN = 'indian';
    const MIXED = 'mixed';
    const MIDDLE_EASTERN = 'middle eastern';
    const BLACK = 'black';
}