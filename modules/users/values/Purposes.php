<?php
/**
 * Created by PhpStorm.
 * User: JimmDiGriz
 * Date: 3/18/2017
 * Time: 9:17 PM
 */

namespace app\modules\users\values;


class Purposes
{
    public static $list = [
        self::SPONSOR,
        self::RELATIONS,
        self::EVENING,
        self::TRAVEL,
    ];
    
    const SPONSOR = 'sponsor';
    const RELATIONS = 'relations';
    const EVENING = 'evening';
    const TRAVEL = 'travel';
}