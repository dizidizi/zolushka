<?php
/**
 * Created by PhpStorm.
 * User: JimmDiGriz
 * Date: 3/18/2017
 * Time: 8:57 PM
 */

namespace app\modules\users\values;


class SexPeriodicity
{
    public static $list = [
        self::MULTIPLE_AT_DAY,
        self::MULTIPLE_AT_MONTH,
        self::MULTIPLE_AT_WEEK,
        self::ONCE_AT_DAY,
        self::WITHOUT_SEX,
    ];
    
    const MULTIPLE_AT_DAY = 'multiple per day';
    const ONCE_AT_DAY = 'once per day';
    const MULTIPLE_AT_WEEK = 'multiple per week';
    const MULTIPLE_AT_MONTH = 'multiple per month';
    const WITHOUT_SEX = 'without sex';
}