<?php
/**
 * Created by PhpStorm.
 * User: JimmDiGriz
 * Date: 3/18/2017
 * Time: 8:56 PM
 */

namespace app\modules\users\values;


class SexRole
{
    public static $list = [
        self::ACTIVE,
        self::PASSIVE,
        self::WOOD,
    ];
    
    const ACTIVE = 'active';
    const PASSIVE = 'passive';
    const WOOD = 'wood';
}