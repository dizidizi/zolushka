<?php
/**
 * Created by PhpStorm.
 * User: JimmDiGriz
 * Date: 3/18/2017
 * Time: 9:18 PM
 */

namespace app\modules\users\values;


class SexTypes
{
    public static $list = [
        self::GROUP,
        self::ANAL,
        self::ORAL,
        self::CLASSIC,
        self::VIRTUAL,
    ];
    
    const GROUP = 'group';
    const ANAL = 'anal';
    const ORAL = 'oral';
    const CLASSIC = 'classic';
    const VIRTUAL = 'virtual';
}