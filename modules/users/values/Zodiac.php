<?php
/**
 * Created by PhpStorm.
 * User: JimmDiGriz
 * Date: 17.03.2017
 * Time: 12:10
 */

namespace app\modules\users\values;


class Zodiac
{
    public static $list = [
        self::TWINS,
        self::GOAT_HORNED,
        self::RAM,
        self::BULL,
        self::CRAB,
        self::LION,
        self::MAIDEN,
        self::SCALES,
        self::SCORPION,
        self::ARCHER,
        self::WATER_BEARER,
        self::FISHES
    ];

    const TWINS = 'twins';
    const GOAT_HORNED = 'goat-horned';
    const RAM = 'ram';
    const BULL = 'bull';
    const CRAB = 'crab';
    const LION = 'lion';
    const MAIDEN = 'maiden';
    const SCALES = 'scales';
    const SCORPION = 'scorpion';
    const ARCHER = 'archer';
    const WATER_BEARER = 'water-bearer';
    const FISHES = 'fishes';
}