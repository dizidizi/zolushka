<?php
/**
 * Created by PhpStorm.
 * User: JimmDiGriz
 * Date: 17.03.2017
 * Time: 15:10
 */
use app\modules\users\values\Gender;
use kartik\widgets\DepDrop;
use kartik\widgets\Select2;
use yii\data\ActiveDataProvider;
use yii\helpers\Url;

/**@var ActiveDataProvider $dataProvider*/
/**@var array $hairList*/
/**@var array $eyesList*/
/**@var array $zodiacList*/
/**@var array $lookList*/
/**@var array $complexionList*/
/**@var array $purposesList*/
/**@var array $sexTypesList*/
/**@var array $sexRoleList*/
/**@var array $sexPeriodicityList*/
/**@var array $genderList*/
/**@var array $groupList*/
/**@var \app\modules\geo\models\Country[] $countries*/

$this->title = 'Search';
?>

<link href="//maxcdn.bootstrapcdn.com/font-awesome/4.3.0/css/font-awesome.min.css" rel="stylesheet">
<header class="page-content-header">
  <div class="container-fluid">
    <div class="tbl">
      <div class="tbl-row">
        <div class="tbl-cell">
          <div class="container center-align users-search">
            <h2 class="text-center" style="margin: 30px 0 40px; color: #76838e;">
              Search housemaid for successful man
              <br />
            </h2>
            <form class="search-form" action="<?= Url::to(['/users/default/search']) ?>">
              <!-- Standard search -->
              <div class="row">
				<div class="col-sm-1">
					<h3 style="color: #76838e; margin-top: 4px;">Search</h3>
				</div>
                <fieldset class="form-group col-sm-2">
                  <select class="select2" name="gender">
					  <? foreach ($genderList as $gender): ?>
                    		<option <? if ($gender == Gender::FEMALE): ?>selected<? endif; ?>><?= $gender ?></option>
					  <? endforeach; ?>
                  </select>
                </fieldset>
			  <fieldset class="form-group  col-sm-3">
				  <?=
				  	Select2::widget([
						  'id' => 'country-id',
						  'name' => 'country_id',
						  'data' => $countries,
						  'options' => [
							  'placeholder' => 'Select country...',
							  'class' => 'select2',
						  ],
						  'theme' =>'default'
					  ]);
				  ?>
			  </fieldset>

			  <fieldset class="form-group  col-sm-3">
				  <?=
				  DepDrop::widget([
				      'type'=>DepDrop::TYPE_SELECT2,
					  'name' => 'region_id',
					  'data' => $regions,
					  'class' => 'select2',
					  'select2Options'=>[
					        'pluginOptions'=>['allowClear'=>true],
					        'theme' =>'default'
					   ],
					  'pluginOptions'=>[
						  'depends'=> ['country-id'],
						  'placeholder' => 'Select region...',
						  'url'=> Url::to(['/geo/default/regions'])
					  ],

				  ]);
				  ?>
			  </fieldset>

                <fieldset class="form-group  col-sm-3">
                  <div class="form-group range-slider-blue range-slider-simple">
                    <input type="text" id="age-slider" name="age" value="" />
                  </div>
                </fieldset>
				<div class="text-center">
				    <button type="submit" class="btn btn-success btn-rounded">
                       <span>Search</span>
                    </button>
					<button type="button" class="search-form__toggle btn btn-default-outline btn-rounded" 
						style="padding-left: 16px;
						padding-right: 18px;
						margin: 20px;">
						<i class="fa fa-search-plus"></i><span>Extended search</span>
					</button>
				</div>
              </div>
			  
			  <div class="advanced-search" style="display: none">
				
				<div class="row">
					<h3 class="text-center">Look and parameters</h3>
					<fieldset class="form-group col-sm-3">
					  <div class="form-group range-slider-blue range-slider-simple">
						<label class="form-label text-center">Height</label>
						<input type="text" id="height-slider" name="height" value="" />
					  </div>
					</fieldset>
					
					<fieldset class="form-group  col-sm-3">
					  <div class="form-group range-slider-blue range-slider-simple">
						<label class="form-label text-center">Weight</label>
						<input type="text" id="weight-slider" name="weight" value="" />
					  </div>
					</fieldset>

					<? if (Yii::$app->user->isGuest || Yii::$app->user->identity->isMale()): ?>
					<fieldset class="form-group  col-sm-3">
					  <div class="form-group range-slider-blue range-slider-simple">
						<label class="form-label text-center">Bust size</label>
						<select class="select2" name="bust_size">
							<option selected></option>
						<? for ($i = 1; $i <= 5; $i++): ?>
							<option><?= $i ?></option>
						<? endfor; ?>
						</select>
					  </div>
					</fieldset>
					<? endif; ?>
					
					<fieldset class="form-group col-sm-3">
					  <div class="form-group range-slider-blue range-slider-simple">
						<label class="form-label text-center">Complexion</label>
						<select class="select2" multiple="multiple" name="complexion[]">
							<? foreach ($complexionList as $complexion): ?>
							<option><?= $complexion ?></option>
							<? endforeach; ?>
						</select>
					  </div>
					</fieldset>
				</div>
				<div class="row">
					<fieldset class="form-group col-sm-3">
					  <div class="form-group range-slider-blue range-slider-simple">
						<label class="form-label text-center">Look</label>
						<select class="select2" multiple="multiple" name="look[]">
							<? foreach ($lookList as $look): ?>
							<option><?= $look ?></option>
							<? endforeach; ?>
						</select>
					  </div>
					</fieldset>
					<fieldset class="form-group col-sm-3">
					  <div class="form-group range-slider-blue range-slider-simple">
						<label class="form-label text-center">Zodiac</label>
						<select class="select2" multiple="multiple" name="zodiac[]">
							<? foreach ($zodiacList as $zodiac): ?>
							<option><?= $zodiac ?></option>
							<? endforeach; ?>
						</select>
					  </div>
					</fieldset>
					<fieldset class="form-group col-sm-3">
					  <div class="form-group range-slider-blue range-slider-simple">
						<label class="form-label text-center">Hair color</label>
						<select class="select2" multiple="multiple" name="hair_color[]">
							<? foreach ($hairList as $color): ?>
							<option><?= $color ?></option>
							<? endforeach; ?>
						</select>
					  </div>
					</fieldset>
					<fieldset class="form-group col-sm-3">
					  <div class="form-group range-slider-blue range-slider-simple">
						<label class="form-label text-center">Eyes color</label>
						<select class="select2" multiple="multiple" name="eyes_color[]">
							<? foreach ($eyesList as $color): ?>
							<option><?= $color ?></option>
							<? endforeach; ?>
						</select>
					  </div>
					</fieldset>
				</div>
				<hr />
				<h3 class="text-center">Purposes</h3>
				<div class="row">
					<fieldset class="form-group col-sm-3">
						<? foreach ($purposesList as $purpose): ?>
						<div class="checkbox">
							<input type="checkbox" name="purposes_list[]" id="check-<?= $purpose ?>" value="<?= $purpose ?>">
							<label for="check-<?= $purpose ?>"><?= $purpose ?></label>
						</div>
					<? endforeach; ?>
					</fieldset>
				</div>
				<hr />
				<h3 class="text-center">Sexual preferences</h3>
				<div class="row">
					<fieldset class="form-group col-sm-4">
					  <div class="form-group range-slider-blue range-slider-simple">
						<label class="form-label text-center">Sex periodicity</label>
						<select class="select2" multiple="multiple" name="sex_periodicity[]">
							<? foreach ($sexPeriodicityList as $periodicity): ?>
							<option><?= $periodicity ?></option>
							<? endforeach; ?>
						</select>
					  </div>
					</fieldset>
					<fieldset class="form-group col-sm-4">
					  <div class="form-group range-slider-blue range-slider-simple">
						<label class="form-label text-center">Sex role</label>
						<select class="select2" multiple="multiple" name="sex_role[]">
							<? foreach ($sexRoleList as $role): ?>
							<option><?= $role ?></option>
							<? endforeach; ?>
						</select>
					  </div>
					</fieldset>
					<fieldset class="form-group col-sm-4">
					  <div class="form-group range-slider-blue range-slider-simple">
						<label class="form-label text-center">Sex type</label>
						<select class="select2" multiple="multiple" name="sex_types[]">
							<? foreach ($sexTypesList as $type): ?>
							<option><?= $type ?></option>
							<? endforeach; ?>
						</select>
					  </div>
					</fieldset>
				</div>
				  <button type="submit" class="btn btn-inline btn-lg btn-success" id="search-submit">Search</button>
			  </div>
            </form>
          </div>
        </div>
      </div>
    </div>
  </div>
</header><!--.page-content-header-->
<div class="container">
    <div class="row">
        <?= \yii\widgets\ListView::widget([
            'dataProvider' => $dataProvider,
            'itemView' => 'items/_user_list_item',
            'summary' => '',
        ]) ?>
    </div>
</div>

