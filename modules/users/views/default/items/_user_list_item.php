<?php
/**
 * Created by PhpStorm.
 * User: JimmDiGriz
 * Date: 17.03.2017
 * Time: 16:41
 */
use app\core\components\AvatarManager;
use app\modules\users\models\User;
use app\modules\users\values\Gender;
use yii\helpers\Url;

/**@var User $model*/

?>

<div class="col-sm-6 col-md-4 col-xl-3">
      <article class="card-user box-typical">
          <div class="ribbon green right-top" data-tooltip="Проверенное фото" data-toggle="tooltip" data-title="Проверенное фото" data-placement="bottom" data-original-title="" title="">
              <i class="fa fa-check-square-o"></i>
          </div>
        <div class="card-user-photo">
          <a href="<?= Url::to(['/users/default/view', 'userId' => $model->id]);?>" target="_blank"><img src="<?= AvatarManager::getAvatarForUser($model) ?>" alt="<?= $model->name ?>"></a>
        </div>
        <div class="card-user-name">
			<a href="<?= Url::to(['/users/default/view', 'userId' => $model->id]);?>" target="_blank"><?= $model->name ?></a>
		</div>
        <div class="card-user-status text-muted"><?= $model->age ?></div>
        <a  href=""
            @click="createChat(<?= $model->id ?>)"
            data-toggle="modal"
            data-target="#chat"
            class="btn btn-primary-outline btn-rounded">
            <i class="fa fa-envelope"></i>
            <span>Написать</span>
        </a>

      </article><!--.card-user-->
</div>

