<?php

use yii\helpers\Html;
use yii\bootstrap\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\modules\users\models\User */
/* @var $form ActiveForm */

\app\assets\AppAsset::register($this);

?>
<div class="default-register">
	<div class="container">
		<header class="section-header">
		  <div class="tbl">
			<div class="tbl-row">
			  <div class="tbl-cell">
				<h3>Регистрация</h3>
			  </div>
			</div>
		  </div>
		</header>
		<section class="card">
			<div class="card-block">
				<?php $form = ActiveForm::begin([
					'id' => 'register-form',
					'layout' => 'horizontal',
					'fieldConfig' => [
						'template' => "{label}\n<div class=\"col-lg-5\">{input}</div>\n<div class=\"col-lg-8\">{error}</div>",
						'labelOptions' => ['class' => 'col-lg-1 control-label'],
					],
				]); ?>

					<?= $form->field($model, 'email')->textInput(['autofocues' => true]) ?>
					<?= $form->field($model, 'password')->passwordInput() ?>
					<?= $form->field($model, 'name')->textInput() ?>
					<!--<?= $form->field($model, 'gender')->radioList(['female' => 'Female', 'male' => 'Male']) ?>-->
					<span class="radio">
						<input type="radio" name="User[gender]" id="radio-1" value="female" checked>
						<label for="radio-1">Female </label>
					</span>
					<span class="radio">
						<input type="radio"  name="User[gender]" id="radio-2" value="male">
						<label for="radio-2">Male </label>
					</span>
					<?= $form->field($model, 'age') ?>
				
					<div class="form-group">
						<?= Html::submitButton(Yii::t('app', 'Submit'), ['class' => 'btn btn-primary']) ?>
					</div>
				<?php ActiveForm::end(); ?>
			</div>
		</section>
	</div>
</div><!-- default-register -->
