<?php
/**
 * Created by PhpStorm.
 * User: JimmDiGriz
 * Date: 3/24/2017
 * Time: 11:46 PM
 */
use kartik\widgets\DepDrop;
use kartik\widgets\Select2;
use yii\helpers\Url;

/**@var \app\modules\users\models\User $user*/
/**@var array $hairList*/
/**@var array $eyesList*/
/**@var array $zodiacList*/
/**@var array $lookList*/
/**@var array $complexionList*/
/**@var array $purposesList*/
/**@var array $sexTypesList*/
/**@var array $sexRoleList*/
/**@var array $sexPeriodicityList*/
/**@var array $genderList*/
/**@var array $groupList*/
/**@var \app\modules\geo\models\Country[] $countries*/

$this->title = 'Settings';

?>

<!-- User ID -->
<input type="hidden" id="getUserId" value="<?= $user->id ?>"/>

<div class="container-fluid">
    <form id="edit-form" action="<?= Url::to(['/users/default/settings'/*, 'userId' => $user->id*/]) ?>">
        <header class="section-header">
            <div class="tbl">
                <div class="tbl-row">
                    <div class="tbl-cell">
                        <h3>My settings</h3>
                        <ol class="breadcrumb breadcrumb-simple">
                            <li><a href="<?= Url::to(['/users/default/index'])?>">Users</a></li>
                            <li class="active">My settings</li>
                        </ol>
                        <div class="subtitle">Welcome to User Edit</div>
                    </div>
                    <div class="tbl-cell tbl-cell-action button">
                         <button type="button" class="btn btn-inline btn-success" id="edit-submit">Save changes</button>
                    </div>
                    <div class="tbl-cell tbl-cell-action button">
                        <a href="<?= Url::to(['/user/default/index'])?>" class="btn btn-inline btn-secondary">Cancel</a>
                    </div>
                </div>
            </div>
        </header>
    <section class="tabs-section">
                <div class="tabs-section-nav tabs-section-nav-icons">
                    <div class="tbl">
                        <ul class="nav" role="tablist">
                            <li class="nav-item">
                                <a class="nav-link active" href="#tabs-1-tab-1" role="tab" data-toggle="tab">
                                    <span class="nav-link-in">
                                        <i class="font-icon font-icon-user"></i>
                                        User Data
                                    </span>
                                </a>
                            </li>
                            <li class="nav-item">
                                <a class="nav-link" href="#tabs-1-tab-2" role="tab" data-toggle="tab">
                                    <span class="nav-link-in">
                                        <i class="font-icon font-icon-notebook"></i>
                                        Basic Information
                                    </span>
                                </a>
                            </li>
                            <li class="nav-item">
                                <a class="nav-link" href="#tabs-1-tab-3" role="tab" data-toggle="tab">
                                    <span class="nav-link-in">
                                        <i class="font-icon font-icon-player-full-screen"></i>
                                        Look
                                    </span>
                                </a>
                            </li>
                            <li class="nav-item">
                                <a class="nav-link" href="#tabs-1-tab-4" role="tab" data-toggle="tab">
                                    <span class="nav-link-in">
                                        <i class="font-icon font-icon-heart"></i>
                                        Sexual Preferences
                                    </span>
                                </a>
                            </li>

                        </ul>
                    </div>
                </div>
                <div class="tab-content">
                    <div role="tabpanel" class="tab-pane row fade in active" id="tabs-1-tab-1">
                        <input type="hidden" name="_csrf" value="<?= Yii::$app->request->getCsrfToken()?>"/>
                        <div class="col-md-6">
                            <h5 class="m-t-lg with-border">User Data</h5>
                            <div class="row">
                                <fieldset class="form-group col-sm-12">
                                    <label class="form-label semibold" for="name">Name</label>
                                    <input type="text" class="form-control" id="name" name="name" placeholder="Name" value="<?= $user->name ?>">
                                </fieldset>

                                <fieldset class="form-group  col-sm-12">
                                    <label class="form-label" for="email">Email address</label>
                                    <input type="email" class="form-control" id="email" name="email" placeholder="Enter email" value="<?= $user->email ?>">
                                </fieldset>

                                <div class="col-lg-4">
                                    <fieldset class="form-group">
                                        <label class="form-label semibold" for="group">Group</label>
                                        <select class="select2" id="group" name="group">
                                            <? foreach ($groupList as $group): ?>
                                                <option <? if ($user->group == $group): ?> selected <? endif; ?>><?= $group ?>
                                                </option>
                                            <? endforeach; ?>
                                        </select>
                                    </fieldset>
                                </div>

    <!--                        <fieldset class="form-group  col-sm-12">-->
    <!--                            <label class="form-label" for="password">Password</label>-->
    <!--                            <input type="password" class="form-control" name="password" id="password" placeholder="Password">-->
    <!--                        </fieldset>-->
                            </div><!--.row-->
                        </div>
                    </div><!--.tab-pane-->
                    <div role="tabpanel" class="tab-pane row fade" id="tabs-1-tab-2">
                        <div class="col-md-6">
                            <h5 class="m-t-lg with-border">Basic Information</h5>
                            <div class="row">
                                <div class="col-lg-4">
                                    <fieldset class="form-group">
                                        <label class="form-label semibold" for="gender">Gender</label>
                                        <select class="select2" id="gender" name="gender">
                                            <? foreach ($genderList as $gender): ?>
                                                <option <? if ($user->gender == $gender): ?> selected <? endif; ?>
                                                    data-content='<span class="fa fa-lg fa-<?= $gender ?>"></span>&nbsp;<?= $gender ?>'><?= $gender ?>
                                                </option>
                                            <? endforeach; ?>
                                        </select>
                                    </fieldset>
                                </div>
                                <div class="col-lg-4">
                                    <fieldset class="form-group">
                                        <label class="form-label" for="age">Age</label>
                                        <input type="text" class="form-control" id="age" name="age" placeholder="" value="<?= $user->age ?>">
                                    </fieldset>
                                </div>
                                <div class="col-lg-4">
                                    <fieldset class="form-group">
                                        <label class="form-label" for="zodiac">Zodiac</label>
                                        <select class="select2" id="zodiac" name="zodiac">
                                            <? if (!$user->zodiac): ?>
                                                <option selected></option>
                                            <? endif ?>
                                            <? foreach ($zodiacList as $zodiac): ?>
                                                <option <? if ($user->zodiac == $zodiac): ?> selected <? endif; ?>><?= $zodiac ?></option>
                                            <? endforeach; ?>
                                        </select>
                                    </fieldset>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-lg-6">
                                    <fieldset class="form-group">
                                        <label class="form-label" for="country">Country</label>
                                        <?=
                                        Select2::widget([
                                            'id' => 'country-id',
                                            'name' => 'country_id',
                                            'data' => $countries,
                                            'value' => $user->country_id,
                                            'options' => [
                                                'placeholder' => 'Select country...',
                                                'class' => 'select2',
                                            ],
                                            'theme' =>'default'
                                        ]);
                                        ?>
                                </div>
                                <div class="col-lg-6">
                                     <label class="form-label" for="country">City</label>
                                    <?=
                                    DepDrop::widget([
                                        'type'=>DepDrop::TYPE_SELECT2,
                                        'name' => 'region_id',
                                        'class' => 'select2',
                                        'select2Options'=>[
                                                'pluginOptions'=>['allowClear'=>true],
                                                'theme' =>'default'
                                         ],
                                        'pluginOptions'=> [
                                            'depends'=> ['country-id'],
                                            'placeholder' => 'Select region...',
                                            'url'=> Url::to(['/geo/default/regions', 'selectedId' => $user->region_id]),
                                            'initialize' => true,
                                        ],
                                    ]);
                                    ?>
                                </div>
                            </div>
                        </div>

                        <div class="col-xs-12">
                            <fieldset class="form-group">
                                <label class="form-label" for="about">About</label>
                                <textarea rows="4" class="form-control editable" placeholder="Press Enter" id="information" name="information" data-autosize><?= $user->information ?></textarea>
                            </fieldset>
                        </div>
                    </div><!--.tab-pane-->
                    <div role="tabpanel" class="tab-pane row fade" id="tabs-1-tab-3">
                        <div class="col-md-12">
                            <h5 class="m-t-lg with-border">Look</h5>
                            <div class="row">
                                <fieldset class="form-group col-md-3">
                                    <label class="form-label" for="height">Height</label>
                                    <input type="text" class="form-control" id="height" name="height" placeholder="" value="<?= $user->height ?>">
                                </fieldset>
                                <fieldset class="form-group col-md-3">
                                    <label class="form-label" for="weight">Weight</label>
                                    <input type="text" class="form-control" id="weight" name="weight" placeholder="" value="<?= $user->weight ?>">
                                </fieldset>
                                <fieldset class="form-group col-md-3">
                                    <label class="form-label" for="complexion">Complexion</label>
                                    <select class="select2" id="complexion" name="complexion">
                                        <? if (!$user->complexion): ?>
                                            <option selected></option>
                                        <? endif; ?>
                                        <? foreach ($complexionList as $complexion): ?>
                                            <option <? if ($user->complexion == $complexion): ?> selected <? endif; ?>><?= $complexion ?></option>
                                        <? endforeach; ?>
                                    </select>
                                </fieldset>
                                    <fieldset class="form-group col-md-3">
                                        <label class="form-label" for="look">Look</label>
                                        <select class="select2" id="look" name="look">
                                            <? if (!$user->look): ?>
                                                <option selected></option>
                                            <? endif; ?>
                                            <? foreach ($lookList as $look): ?>
                                                <option<? if ($user->look == $look): ?> selected <? endif; ?>><?= $look ?></option>
                                            <? endforeach; ?>
                                        </select>
                                    </fieldset>
                            </div>
                            <div class="row">
                                <fieldset class="form-group col-md-4">
                                    <label class="form-label" for="hair">Hair Color</label>
                                    <select class="select2" id="hair" name="hair_color">
                                        <? if (!$user->hair_color): ?>
                                            <option selected></option>
                                        <? endif; ?>
                                        <? foreach ($hairList as $hair): ?>
                                            <option <? if ($user->hair_color == $hair): ?> selected <? endif; ?>><?= $hair ?></option>
                                        <? endforeach; ?>
                                    </select>
                                </fieldset>
                                <fieldset class="form-group col-md-4">
                                    <label class="form-label" for="eyes">Eyes Color</label>
                                    <select class="select2" id="eyes" name="eyes_color">
                                        <? if (!$user->eyes_color): ?>
                                            <option selected></option>
                                        <? endif; ?>
                                        <? foreach ($eyesList as $color): ?>
                                            <option <? if ($user->eyes_color == $color): ?> selected <? endif; ?>><?= $color ?></option>
                                        <? endforeach; ?>
                                    </select>
                                </fieldset>
                                <?if ($user->isFemale()): ?>
                                    <fieldset class="form-group col-md-4">
                                        <label class="form-label" for="bust">Bust Size</label>
                                        <select class="select2" id="bust" name="bust_size">
                                            <? if (!$user->bust_size): ?>
                                                <option selected></option>
                                            <? endif; ?>
                                            <? for ($i = 1; $i <= 5; $i++): ?>
                                                <option <? if ($user->bust_size == $i): ?> selected <? endif; ?>><?= $i ?></option>
                                            <? endfor; ?>
                                        </select>
                                    </fieldset>
                                <? endif; ?>
                            </div>
                        </div>
                    </div><!--.tab-pane-->
                    <div role="tabpanel" class="tab-pane row fade" id="tabs-1-tab-4">
                        <div class="col-md-12">
                            <h5 class="m-t-lg with-border">Sexual Preferences</h5>
                            <div class="row">
                                <fieldset class="form-group col-md-4">
                                    <label class="form-label" for="periodicity">Sex Periodicity</label>
                                    <select class="select2" id="periodicity" name="sex_periodicity">
                                        <? if (!$user->sex_periodicity): ?>
                                            <option selected></option>
                                        <? endif; ?>
                                        <? foreach ($sexPeriodicityList as $periodicity): ?>
                                            <option <? if ($user->sex_periodicity == $periodicity): ?> selected <? endif; ?>><?= $periodicity ?></option>
                                        <? endforeach; ?>
                                    </select>
                                </fieldset>
                                <fieldset class="form-group col-md-4">
                                    <label class="form-label" for="role">Sex Role</label>
                                    <select class="select2" id="role" name="sex_role">
                                        <? if (!$user->sex_role): ?>
                                            <option selected></option>
                                        <? endif; ?>
                                        <? foreach ($sexRoleList as $role): ?>
                                            <option <? if ($user->sex_role == $role): ?> selected <? endif; ?>><?= $role ?></option>
                                        <? endforeach; ?>
                                    </select>
                                </fieldset>
                                <fieldset class="form-group col-md-4">
                                    <label class="form-label" for="types">Sex Types</label>
                                    <select class="select2" multiple="multiple" id="types" name="types[]">
                                        <? foreach ($sexTypesList as $type): ?>
                                            <option <? if ($user->containsSexType($type)): ?> selected <? endif; ?>><?= $type ?></option>
                                        <? endforeach; ?>
                                    </select>
                                </fieldset>
                            </div>
                        </div>
                        <? if ($user->isFemale()): ?>
                        <div class="col-md-12">
                            <h5 class="m-t-lg with-border">Purposes</h5>
                            <div class="row">
                                <fieldset class="form-group col-md-6">
                                    <select class="select2" multiple="multiple" id="purposes" name="purposes[]">
                                        <? foreach ($purposesList as $purpose): ?>
                                            <option <? if ($user->containsPurpose($purpose)): ?> selected <? endif; ?>><?= $purpose ?></option>
                                        <? endforeach; ?>
                                    </select>
                                </fieldset>
                            </div>
                        </div>
                        <? endif; ?>
                    </div><!--.tab-pane-->
                </div>
            </section>
        </form>
        <section class="card profile-gallery" id="profile-gallery">
            <header class="card-header card-header-lg">
                <div class="tbl">
                    <div class="tbl-row">
                        <div class="tbl-cell tbl-cell-title">
                            User Gallery
                        </div>
                        <div class="tbl-cell tbl-cell-action button" v-if="photos">
                            <button type="button"
                                    class="btn btn-inline btn-primary upload-btn"
                                    data-toggle="modal"
                                    data-target="#uploadPhotos" @click="clearQueue">
                                Upload photos
                            </button>
                        </div>
                    </div>
                </div>
            </header>
            <div class="card-block">
                <div class="row">
                    <div class="col-md-3">
                        <section class="profile-side-user" id="app">
							<button type="button" class="avatar-preview avatar-preview-128">
								<img  :src="avatarUrl ? avatarUrl : '<?= \app\core\components\AvatarManager::getAvatarForUser($user) ?>'"  alt=""/>
								<span class="update" @click="toggleShow">
									<i class="font-icon font-icon-picture-double"></i>
									Update photo
								</span>
							</button>
							<avatar url="<?= Url::to(['/photos/api/upload']) ?>"
								field="avatar"
								key="0"
								ref="avatarUpload"
								lang-type="ru"
								:value.sync="show"
								v-on:crop-success="cropSuccess"
								v-on:crop-upload-success="cropUploadSuccess"

								:width="253"
								:height="253"
								:params="{user_id:<?= $user->id ?>,set_avatar:1}"></avatar>
						   <div class="bottom-txt">Your Avatar</div>
						</section>
                    </div>
                    <div class="col-md-9">

                        <div class="row" >
                            <template>

                                <div class="add-customers-screen tbl" v-if="!photos">
                                    <div class="add-customers-screen-in">
                                        <div class="add-customers-screen-user">
                                            <i class="font-icon font-icon-picture-2"></i>
                                        </div>
                                        <h2>Your photo gallery</h2>
                                        <p class="lead color-blue-grey-lighter">You can add new photos, set avatar <br/>and change position</p>
                                        <button class="btn btn-primary" data-toggle="modal" data-target="#uploadPhotos" @click="clearQueue">Upload photos</button>
                                    </div>
                                </div>
                                <div class="col-md-12">
                                    <h5 class="m-t-lg" v-if="photos">Photos</h5>
                                    <div class="row profile-gallery__list">
                                         <draggable
                                            v-bind="photos"
                                            :options="{
                                            }"
                                            @end="updatePosition"
                                            >


                                                <div class="col-sm-6 col-md-3 col-lg-4 gallery-item"
                                                    v-for="(photo,index) in photos"
                                                    :key="photo.id"
                                                    :data-position="photo.order"
                                                    :data-id="photo.id"
                                                    >
                                                        <a :href="photo.filename" class="box-typical gallery-item__image" v-bind:style="{backgroundImage: 'url(' + photo.filename + ')'}">
                                                        </a>
                                                        <span class="gallery-action-btn gallery-item__delete"
                                                            @click="removePhoto(index, photo)"
                                                            data-tooltip="Remove this photo"
                                                            data-toggle="tooltip"
                                                            data-title="Remove this photo"
                                                            data-placement="left"
                                                            data-original-title=""
                                                            title="">
                                                            <i class="font-icon-close-2"></i>
                                                        </span>
                                                        <span class="gallery-action-btn gallery-item__set-avatar"
                                                            @click="setAvatar(index, photo.filename)"
                                                            data-tooltip="Set to avatar"
                                                            data-toggle="tooltip"
                                                            data-title="Set to avatar"
                                                            data-placement="left"
                                                            data-original-title=""
                                                            title="">
                                                            <i class="font-icon-user"></i>
                                                        </span>
                                                </div>
                                         </draggable>
                                    </div>
                                </div>

                                <div class="modal fade"
                                     id="uploadPhotos"
                                     tabindex="-1"
                                     role="dialog"
                                     aria-labelledby="Upload new photos"
                                     aria-hidden="true">
                                    <div class="modal-dialog" role="document">
                                        <div class="modal-content">
                                            <div class="modal-header">
                                                <button type="button" class="modal-close" data-dismiss="modal" aria-label="Close">
                                                    <i class="font-icon-close-2"></i>
                                                </button>
                                                <h4 class="modal-title" id="myModalLabel">Upload new photos</h4>
                                            </div>
                                            <div class="modal-body" id="main-upload" >
                                                <dropzone
                                                    id="galleryUpload"
                                                    ref="galleryUpload"

                                                    url="/photos/api/upload"

                                                    v-on:vdropzone-success="success"
                                                    v-on:vdropzone-sending="sendingEvent"
                                                    v-on:vdropzone-sending-multiple="sendingEvent"

                                                    :use-custom-dropzone-options="true"
                                                    :dropzone-options="dropzoneOptions">
                                                </dropzone>
                                            </div>
                                            <div class="modal-footer">
                                                <button type="button" class="btn btn-rounded btn-primary" id="startUpload" data-dismiss="modal" @click="uploadPhotos">Close</button>
                                            </div>
                                        </div>
                                    </div>
                                </div><!-- Upload modal-->
                            </template>
                        </div>
                    </div>
                </div>
            </div>
        </section>
</div> <!-- Container Fluid -->