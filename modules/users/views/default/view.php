<?php
/**
 * Created by PhpStorm.
 * User: JimmDiGriz
 * Date: 3/20/2017
 * Time: 12:49 AM
 */
use app\core\components\AvatarManager;
use app\modules\users\values\Gender;
use yii\helpers\Url;
use kartik\widgets\FileInput;

/**@var \app\modules\users\models\User $user*/

\app\assets\FileInputAsset::register($this);

?>
<!-- User ID -->
<input type="hidden" id="getUserId" value="<?= $user->id ?>"/>
<input type="hidden" value="<?= $user->id ?>" id="user-id">
<div id="profile-gallery" xmlns="http://www.w3.org/1999/html">
    <div id="app">
	
        <div class="profile-header-photo gradient" v-bind:style="{ backgroundImage: avatarUrl1 ? 'url(' + avatarUrl1 + ')' : 'url(<?= AvatarManager::getCoverForUser($user) ?>)'}" v-show="true">

            <div class="profile-header-photo-in">
                <div class="tbl-cell">
                    <div class="info-block">
                        <div class="container-fluid">
                            <div class="row">
                                <div class="col-xl-9 col-xl-offset-3 col-lg-8 col-lg-offset-4 col-md-offset-0">
                                    <div class="tbl info-tbl">
                                        <div class="tbl-row">
                                            <div class="tbl-cell">
                                                <p class="title">Dan Counsell</p>
                                                <p>Company Founder</p>
                                            </div>
                                            <div class="tbl-cell tbl-cell-stat">
                                                <div class="inline-block">
                                                    <p class="title">23К</p>
                                                    <p>Visitors</p>
                                                </div>
                                            </div>
                                            <div class="tbl-cell tbl-cell-stat">
                                                <div class="inline-block">
                                                    <p class="title"><?= $user->getPhotos()->count() /**@todo: fix*/ ?></p>
                                                    <p>Photos</p>
                                                </div>
                                            </div>
                                            <div class="tbl-cell tbl-cell-stat">
                                                <div class="inline-block">
                                                    <p class="title">18</p>
                                                    <p>Videos</p>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <? if (!Yii::$app->user->isGuest && (Yii::$app->user->identity->id == $user->id || Yii::$app->user->is(\app\rbac\Roles::ADMIN))): ?>
            <button type="button" class="change-cover" @click="toggleShow1">
                <i class="font-icon font-icon-picture-double"></i>
                Change cover

            </button>
            <avatar url="<?= Url::to(['/photos/api/upload']) ?>"
                name="avatar1"
                field="avatar1"
                key="0"
                ref="avatarUpload1"
                lang-type="ru"
                v-model="show1"
                v-on:crop-success="cropSuccess"
                v-on:crop-upload-success="cropUploadSuccess"
                :width = "1366"
                :height = "268"
                :params="{user_id:<?= $user->id ?>,set_cover:1}"></avatar>
            <? endif; ?>
        </div><!--.profile-header-photo-->
        <div class="container-fluid">
            <div class="row">
                <div class="col-xl-3 col-lg-4">
                    <aside class="profile-side">
                        <section class="box-typical profile-side-user" >
                            <div class="ribbon green right-top" data-tooltip="Проверенное фото" data-toggle="tooltip" data-title="Проверенное фото" data-placement="bottom" data-original-title="" title="">
                                <i class="fa fa-check-square-o"></i>
                            </div>
                            <button type="button" class="avatar-preview avatar-preview-128" >
							<img :src="avatarUrl ? avatarUrl : '<?= AvatarManager::getAvatarForUser($user) ?>'"   alt=""/>
						<? if (!Yii::$app->user->isGuest && (Yii::$app->user->identity->id == $user->id || Yii::$app->user->is(\app\rbac\Roles::ADMIN))): ?>
							  <span class="update" @click="toggleShow">
								<i class="font-icon font-icon-picture-double"></i>
								Update photo
							  </span>
						<? else: ?>
						<? endif; ?>
                            </button>
                            <avatar url="<?= Url::to(['/photos/api/upload']) ?>"
                                    name="avatar"
                                    field="avatar"
                                    key="0"
                                    ref="avatarUpload"
                                    lang-type="ru"
                                    v-model="show"
                                    :width="253"
                                    :height="253"
                                    v-on:crop-success="cropSuccess"
                                    v-on:crop-upload-success="cropUploadSuccess"
                                    :params="{user_id:<?= $user->id ?>,set_avatar:1}"></avatar>
                            <div class="btn-group" role="group">
                                <button type="button" class="btn btn-rounded"
                                    data-toggle="modal"
                                    data-target="#chat"
                                    @click="createChat(<?= $user->id ?>)">
                                    Chat
                                </button>
                            </div>
                            <button type="button" class="btn btn-rounded btn-success-outline">Call</button>
                            <div class="bottom-txt">Last seen 10 minutes ago</div>
                        </section>

                        <section class="box-typical">
                            <div class="box-typical-inner">
                                <ul class="nav justify-content-center user-actions-menu">
                                    <li class="nav-item">
                                        <a href="#" class="nav-link">
                                            <i class="fa fa-star fa-hover"></i>
                                            <span>To favourites</span>
                                        </a>
                                    </li>
                                    <li class="nav-item">
                                        <a href="#" class="nav-link">
                                            <i class="fa fa-ban fa-hover"></i>
                                            <span>Ignor</span>
                                        </a>
                                    </li>
                                    <li class="nav-item">
                                        <a href="#" class="nav-link text-success">
                                            <i class="fa fa-ban fa-gift"></i>
                                            <span>Make compliment</span>
                                        </a>
                                    </li>
                                </ul>
                            </div>
                        </section>
                        <section class="box-typical">
                            <header class="box-typical-header-sm bordered">Look</header>
                            <div class="box-typical-inner">
                                <table class="user-page-table">
                                    <tr>
                                        <td>
                                            <b>Height: </b>
                                        </td>
                                        <td>
                                            <?= $user->height ?>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td><b>Weight: </b></td>
                                        <td><?= $user->weight ?></td>
                                    </tr>
                                    <? if ($user->isFemale()): ?>
                                    <tr>
                                        <td><b>Bust Size: </b></td>
                                        <td><?= $user->bust_size ?></td>
                                    </tr>
                                    <? endif; ?>
                                    <tr>
                                        <td><b>Eyes Color: </b></td>
                                        <td><?= $user->eyes_color ?></td>
                                    </tr>
                                    <tr>
                                        <td><b>Hair Color: </b></td>
                                        <td><?= $user->hair_color ?></td>
                                    </tr>
                                    <tr>
                                        <td><b>Look: </b></td>
                                        <td><?= $user->look ?></td>
                                    </tr>
                                    <tr>
                                        <td><b>Complexion: </b></td>
                                        <td><?= $user->complexion ?></td>
                                    </tr>
                                </table>
                            </div>
                        </section>
                        <? if ($user->isFemale()): ?>
                        <section class="box-typical">
                            <header class="box-typical-header-sm bordered">Purposes</header>
                            <div class="box-typical-inner">
                                <table class="user-page-table">
                                    <? foreach ($user->purposes as $purpose): ?>
                                    <tr>
                                        <td>
                                            <b><?= $purpose->purpose ?></b>
                                        </td>
                                        <td></td>
                                    </tr>
                                    <? endforeach; ?>
                                </table>
                            </div>
                        </section>
                        <? endif; ?>
                        <section class="box-typical">
                            <header class="box-typical-header-sm bordered">Sexual Preferences</header>
                            <div class="box-typical-inner">
                                <table class="user-page-table">
                                    <tr>
                                        <td>
                                            <b>How Often</b>
                                        </td>
                                        <td>
                                            <?= $user->sex_periodicity ?>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td><b>Interesting in: </b></td>

                                        <td>
                                            <ul class="user-params-list">
                                            <? foreach ($user->sexTypes as $type):?>

                                                <li><?= $type->type ?></li>

                                            <? endforeach; ?>
                                            </ul>
                                        </td>

                                    </tr>
                                </table>
                            </div>
                        </section>
                    </aside><!--.profile-side-->
                </div>
                <div class="col-xl-9 col-lg-8 p-t-md">
                    <header class="section-header">
                        <div class="tbl">
                            <div class="tbl-row">
                                <div class="tbl-cell">
                                    <h1><?= $user->getTitle() ?></h1>
                                </div>
                            </div>
                        </div>
                    </header>
					
                    <section class=" box-typical profile-post">
                        <header class="box-typical-header-sm bordered profile-post-header">About me</header>
                        <div class="box-typical-inner profile-post-content">
							<? if ($user->information): ?>
								<p><?= $user->information ?></p>
								<? else: ?>
								<p>The user preferred not to provide information about himself</p>
							<? endif; ?>
                        </div>
                    </section>
					
                    <section class=" box-typical profile-post">
                        <header class="box-typical-header-sm bordered profile-post-header">Gallery</header>
                        <div class="profile-post-content">
                            <div class="row profile-gallery__list">
                                <div class="col-sm-6 col-md-3 col-lg-4 gallery-item"
                                    v-for="(photo,index) in photos"
                                    :key="photo.id"
                                    :data-position="photo.order"
                                    :data-id="photo.id"
                                    >
                                    <a :href="photo.filename" class="box-typical gallery-item__image" v-bind:style="{backgroundImage: 'url(' + photo.filename + ')'}">
                                    </a>

                                </div>
                            </div>
                        </div>

                    </section>
                </div>
            </div>
        </div><!--.row-->
    </div>
</div>
