<?php
/**
 * Created by PhpStorm.
 * User: JimmDiGriz
 * Date: 16.03.2017
 * Time: 14:41
 */

namespace app\rbac;

class Permissions
{
    const LOGIN = 'login';
    const VIEW = 'view';
    const UPDATE = 'update';
    const DELETE = 'delete';

    const ADMIN_ACCESS = 'admin-access';
}