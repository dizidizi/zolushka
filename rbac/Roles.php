<?php
/**
 * Created by PhpStorm.
 * User: JimmDiGriz
 * Date: 16.03.2017
 * Time: 14:41
 */

namespace app\rbac;

class Roles
{
    public static $roles = [
        self::ADMIN,
        self::MODERATOR,
        self::REDACTOR,
        self::USER,
        self::BANNED,
    ];
    
    const ADMIN = 'admin';
    const MODERATOR = 'moderator';
    const REDACTOR = 'redactor';
    const USER = 'user';

    const BANNED = 'banned';
}