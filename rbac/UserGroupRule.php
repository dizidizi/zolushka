<?php
/**
 * Created by PhpStorm.
 * User: JimmDiGriz
 * Date: 16.03.2017
 * Time: 14:38
 */

namespace app\rbac;

use yii\rbac\Item;
use yii\rbac\Rule;

/**
 * @property string $name
 */
class UserGroupRule extends Rule
{
    public $name = 'userGroup';

    /**@var string $group*/
    protected $group;

    /**
     * Executes the rule.
     *
     * @param string|int $user the user ID. This should be either an integer or a string representing
     * the unique identifier of a user. See [[\yii\web\User::id]].
     * @param Item $item the role or permission that this rule is associated with
     * @param array $params parameters passed to [[CheckAccessInterface::checkAccess()]].
     *
     * @return bool a value indicating whether the rule permits the auth item it is associated with.
     */
    public function execute($user, $item, $params)
    {
        if (!\Yii::$app->user->isGuest) {
            $this->group = \Yii::$app->user->indentity->group;

            if ($item->name == Roles::ADMIN) {
                return $this->isAdmin();
            }

            if ($item->name == Roles::MODERATOR) {
                return $this->isModerator();
            }

            if ($item->name == Roles::REDACTOR) {
                return $this->isRedactor();
            }

            if ($item->name == Roles::USER) {
                return $this->isUser();
            }
        }

        /**guests can nothing*/
        return false;
    }

    /**
     * @return bool
     */
    protected function isAdmin()
    {
        return $this->group == Roles::ADMIN;
    }

    /**
     * @return bool
     */
    protected function isModerator()
    {
        return $this->isAdmin() || $this->group == Roles::MODERATOR;
    }

    /**
     * @return bool
     */
    protected function isRedactor()
    {
        return $this->isModerator() || $this->group == Roles::REDACTOR;
    }

    /**
     * @return bool
     */
    protected function isUser()
    {
        return $this->isRedactor() || $this->group == Roles::USER;
    }
}