<?php
return [
    'login' => [
        'type' => 2,
    ],
    'admin-access' => [
        'type' => 2,
    ],
    'user' => [
        'type' => 1,
        'ruleName' => 'userGroup',
        'children' => [
            'login',
        ],
    ],
    'redactor' => [
        'type' => 1,
        'ruleName' => 'userGroup',
        'children' => [
            'user',
        ],
    ],
    'moderator' => [
        'type' => 1,
        'ruleName' => 'userGroup',
        'children' => [
            'redactor',
        ],
    ],
    'admin' => [
        'type' => 1,
        'ruleName' => 'userGroup',
        'children' => [
            'redactor',
            'admin-access',
        ],
    ],
    'banned' => [
        'type' => 1,
        'ruleName' => 'userGroup',
    ],
];
