/**
 * Created by JimmDiGriz on 30.04.2017.
 */

'use strict';

module.exports = {
    'port': 8110,
    'io': {
        'origins': '*:*',
        'transports': ['websocket', 'polling', 'xhr-polling'],
        'heartbeat timeout': 300000,
        'heartbeat interval': 30000
    },
    'redis': {
        'host': 'localhost',
        'port': 6379
    },
    'api': {
        'host': 'localhost:8003'
    }
};