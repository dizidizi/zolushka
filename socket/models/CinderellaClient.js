/**
 * Created by JimmDiGriz on 30.04.2017.
 */

'use strict';

let User = require('./User');
let CinderellaApi = require('../modules/cinderella-api');
let _ = require('lodash');

class CinderellaClient {
    constructor() {
        this.user = null;
    }
    
    authenticate(callback) {
        if (_.isNull(this.user)) {
            callback('UserIsNotSet');
            return;
        }

        CinderellaApi.authenticate(this.user,  callback);
    }

    /**
     * @param {function} callback
     **/
    setOnline(callback) {
        if (_.isNull(this.user)) {
            callback('UserIsNotSet');
            return;
        }

        CinderellaApi.toOnline(this.user,  callback);
    }

    /**
     * @param {function} callback
     **/
    setOffline(callback) {
        if (_.isNull(this.user)) {
            callback('UserIsNotSet');
            return;
        }

        CinderellaApi.toOffline(this.user,  callback);
    }
    
    get id() {
        return this.user.id;
    }
}

module.exports = CinderellaClient;