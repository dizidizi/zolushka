/**
 * Created by JimmDiGriz on 30.04.2017.
 */

'use strict';

class CinderellaError extends Error {

}

module.exports = CinderellaError;