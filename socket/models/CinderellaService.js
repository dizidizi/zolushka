/**
 * Created by JimmDiGriz on 30.04.2017.
 */

'use strict';

const _ = require('lodash');
const CinderellaError = require('./CinderellaError');
const logger = require('../modules/cinderella-logger');

class CinderellaService {
    /**
     * @param {Server} io
     **/
    constructor(io) {
        this._io = io;

        this.CONNECTION_EVENT = 'connection';
        this.AUTH_EVENT = 'auth';
        this.ERROR_EVENT = 'error';
        this.DISCONNECT_EVENT = 'disconnect';
        
        this.name = 'CinderellaService';
    }

    /**
     * @param {string} msg
     * @param {Socket} socket
     **/
    sendError(msg, socket) {
        socket.emit(this.ERROR_EVENT, msg);
    }

    /**
     * @param {string} msg
     * @param {Socket} socket
     **/
    processError(msg, socket = null) {
        this.error(msg);

        if (!_.isNil(socket)) {
            sendError(msg,  socket);
        }
    }

    /**
     * @param {Socket} socket
     * @param {Error} error
     **/
    errorHandler(socket, error) {
        if (error instanceof CinderellaError) {
            processError(error.message, socket);
            return;
        }

        throw error;
    }

    /**
     * @param {string} message
     **/
    info(message) {
        logger.info(`[${this.name}]: ${message}`);
    }

    /**
     * @param {string} message
     **/
    debug(message) {
        logger.debug(`[${this.name}]: ${message}`);
    }

    /**
     * @param {string} message
     **/
    error(message) {
        logger.error(`[${this.name}]: ${message}`);
    }
}

module.exports = CinderellaService;