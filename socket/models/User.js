/**
 * Created by JimmDiGriz on 30.04.2017.
 */

'use strict';

let _ = require('lodash');

class User {
    /**
     * @param {String} accessToken
     **/
    constructor(id, accessToken) {
        this.id = id;
        this.accessToken = accessToken;
    }

    validate() {
        let result = true;

        _.forOwn(this, (value) => {
            if (_.isNil(value) || value == '') {
                result = false;
                return false;
            }
        });

        return result;
    }
}

module.exports = User;