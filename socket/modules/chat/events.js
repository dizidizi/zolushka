/**
 * Created by JimmDiGriz on 02.05.2017.
 */

'use strict';

module.exports = {
    AUTH_SUCCESS: 'auth-success',
    SEND_MESSAGE: 'send-message',
    NEW_MESSAGE: 'new-message',
    TYPING: 'typing',
    READ_MESSAGE: 'read-message',
    DELETE_MESSAGE: 'delete-message',
    EDIT_MESSAGE: 'edit-message'
};
