/**
 * Created by JimmDiGriz on 30.04.2017.
 */

'use strict';

const CinderellaApi = require('../cinderella-api');
const CinderellaClient = require('../../models/CinderellaClient');
const User = require('../../models/User');
const _ = require('lodash');
const CinderellaService = require('../../models/CinderellaService');
const CinderellaRequest = require('../../models/CinderellaRequest');
const CinderellaError = require('../../models/CinderellaError');

const Events = require('./events');

class CinderellaChat extends CinderellaService {
    /**
     * @param {Server} io
     **/
    constructor(io) {
        super(io);

        this._io.adapter.clientsMap = new Map();

        this.name = 'CinderellaChat';
    }
    
    getClientById(id) {
        if (this._io.adapter.clientsMap.has(id)) {
            return this._io.adapter.clientsMap.get(id);
        }
        
        return false;
    }

    init() {
        this._io.on(this.CONNECTION_EVENT, (socket) => {
            this.info('Client Connect');

            socket.unique(this.AUTH_EVENT, (data) => { this.onAuth(socket, data); });
            socket.unique(this.DISCONNECT_EVENT, () => { this.onDisconnect(socket); });

            socket.unique(this.ERROR_EVENT, (error) => { this.errorHandler(socket, error); });
            
            this.info('Transport: ' + socket.conn.transport.name);
        });

        return this;
    }
    
    broadcastOnAllConnections(id, event, message) {
        id = id.toString();
        
        if (!this._io.adapter.clientsMap.get(id)) {
            return;
        }
        
        for (let socket of this._io.adapter.clientsMap.get(id)) {
            socket.emit(event, message);
        }
    }

    onAuth(socket, data) {
        this.info('Client Try To Auth');

        this.debug(data);

        data = CinderellaRequest.fromString(data);

        if (!data.validate(['userId', 'accessToken'])) {
            throw new CinderellaError('Wrong Auth Request');
        }

        let client = new CinderellaClient();
        client.user = new User(data.userId, data.accessToken);

        client.authenticate((data) => {
            console.log(data);
            
            data = JSON.parse(data);
            
            if (!data.email) {
                throw new CinderellaError("UserNotAuthenticated");
            }
            client.setOnline((data) => {
                data = JSON.parse(data);
                
                if (!data.email) {
                    throw new CinderellaError(JSON.stringify(data));
                }

                socket.cinderellaClient = client;
                
                if (this._io.adapter.clientsMap.has(client.id)) {
                    this._io.adapter.clientsMap.get(client.id).add(socket);
                    //this.softDisconnect(this._io.adapter.clientsMap.get(client.id));
                } else {
                    this._io.adapter.clientsMap.set(client.user.id, new Set());
                    this._io.adapter.clientsMap.get(client.id).add(socket);
                }

                
                socket.emit(Events.AUTH_SUCCESS);
                
                socket.unique(Events.SEND_MESSAGE, (data) => { this.onChatMessage(socket, data); });
                socket.unique(Events.TYPING, (data) => { this.onTyping(socket, data); });
                socket.unique(Events.READ_MESSAGE, (data) => { this.onReadMessage(socket, data); });
                socket.unique(Events.DELETE_MESSAGE, (data) => { this.onDeleteMessage(socket, data); });
                socket.unique(Events.EDIT_MESSAGE, (data) => { this.onEditMessage(socket, data); });

                this.info('Client Goes Online');
            });
        });
    }

    softDisconnect(socket) {
        this.info('SoftDisconnect Call');

        socket.removeAllListeners(this.DISCONNECT_EVENT, () => { this.onDisconnect(socket); });

        socket.disconnect();
    }

    onDisconnect(socket) {
        if (_.isNil(socket.cinderellaClient)) {
            this.info('Anonymous Client Disconnected');
            return;
        }

        let client = socket.cinderellaClient;

        this.info('Authenticated User Disconnected');

        client.setOffline(() => {
            this.info('Client Goes Offline');
            
            let id = socket.cinderellaClient.id;
            
            for (let s of this._io.adapter.clientsMap.get(id)) {
                delete s.cinderellaClient;
            }

            this._io.adapter.clientsMap.delete(id);
        });
    };
    
    onChatMessage(socket, data) {
        this.info('Client Try To Send Message');

        this.debug(data);

        data = CinderellaRequest.fromString(data, false, ['chatId', 'message', 'senderId', 'userId']);
        
        this.info(JSON.stringify(data));

        if (!data.validate(['chatId', 'message', 'senderId', 'userId'])) {
            throw new CinderellaError('Wrong Message Request');
        }
        
        CinderellaApi.sendMessage(
            socket.cinderellaClient.user, 
            data.chatId, 
            data.message,
            (response) => {
                if (!JSON.parse(response).model) {
                    throw new CinderellaError(response);
                }
                
                console.log(data.userId);
                
                //let participant = this.getClientById(data.userId);
                
                //if (participant) {
                    this.broadcastOnAllConnections(data.userId.toString(), Events.SEND_MESSAGE, response);
                    //participant.emit(Events.SEND_MESSAGE, response);
                //}
                this.broadcastOnAllConnections(socket.cinderellaClient.user.id, Events.SEND_MESSAGE, response);
                //socket.emit(Events.SEND_MESSAGE, response);
            });
    }
    
    onTyping(socket, data) {
        this.info('Client Try To Typing');

        this.debug(data);

        data = CinderellaRequest.fromString(data, false, ['userId', 'senderId']);

        if (!data.validate(['userId', 'senderId'])) {
            throw new CinderellaError('Wrong Typing Request: userId missing');
        }
        
        const userId = data.userId;
        
        this.broadcastOnAllConnections(userId, Events.TYPING, JSON.stringify({
            senderId: data.senderId
        }));
    }
    
    onReadMessage(socket, data) {
        this.info('Client Try To Read Message');

        this.debug(data);

        data = CinderellaRequest.fromString(data, false, ['chatId']);

        if (!data.validate(['chatId'])) {
            throw new CinderellaError('Wrong Reading Request');
        }
        
        CinderellaApi.readMessage(
            socket.cinderellaClient.user,
            data.chatId, (response) => {
                
            });
    }
    
    onDeleteMessage(socket, data) {
        this.info('Client Try To Delete Message');

        this.debug(data);

        data = CinderellaRequest.fromString(data, false, ['messageIds']);

        if (!data.validate(['messageIds'])) {
            throw new CinderellaError('Wrong Deleting Request');
        }
        
        CinderellaApi.deleteMessage(
            socket.cinderellaClient.user,
            data.messageIds,
            (response) => {
                this.debug(response);
                
                response = JSON.parse(response);
                
                if (!response.chatId) {
                    throw new CinderellaError('Deleting Failed');
                }
                
                const userResponse = {
                    chatId: response.chatId,
                    messageIds: data.messageIds
                };
                
                for (let user of response.chatUsers) {
                    this.info('Broadcasting To: ' + user.id);
                    this.broadcastOnAllConnections(user.id.toString(), Events.DELETE_MESSAGE, JSON.stringify(userResponse));
                }
            }
        )
    }
    
    onEditMessage(socket, data) {
        this.info('Client Try To Edit Message');

        this.debug(data);

        data = CinderellaRequest.fromString(data, false, ['messageId', 'text']);

        if (!data.validate(['messageId', 'text'])) {
            throw new CinderellaError('Wrong Editing Request');
        }

        CinderellaApi.editMessage(
            socket.cinderellaClient.user,
            data.messageId,
            data.text,
            (response) => {
                this.debug(response);

                response = JSON.parse(response);
                
                if (!response.model) {
                    throw new CinderellaError('Editing Failed');
                }
                
                const userResponse = {
                    chatId: response.model.chat_id,
                    messageId: response.model.id,
                    message: response.model.message
                };
                
                for (let user of response.users) {
                    this.info('Broadcasting Edit To: ' + user.id);
                    
                    this.broadcastOnAllConnections(user.id.toString(), Events.EDIT_MESSAGE, JSON.stringify(userResponse));
                }
            }
        );
    }
}

module.exports = CinderellaChat;