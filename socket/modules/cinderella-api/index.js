/**
 * Created by JimmDiGriz on 30.04.2017.
 */

'use strict';

const request = require('request');
const User = require('../../models/User');
const config = require('../../config');
const routes = require('./routes');
const CinderellaError = require('../../models/CinderellaError');

//request.debug = true;
//require('request-debug')(request);

class CinderellaApi {
    /**
     * @param {User} user
     * @param {function} callback
     **/
    static authenticate(user, callback) {
        request.post({
            url: CinderellaApi._getUrl(routes.AUTH),
            form: CinderellaApi._getForm('id', user.id, 'accessToken', user.accessToken)
        }, (error, response, data) => {
            if (error) {
                throw new CinderellaError(data);
            }
            
            callback(data);
        });
    }
    
    /**
     * @param {User} user
     * @param {function} callback
     **/
    static toOnline(user, callback) {
        request.put({
            url: CinderellaApi._getUrl(routes.ONLINE),
            headers: {'content-type': 'application/x-www-form-urlencoded'},
            form: CinderellaApi._getForm('userId', user.id)
        }, (error, response, data) => {
            if (error) {
                throw new CinderellaError(data);
            }
            
            callback(data);
        });
    }

    /**
     * @param {User} user
     * @param {function} callback
     **/
    static toOffline(user, callback) {
        request.put({
            url: CinderellaApi._getUrl(routes.OFFLINE),
            headers: {'content-type': 'application/x-www-form-urlencoded'},
            form: CinderellaApi._getForm('userId', user.id)
        }, (error, response, data) => {
            if (error) {
                throw new CinderellaError(data);
            }
            
            callback(data);
        });
    }

    /**
     * @param {int} id
     * @param {function} callback
     **/
    static getUserById(id, callback) {
        request.get({
            url: CinderellaApi._getUrl(routes.USER),
            qs: CinderellaApi._getForm('id', id)
        }, (error, response, data) => {
            if (error) {
                throw new CinderellaError(data);
            }
            
            callback(data);
        });
    }

    /**
     * @param {User} user
     * @param {int} chatId
     * @param {string} message
     * @param {function} callback
     **/
    static sendMessage(user, chatId, message, callback) {
        request.post({
            url: CinderellaApi._getUrl(routes.SEND_MESSAGE),
            form: CinderellaApi._getForm('senderId', user.id, 'chatId', chatId, 'message', message)
        }, (error,  response, data) => {
            if (error) {
                throw new CinderellaError(data);
            }
    
            callback(data);
        });
    }

    /**
     * @param {User} user
     * @param {int} messageId
     * @param {string} message
     * @param {function} callback
     **/
    static editMessage(user, messageId, message, callback) {
        request.put({
            url: CinderellaApi._getUrl(routes.EDIT_MESSAGE),
            headers: {'content-type': 'application/x-www-form-urlencoded'},
            form: CinderellaApi._getForm('userId', user.id, 'messageId', messageId, 'message', message)
        }, (error,  response, data) => {
            if (error) {
                throw new CinderellaError(data);
            }

            callback(data);
        });
    }

    /**
     * @param {User} user
     * @param {int} chatId
     * @param {function} callback
     **/
    static readMessage(user, chatId, callback) {
        request.get({
            url: CinderellaApi._getUrl(routes.READ_MESSAGE),
            qs: CinderellaApi._getForm('userId', user.id, 'chatId', chatId)
        }, (error,  response, data) => {
            if (error) {
                throw new CinderellaError(data);
            }

            callback(data);
        });
    }

    /**
     * @param {User} user
     * @param {int[]} messageIds
     * @param {function} callback
     **/
    static deleteMessage(user, messageIds, callback) {
        request.delete({
            url: CinderellaApi._getUrl(routes.DELETE_MESSAGE),
            qs: CinderellaApi._getForm('userId', user.id, 'messageIds', messageIds)
        }, (error,  response, data) => {
            if (error) {
                throw new CinderellaError(data);
            }

            callback(data);
        });
    }

    /**
     * @param {String} route
     *
     * @return {String}
     **/
    static _getUrl(route) {
        return `http://${config.api.host}/${route}/`;
    }

    /**
     * @return {object}
     **/
    static _getForm() {
        let form = {};

        for (let i = 0; i < arguments.length; i++) {
            if (i % 2 != 0) {
                form[arguments[i - 1]] = arguments[i];
            }
        }

        return form;
    }
}

module.exports = CinderellaApi;