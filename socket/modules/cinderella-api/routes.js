/**
 * Created by JimmDiGriz on 30.04.2017.
 */

module.exports = {
    AUTH: 'users/api/auth',
    ONLINE: 'users/api/online',
    OFFLINE: 'users/api/offline',
    USER: 'users/api/view',
    SEND_MESSAGE: 'chat/api/send',
    READ_MESSAGE: 'chat/api/read',
    EDIT_MESSAGE: 'chat/api/edit',
    DELETE_MESSAGE: 'chat/api/delete-messages',
    CHATS: 'chat/api/chats',
    MESSAGES: 'chat/api/messages'
};
