/**
 * Created by JimmDiGriz on 30.04.2017.
 */

'use strict';

const CinderellaApi = require('../cinderella-api');
const CinderellaClient = require('../../models/CinderellaClient');
const User = require('../../models/User');
const _ = require('lodash');
const CinderellaService = require('../../models/CinderellaService');
const CinderellaRequest = require('../../models/CinderellaRequest');
const CinderellaError = require('../../models/CinderellaError');

class OnlineStatusService extends CinderellaService {
    /**
     * @param {Server} io
     **/
    constructor(io) {
        super(io);
        
        this._io.adapter.clientsMap = new Map();
        
        this.name = 'OnlineStatusService';
    }

    init() {
        this._io.on(this.CONNECTION_EVENT, (socket) => {
            this.info('Client Connect');

            socket.unique(this.AUTH_EVENT, (data) => { this.onAuth(socket, data); });
            socket.unique(this.DISCONNECT_EVENT, () => { this.onDisconnect(socket); });

            socket.unique(this.ERROR_EVENT, (error) => { this.errorHandler(socket, error); });
        });

        return this;
    }
    
    onAuth(socket, data) {
        this.info('Client Try To Auth');

        this.debug(data);

        data = CinderellaRequest.fromString(data);

        if (!data.validate(['id', 'accessToken'])) {
            throw new CinderellaError('Wrong Auth Request');
        }

        let client = new CinderellaClient();
        client.user = new User(data.id, data.accessToken);

        client.authenticate(() => {
            client.setOnline(() => {
                if (this._io.adapter.clientsMap.has(client.id)) {
                    this.softDisconnect(this._io.adapter.clientsMap.get(client.id));
                }
                
                socket.cinderellaClient = client;
                
                this._io.adapter.clientsMap.set(client.user.id, socket);
                
                this.info('Client Goes Online');
            });
        });
    }

    softDisconnect(socket) {
        this.info('SoftDisconnect Call');

        socket.removeAllListeners(this.DISCONNECT_EVENT, () => { this.onDisconnect(socket); });

        socket.disconnect();
    }
    
    onDisconnect(socket) {
        if (_.isNil(socket.cinderellaClient)) {
            this.info('Anonymous Client Disconnected');
            return;
        }
        
        let client = socket.cinderellaClient;
        
        this.info('Authenticated User Disconnected');
        
        client.setOffline(() => {
            this.info('Client Goes Offline');

            this._io.adapter.clientsMap.delete(socket.cinderellaClient.id);
        
            delete socket.cinderellaClient;
        });
    };
}

module.exports = OnlineStatusService;