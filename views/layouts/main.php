<?php
/**
 * Created by PhpStorm.
 * User: JimmDiGriz
 * Date: 3/18/2017
 * Time: 5:47 PM
 */

use app\assets\FrontendAsset;
use yii\helpers\Html;
use yii\helpers\Url;

FrontendAsset::register($this);

$this->title = 'Cinderella';

?>

<?php $this->beginPage() ?>
<!DOCTYPE html>
<html lang="<?= Yii::$app->language ?>">
<head>
    <meta charset="<?= Yii::$app->charset ?>">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <?= Html::csrfMetaTags() ?>
    <title><?= Html::encode($this->title) ?></title>
    <?php $this->head() ?>
</head>
<body class="without-left-menu theme-picton-blue-white-ebony chrome-browser">
<?php $this->beginBody() ?>
<div id="mainApp">
<notify ref="chatNotify"></notify>
<header class="site-header">
    <div class="container-fluid">
        <a href="<?= Url::to(['/users/default/index']) ?>" class="site-logo">
            <span style="font-size: 18pt; color: white; font-weight: bold;">Cinderella</span>
        </a>
        <button class="hamburger hamburger--htla">
            <span>toggle menu</span>
        </button>
        <div class="site-header-content">
            <div class="site-header-content-in">
                <div class="mobile-menu-right-overlay"></div>
                    <div class="site-header-shown">

                    <? if (!Yii::$app->user->isGuest): ?>
                        <input type="hidden" name="accessToken" id="accessToken" value="<?= Yii::$app->user->identity->getAuthKey() ?>">
                        <input type="hidden" name="currentUserId" id="currentUserId" value="<?= Yii::$app->user->id ?>">
                        <input type="hidden" name="currentUserName" id="currentUserName" value="<?= Yii::$app->user->identity->name ?>">
                        <input type="hidden" name="currentUserAvatar" id="currentUserAvatar" value="<?= \app\core\components\AvatarManager::getAvatarForUser(Yii::$app->user->identity) ?>">
							<? if (Yii::$app->user->identity->group == \app\rbac\Roles::ADMIN): ?>
								<div class="dropdown dropdown-notification messages" style="margin-top: 5px; margin-right: 20px;">
									<a href="<?= Url::to(['/admin/default/index']) ?>">Administrate</a>
								</div>
							<? endif; ?>
							<div class="dropdown dropdown-notification messages">
                                <a href="#"
                                   class="header-alarm dropdown-toggle "
                                   id="dd-messages"

                                   aria-haspopup="true"
                                   aria-expanded="false"
                                   data-toggle="modal"
                                   data-target="#chat"
                                   @click="createChat">
                                    <i class="font-icon-mail"></i>
                                </a>
                            </div>
                            <div class="dropdown user-menu">
                                <button class="dropdown-toggle" id="dd-user-menu" type="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                    <img src="<?= \app\core\components\AvatarManager::getAvatarForUser(Yii::$app->user->identity) ?>" alt="">
                                </button>
                                <div class="dropdown-menu dropdown-menu-right" aria-labelledby="dd-user-menu">
                                    <a class="dropdown-item" href="<?= Url::to(['/users/default/view', 'userId' => \Yii::$app->user->identity->id])?>"><span class="font-icon glyphicon glyphicon-user"></span>Profile</a>
                                    <a class="dropdown-item" href="<?= Url::to(['/users/default/settings'])?>"><span class="font-icon glyphicon glyphicon-cog"></span>Settings</a>
                                    <div class="dropdown-divider"></div>
                                    <a class="dropdown-item" href="<?= Url::to(['/users/default/logout']) ?>"><span class="font-icon glyphicon glyphicon-log-out"></span>Logout</a>
                                </div>
                            </div>
							
                    <? else: ?>
                        <a href="<?= Url::to(['/users/default/login']) ?>">Login</a>
                        <a href="<?= Url::to(['/users/default/register']) ?>">Register</a>
                    <? endif; ?>
                </div>
                
                <div class="site-header-collapsed">
                    <div class="site-header-collapsed-in">
                    </div><!--.site-header-collapsed-in-->
                </div><!--.site-header-collapsed-->
            </div><!--site-header-content-in-->
        </div><!--.site-header-content-->
    </div><!--.container-fluid-->
</header><!--.site-header-->

<!-- Chat modal -->

<div class="modal fade"
     id="chat"
     tabindex="-1"
     role="dialog"
     aria-labelledby="Chat window"
     aria-hidden="true">
    <div class="modal-dialog modal-lg chat-modal" role="document">
        <div class="modal-content">
            <button type="button" class="modal-close" @click="closeChat" data-dismiss="modal" aria-label="Close">
                <i class="font-icon-close-2"></i>
            </button>
            <div class="modal-body" id="main-upload" >
               <chat ref="mainChat"></chat>
            </div>
        </div>
    </div>
</div>

<!-- Chat modal END -->


<!--<div class="mobile-menu-left-overlay"></div>-->
<!--<nav class="side-menu">-->
<!--    <ul class="side-menu-list">-->
<!--        <li class="blue">-->
<!--            <a href="--><?//= \yii\helpers\Url::to(['/admin/default/index']) ?><!--">-->
<!--                <i class="font-icon font-icon-user"></i>-->
<!--                <span class="lbl">Users</span>-->
<!--            </a>-->
<!--        </li>-->
<!--    </ul>-->
<!--</nav><!--.side-menu-->

<div class="page-content">
    <?= $content ?>
</div>

</div> <!-- #mainApp -->

<?php $this->endBody() ?>
<script>
		$(document).ready(function() {
			$('.panel').lobiPanel({
				sortable: true
			});
			$('.panel').on('dragged.lobiPanel', function(ev, lobiPanel){
				$('.dahsboard-column').matchHeight();
			});

			/* User page scripts */

			$(".fancybox").fancybox({
				padding: 0,
				openEffect	: 'none',
				closeEffect	: 'none'
			});
		});
</script>
</body>
</html>
<?php $this->endPage() ?>


