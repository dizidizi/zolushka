import Request from '../../../core/Request'

export default {
    methods: {
        /**
         * @param {int} id
         * @param {function} callback
         * */
        getUserFromApi: function(id, callback) {
            let xhr = new XMLHttpRequest();
            let self = this;

            let query = 'id=' + id + '&expand=avatar';

            xhr.open('GET', '/users/api/view?' + query);
            xhr.setRequestHeader('content-Type', 'application/json');

            xhr.onload = function() {
                if (xhr.status === 200) {
                    let data = JSON.parse(xhr.responseText);
                    console.log('User Get ' + data);

                    callback(data);
                }

                return null;
            };

            xhr.send();
        },
        /**
         * @param {function} callback
         **/
        createChatByApi: function(callback = null) {
            let xhr = new XMLHttpRequest();
            let self = this;

            let body = 'userId=' + this.currentUserId;

            console.log('Creating Chat With: ' + this.currentUserId);

            xhr.open('POST', '/chat/api/create-chat/');

            xhr.setRequestHeader('content-Type', 'application/x-www-form-urlencoded');

            console.log('CurrentUserId: ' + this.curentUserId);

            xhr.onload = () => {
                if (xhr.status === 200) {
                    console.log(data);
                    let data = JSON.parse(xhr.responseText);

                    if (callback) {
                        callback(data);
                    }

                    this.getChats();
                }

                if (xhr.status === 403) {
                    console.log('ChatCreation: PermissionDenied; ChatAlreadyExists');
                }

                if (xhr.status === 500 || xhr.status === 400) {
                    console.log(xhr.responseText);
                }
            };

            xhr.send(body);
        },
        getChats: function() {
            /**@todo: add scrollable behavior*/
            let xhr = new XMLHttpRequest();
            let self = this;

            xhr.open('GET', '/chat/api/chats?expand=users,unread,folders');
            xhr.setRequestHeader('content-Type', 'application/json');

            xhr.onload = function() {
                if (xhr.status === 200) {
                    let data = JSON.parse(xhr.responseText);
                    console.log('Chats data ' + data);

                    let invalidParticipant = {
                        user_id: 0,
                        chat_id: 0,
                        user_name: 'Anonymous',
                        online: 'never',
                        gender: 'male',
                        avatar: self.default_male_avatar,
                        last_seen: 'never',
                        typing: false,
                        folder_id: [0],
                        unread_count: 0,
                        is_selected: false,
                        comment: '',
                        is_real: false,
                        typing_timeout: null
                    };

                    self.chats = data.map((chat) => {
                        let participant = invalidParticipant;

                        for (let userInChat of chat.users) {
                            if (userInChat.id != window.currentUserId) {
                                participant = userInChat;
                                break;
                            }
                        }

                        if (participant.user_id == 0) {
                            return participant;
                        }

                        return {
                            user_id: participant.id,
                            chat_id: chat.id,
                            user_name: participant.name,
                            online: participant.is_online,
                            gender: participant.gender,
                            avatar: participant.avatar_src ? participant.avatar_src :
                                participant.gender == 'male' ? self.default_male_avatar : self.default_female_avatar,
                            last_seen: participant.updated_at,
                            typing: false,
                            folder_id: chat.folders.length != 0 
                                ? chat.folders.map(folder => folder.id)
                                :[0],
                            unread_count: chat.unread,
                            is_selected: false,
                            comment: '',
                            is_real: true,
                            typing_timeout: null
                        };
                    });
                }

                if (xhr.status === 404) {
                    console.log('ChatsNotFound');
                }

                if (xhr.status === 500 || xhr.status === 400) {
                    console.log(xhr.responseText);
                }
            };

            xhr.send();
        },
        /**
         * Recieves messages for chat
         *
         * @param {int} chatId
         **/
        getMessages: function(chatId) {
            /**@todo: add scrollable behavior*/
            console.log('GetMessages Call');

            if (this.messages[chatId]) {
                //contains messages
                this.currentMessages = this.messages[chatId];
                return;
            }

            if (chatId == -1) {
                this.currentMessages = [];
                return;
            }

            let xhr = new XMLHttpRequest();
            let self = this;

            xhr.open('GET', '/chat/api/messages?expand=user,user.avatar&chatId=' + chatId);
            xhr.setRequestHeader('content-Type', 'application/json');

            xhr.onload = function() {
                if (xhr.status === 200) {
                    let data = JSON.parse(xhr.responseText);
                    console.log(data);

                    if (!self.messages[chatId]) {
                        self.messages[chatId] = [];
                    }

                    self.messages[chatId] = self.messages[chatId].concat(data.map((message) => {
                        let result = {
                            id: message.id,
                            user_id: message.user_id,
                            user_name: message.user.name,
                            created_at: message.created_at,
                            text: message.message,
                            status: message.status,
                            avatar: message.user.avatar_src ? message.user.avatar_src :
                                message.user.gender == 'male' ? self.default_male_avatar : self.default_female_avatar,
                        };

                        return result;
                    })).reverse();

                    self.currentMessages = self.messages[chatId];
                }

                if (xhr.status === 404) {
                    console.log('ChatsNotFound');
                }

                if (xhr.status === 500 || xhr.status === 400) {
                    console.log(xhr.responseText);
                }
            };

            xhr.send();
        },
        getAvatarForUser: function(userId, callback = null) {
            console.log('GetAvatar Call');

            let xhr = new XMLHttpRequest();
            let self = this;

            xhr.open('GET', '/users/api/avatar?userId' + userId);
            xhr.setRequestHeader('content-Type', 'application/json');

            xhr.onload = function() {
                if (xhr.status === 200) {
                    let data = JSON.parse(xhr.responseText);
                    console.log(data);

                    for (let chat of self.chats) {
                        if (chat.user_id == userId) {
                            chat.avatar = data.avatar;
                            chat.is_avatar_getted = true;

                            for (let message of self.messages[chat.chat_id]) {
                                if (message.user_id == userId) {
                                    message.avatar = data.avatar;
                                }
                            }
                        }
                    }

                    self.currentMessages = self.messages[self.currentChat.chat_id];
                }
            };

            xhr.send();
        },
        /**
         * @param {int} userId
         * @param {function} callback
         **/
        getCommentForUser: function(userId, callback = null) {
            console.log('GetCommentForUser Call');

            let xhr = new XMLHttpRequest();
            let self = this;

            xhr.open('GET', '/users/api/comments?userId' + userId);
            xhr.setRequestHeader('content-Type', 'application/json');

            xhr.onload = function() {
                if (xhr.status === 200) {
                    let data = JSON.parse(xhr.responseText);
                    console.log(data);

                    for (let chat of self.chats) {
                        if (chat.user_id == data[0].to_user) {
                            chat.comment = data[0].comment;
                        }
                    }

                    if (data[0].to_user == self.currentChat.user_id) {
                        self.currentChat.comment = data[0].comment;
                    }

                    self.rechargeTooltip.call(self);

                    if (callback) {
                        callback(data);
                    }
                }
            };

            xhr.send();
        },
        /**
         * @param {int} userId
         * @param {string} comment
         * @param {function} callback
         **/
        createCommentToUser: function(userId, comment, callback = null) {
            console.log('CreateCommentToUser Call');

            let xhr = new XMLHttpRequest();
            let self = this;

            const body = 'user_id=' + userId + '&message=' + comment;

            xhr.open('POST', '/users/api/comment/');
            xhr.setRequestHeader('content-Type', 'application/x-www-form-urlencoded');

            xhr.onload = function() {
                if (xhr.status === 200) {
                    let data = JSON.parse(xhr.responseText);
                    console.log(data);

                    for (let chat of self.chats) {
                        if (chat.user_id == userId) {
                            chat.comment = comment;
                        }
                    }

                    if (userId == self.currentChat.user_id) {
                        self.currentChat.comment = data.comment;
                    }

                    self.rechargeTooltip.call(self);
                }
            };

            xhr.send(body);
        },
        getFolders: function() {
            const request = new Request();
            
            request.method = request.Method.GET;
            request.route = 'chat/api/folders';
            request.send((response) => {
                console.log(response);
                
                if (response.length == 0) {
                    console.log('FoldersNotFound');
                    return;
                }
                
                this.folder_options = response.map((folder) => {
                    return {
                        id: folder.id,
                        text: folder.name,
                        is_default: false
                    };
                });
            });
        },
        createFolder: function(name) {
            const request = new Request();
            
            request.method = request.Method.POST;
            request.route = 'chat/api/create-folder';
            request.add('name', name);
            
            request.send((response) => {
                
            }, (error) => {
                
            });
        },
        editFolder: function(id, name) {
            const request = new Request();

            request.method = request.Method.PUT;
            request.route = 'chat/api/edit-folder';
            request.add('id', id);
            request.add('name', name);

            request.send((response) => {

            }, (error) => {

            });
        },
        deleteFolder: function(id) {
            const request = new Request();

            request.method = request.Method.DELETE;
            request.route = 'chat/api/delete-folder';
            request.add('folderId', id);

            request.send((response) => {

            }, (error) => {

            });
        },
        addToFolder: function(folderId, chatId) {
            const request = new Request();

            request.method = request.Method.POST;
            request.route = 'chat/api/add-to-folder';
            request.add('folder_id', folderId);
            request.add('chat_id', chatId);

            request.send((response) => {

            }, (error) => {

            });
        },
        removeFromFolder: function(folderId, chatId) {
            const request = new Request();

            request.method = request.Method.DELETE;
            request.route = 'chat/api/remove-from-folder';
            request.add('folderId', folderId);
            request.add('chatId', chatId);

            request.send((response) => {

            }, (error) => {

            });
        }
    }
};