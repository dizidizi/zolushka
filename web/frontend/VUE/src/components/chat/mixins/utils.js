export default {
    methods: {
        /**
         * Temp method.
         * Return user name by user id.
         *
         * @param {int} userId
         *
         * @return {string}
         **/
        getUserNameById: function(userId) {
            if (userId == window.currentUserId) {
                return window.currentUserName;
            }

            for (let user of this.chats) {
                if (user.user_id == userId) {
                    return user.user_name;
                }
            }

            return 'Anonymous';
        },
        /**
         * Return chat by user id.
         * In case of development issue - multiple cahts with same user - methid returns first founded chat.
         *
         * @param {int} userId
         *
         * @return {Object}
         **/
        getChatByUserId: function(userId) {
            for (let chat of this.chats) {
                if (chat.user_id == userId) {
                    return chat;
                }
            }

            return null;
        },
        /**
         * @param {int} chatId
         *
         * @return {int}
         **/
        getChatIndexById: function(chatId) {
            for (let i = 0; i < this.chats.length; i++) {
                if (this.chats[i].chat_id == chatId) {
                    return i;
                }
            }

            return -1;
        },
        /**
         * Format date getted from server
         *
         * @todo: make it computed
         *
         * @param {string} date
         * */
        getFormattedDateFor: function(date) {
            date = new Date(date);

            //console.log(date);

            return `${date.getUTCHours()}:${date.getUTCMinutes()}`;
        },
        getMyAvatar: function() {
            return window.currentUserAvatar;
        }
    }
};