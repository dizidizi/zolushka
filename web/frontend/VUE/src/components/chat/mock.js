export default {
    currentMessages: [
        {
            avatar: '/frontend/img/avatar-1-298.jpg',
            created_at: '2017-05-12 12:52:24',
            status: 2,
            text: 'Cho kak dela?',
            user_id: 4125,
            user_name: 'admin'
        },
        {
            avatar: '/frontend/img/avatar-1-298.jpg',
            created_at: '2017-05-12 12:52:24',
            status: 2,
            text: 'Cho kak dela?',
            user_id: 4125,
            user_name: 'admin'
        },
        {
            avatar: '/frontend/img/avatar-1-298.jpg',
            created_at: '2017-05-12 12:52:24',
            status: 2,
            text: 'Cho kak dela?',
            user_id: 4125,
            user_name: 'admin'
        },
        {
            avatar: '/frontend/img/avatar-1-298.jpg',
            created_at: '2017-05-12 12:52:24',
            status: 2,
            text: 'Cho kak dela?',
            user_id: 4125,
            user_name: 'admin'
        },
        {
            avatar: '/frontend/img/avatar-1-298.jpg',
            created_at: '2017-05-12 12:52:24',
            status: 2,
            text: 'Cho kak dela?',
            user_id: 4125,
            user_name: 'admin'
        },
        {
            avatar: '/frontend/img/avatar-1-298.jpg',
            created_at: '2017-05-12 12:52:24',
            status: 2,
            text: 'Cho kak dela?',
            user_id: 4125,
            user_name: 'admin'
        },
        {
            avatar: '/frontend/img/avatar-1-298.jpg',
            created_at: '2017-05-12 12:52:24',
            status: 2,
            text: 'Cho kak dela?',
            user_id: 4125,
            user_name: 'admin'
        },
        {
            avatar: '/frontend/img/avatar-1-298.jpg',
            created_at: '2017-05-12 12:52:24',
            status: 2,
            text: 'Cho kak dela?',
            user_id: 4125,
            user_name: 'admin'
        },
        {
            avatar: '/frontend/img/avatar-1-298.jpg',
            created_at: '2017-05-12 12:52:24',
            status: 2,
            text: 'Cho kak dela?',
            user_id: 4125,
            user_name: 'admin'
        }
    ],
    folder_options: [
        {
            id: 0,
            text: 'All',
            is_default: true
        }//,
        //{
        //    id: 2,
        //    text: 'Favorites',
        //    is_default: true
        //},
        //{
        //    id: 3,
        //    text: 'Commented',
        //    is_default: true
        //},
        //{
        //    id: 4,
        //    text: 'Blocked',
        //    is_default: true
        //},
        //{
        //    id: 5,
        //    text: 'With blue eyes',
        //    is_default: false
        //}
    ],
    chats: [
        {
            user_id: 1,
            chat_id: 1,
            user_name: 'Anonymous',
            online: 'never',
            gender: 'male',
            avatar: '/frontend/img/avatar-1-298.jpg',
            last_seen: 'never',
            typing: false,
            folder_id: [1,2],
            unread_count: 0,
            is_selected: false,
            comment: ''
        },
        {
            user_id: 2,
            chat_id: 2,
            user_name: 'Anonymous 2',
            online: 'never',
            gender: 'male',
            avatar: '/frontend/img/avatar-1-298.jpg',
            last_seen: 'never',
            typing: false,
            folder_id: [1],
            unread_count: 0,
            is_selected: false,
            comment: ''
        },
        {
            user_id: 3,
            chat_id: 3,
            user_name: 'Anonymous 2',
            online: 'never',
            gender: 'male',
            avatar: '/frontend/img/avatar-1-298.jpg',
            last_seen: 'never',
            typing: false,
            folder_id: [1],
            unread_count: 0,
            is_selected: false,
            comment: ''
        },
        {
            user_id: 4,
            chat_id: 4,
            user_name: 'Anonymous 2',
            online: 'never',
            gender: 'male',
            avatar: '/frontend/img/avatar-1-298.jpg',
            last_seen: 'never',
            typing: false,
            folder_id: [1],
            unread_count: 0,
            is_selected: false,
            comment: ''
        },
        {
            user_id: 5,
            chat_id: 5,
            user_name: 'Anonymous 2',
            online: 'never',
            gender: 'male',
            avatar: '/frontend/img/avatar-1-298.jpg',
            last_seen: 'never',
            typing: false,
            folder_id: [1],
            unread_count: 0,
            is_selected: false,
            comment: ''
        },
        {
            user_id: 6,
            chat_id: 6,
            user_name: 'Anonymous 2',
            online: 'never',
            gender: 'male',
            avatar: '/frontend/img/avatar-1-298.jpg',
            last_seen: 'never',
            typing: false,
            folder_id: [1],
            unread_count: 0,
            is_selected: false,
            comment: ''
        },
        {
            user_id: 7,
            chat_id: 7,
            user_name: 'Anonymous 2',
            online: 'never',
            gender: 'male',
            avatar: '/frontend/img/avatar-1-298.jpg',
            last_seen: 'never',
            typing: false,
            folder_id: [1],
            unread_count: 0,
            is_selected: false,
            comment: ''
        },
        {
            user_id: 8,
            chat_id: 8,
            user_name: 'Anonymous 2',
            online: 'never',
            gender: 'male',
            avatar: '/frontend/img/avatar-1-298.jpg',
            last_seen: 'never',
            typing: false,
            folder_id: [1],
            unread_count: 0,
            is_selected: false,
            comment: ''
        },
        {
            user_id: 9,
            chat_id: 9,
            user_name: 'Anonymous 2',
            online: 'never',
            gender: 'male',
            avatar: '/frontend/img/avatar-1-298.jpg',
            last_seen: 'never',
            typing: false,
            folder_id: [1],
            unread_count: 0,
            is_selected: false,
            comment: ''
        },
        {
            user_id: 10,
            chat_id: 10,
            user_name: 'Anonymous 2',
            online: 'never',
            gender: 'male',
            avatar: '/frontend/img/avatar-1-298.jpg',
            last_seen: 'never',
            typing: false,
            folder_id: [1],
            unread_count: 0,
            is_selected: false,
            comment: ''
        }
    ],
};