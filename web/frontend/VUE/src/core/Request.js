/**
 * Created by JimmDiGriz on 4/1/2017.
 */

class Request {
    constructor() {
        this.Verbs = {
            GET: 'GET',
            POST: 'POST',
            PUT: 'PUT',
            PATCH: 'PATCH',
            DELETE: 'DELETE'
        };
        
        this.Method = {
            GET: {
                'verb': 'GET',
                'type': 'application/json'
            },
            POST: {
                'verb': 'POST',
                'type': 'application/x-www-form-urlencoded'
            },
            PUT: {
                'verb': 'PUT',
                'type': 'application/x-www-form-urlencoded'
            },
            PATCH: {
                'verb': 'PATCH',
                'type': 'application/x-www-form-urlencoded'
            },
            DELETE: {
                'verb': 'DELETE',
                'type': 'application/json'
            }
        };
        
        this._url = window.host;
        this._route = '';
        //this._port = 80;
        this._method = this.Method.GET;
        this._payload = [];
        this._headers = [];
    }
    
    get url() {
        return this._url + '/' + this._route;
    }
    
    set url(value) {
        this._url = value;
    }
    
    set route(value) {
        this._route = value;
    }
    
    //get port() {
    //    return this._port;
    //}
    
    //set port(value) {
    //    this._port = value;
    //}
    
    get method() {
        return this._method;
    }
    
    set method(value) {
        this._method = value;
    }
    
    get payload() {
        return this._payload;
    }
    //
    //set payload(value) {
    //    this._payload = value;
    //}
    
    get headers() {
        return this._headers;
    }
    
    /**
     * @param {string} name
     * @param {mixed} value
     * 
     * @return {Request}
     **/
    add(name, value) {
        this._payload[name] = value;
        
        return this;
    }

    /**
     * @param {string} name
     * @param {mixed} value
     * 
     * @return {Request}
     **/
    header(name, value) {
        this._headers[name] = value;
        
        return this;
    }
    
    /**
     * @param {function} success
     * @param {function} error
     **/
    send(success, error) {
        let req = new XMLHttpRequest();
        let body = null;

        if (this._isQueryMethod()) {
            this.url = this._query();
        } else {
            body = this._query();
        }

        req.open(this.method.verb, this.url);
        
        this._addContentTypeHeader();
        
        for (let name in this.headers) {
            if (this.headers.hasOwnProperty(name)) {
                req.setRequestHeader(name, this.headers[name]);
            }
        }

        req.onload = () => {
            let response = JSON.parse(req.responseText);
            
            if (req.status === 200) {
                if (success) {
                    success(response);
                }
            } else {
                if (error) {
                    error(response);
                }
            }
        };
        
        console.log('Sending To: ' + this.url);

        if (body) {
            req.send(body);
        } else {
            req.send();
        }
    }
    
    /**
     * @return {string}
     **/
    _query() {
        if (this.payload.length == 0) {
            return '';
        }
        
        let query = '?';
        
        for (let key in this.payload) {
            if (this.payload.hasOwnProperty(key)) {
                query += `${key}=${this.payload[key]}&`;
            }
        }
        
        return query;
    }
    
    /**
     * @return {bool}
     **/
    _isQueryMethod() {
        return this.method.verb == this.Verbs.GET 
            || this.method.verb == this.Verbs.DELETE;
    }
    
    _addContentTypeHeader() {
        this.header('content-Type', this.method.type);
    }
}

export default Request;