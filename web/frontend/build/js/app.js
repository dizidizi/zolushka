'use strict';

/* Initialize Wisywig */

var editor = new MediumEditor('.editable');

$(document).ready(function () {

    /* ==========================================================================
     Header mobile menu
     ========================================================================== */

    // Dropdowns
    $('.site-header-collapsed .dropdown').each(function () {
        var parent = $(this),
            btn = parent.find('.dropdown-toggle');

        btn.click(function () {
            if (parent.hasClass('mobile-opened')) {
                parent.removeClass('mobile-opened');
            } else {
                parent.addClass('mobile-opened');
            }
        });
    });

    $('.dropdown-more').each(function () {
        var parent = $(this),
            more = parent.find('.dropdown-more-caption'),
            classOpen = 'opened';

        more.click(function () {
            if (parent.hasClass(classOpen)) {
                parent.removeClass(classOpen);
            } else {
                parent.addClass(classOpen);
            }
        });
    });

    // Left mobile menu
    $('.hamburger').click(function () {
        if ($('body').hasClass('menu-left-opened')) {
            $(this).removeClass('is-active');
            $('body').removeClass('menu-left-opened');
            $('html').css('overflow', 'auto');
        } else {
            $(this).addClass('is-active');
            $('body').addClass('menu-left-opened');
            $('html').css('overflow', 'hidden');
        }
    });

    $('.mobile-menu-left-overlay').click(function () {
        $('.hamburger').removeClass('is-active');
        $('body').removeClass('menu-left-opened');
        $('html').css('overflow', 'auto');
    });

    // Right mobile menu
    $('.site-header .burger-right').click(function () {
        if ($('body').hasClass('menu-right-opened')) {
            $('body').removeClass('menu-right-opened');
            $('html').css('overflow', 'auto');
        } else {
            $('.hamburger').removeClass('is-active');
            $('body').removeClass('menu-left-opened');
            $('body').addClass('menu-right-opened');
            $('html').css('overflow', 'hidden');
        }
    });

    $('.mobile-menu-right-overlay').click(function () {
        $('body').removeClass('menu-right-opened');
        $('html').css('overflow', 'auto');
    });

    /* ==========================================================================
     Header help
     ========================================================================== */

    $('.help-dropdown').each(function () {
        var parent = $(this),
            btn = parent.find('>button'),
            popup = parent.find('.help-dropdown-popup'),
            jscroll;

        btn.click(function () {
            if (parent.hasClass('opened')) {
                parent.removeClass('opened');
                jscroll.destroy();
            } else {
                parent.addClass('opened');

                if (!("ontouchstart" in document.documentElement)) {
                    setTimeout(function () {
                        jscroll = parent.find('.jscroll').jScrollPane(jScrollOptions).data().jsp;
                    }, 0);
                }
            }
        });

        $('html').click(function (event) {
            if (!$(event.target).closest('.help-dropdown-popup').length && !$(event.target).closest('.help-dropdown>button').length && !$(event.target).is('.help-dropdown-popup') && !$(event.target).is('.help-dropdown>button')) {
                if (parent.hasClass('opened')) {
                    parent.removeClass('opened');
                    jscroll.destroy();
                }
            }
        });
    });

    /* ==========================================================================
     Side menu list
     ========================================================================== */

    $('.side-menu-list li.with-sub').each(function () {
        var parent = $(this),
            clickLink = parent.find('>span'),
            subMenu = parent.find('>ul');

        clickLink.click(function () {
            if (parent.hasClass('opened')) {
                parent.removeClass('opened');
                subMenu.slideUp();
                subMenu.find('.opened').removeClass('opened');
            } else {
                if (clickLink.parents('.with-sub').size() == 1) {
                    $('.side-menu-list .opened').removeClass('opened').find('ul').slideUp();
                }
                parent.addClass('opened');
                subMenu.slideDown();
            }
        });
    });

    /* ==========================================================================
     Select
     ========================================================================== */

    if ($('.bootstrap-select').size()) {
        // Bootstrap-select
        $('.bootstrap-select').selectpicker({
            style: '',
            width: '100%',
            size: 8
        });
    }

    if ($('.select2').size()) {
        // Select2
        //$.fn.select2.defaults.set("minimumResultsForSearch", "Infinity");

        $('.select2').not('.manual').select2();

        $(".select2-icon").not('.manual').select2({
            templateSelection: select2Icons,
            templateResult: select2Icons
        });

        $(".select2-arrow").not('.manual').select2({
            theme: "arrow"
        });

        $('.select2-no-search-arrow').select2({
            minimumResultsForSearch: "Infinity",
            theme: "arrow"
        });

        $('.select2-no-search-default').select2({
            minimumResultsForSearch: "Infinity"
        });

        $(".select2-white").not('.manual').select2({
            theme: "white"
        });

        $(".select2-photo").not('.manual').select2({
            templateSelection: select2Photos,
            templateResult: select2Photos
        });
    }

    function select2Icons(state) {
        if (!state.id) {
            return state.text;
        }
        var $state = $('<span class="font-icon ' + state.element.getAttribute('data-icon') + '"></span><span>' + state.text + '</span>');
        return $state;
    }

    function select2Photos(state) {
        if (!state.id) {
            return state.text;
        }
        var $state = $('<span class="user-item"><img src="' + state.element.getAttribute('data-photo') + '"/>' + state.text + '</span>');
        return $state;
    }

    /* ==========================================================================
     Tooltips
     ========================================================================== */

    // Tooltip
    $('[data-toggle="tooltip"]').tooltip({
        html: true
    });

    // Popovers
    $('[data-toggle="popover"]').popover({
        trigger: 'focus'
    });

    /* ==========================================================================
     Full height box
     ========================================================================== */

    function boxFullHeight() {
        var sectionHeader = $('.section-header');
        var sectionHeaderHeight = 0;

        if (sectionHeader.size()) {
            sectionHeaderHeight = parseInt(sectionHeader.height()) + parseInt(sectionHeader.css('padding-bottom'));
        }

        $('.box-typical-full-height').css('min-height', $(window).height() - parseInt($('.page-content').css('padding-top')) - parseInt($('.page-content').css('padding-bottom')) - sectionHeaderHeight - parseInt($('.box-typical-full-height').css('margin-bottom')) - 2);
        $('.box-typical-full-height>.tbl, .box-typical-full-height>.box-typical-center').height(parseInt($('.box-typical-full-height').css('min-height')));
    }

    boxFullHeight();

    $(window).resize(function () {
        boxFullHeight();
    });

    /* ==========================================================================
     Chat
     ========================================================================== */

    function chatHeights() {
        $('.chat-dialog-area, .chat-dialog-area > .vue-scrollbar__wrapper').height($(window).height() - parseInt($('.page-content').css('padding-top')) - parseInt($('.page-content').css('padding-bottom')) - parseInt($('.chat-container').css('margin-bottom')) - 2 - $('.chat-area-header').outerHeight() - $('.chat-area-bottom').outerHeight() - 95);
        $('.chat-list-in, .chat-list-in > .vue-scrollbar__wrapper').height($(window).height() - parseInt($('.page-content').css('padding-top')) - parseInt($('.page-content').css('padding-bottom')) - parseInt($('.chat-container').css('margin-bottom')) - 2 - $('.chat-area-header').outerHeight() - 100).css('min-height', parseInt($('.chat-dialog-area').css('min-height')) + $('.chat-area-bottom').outerHeight());
    }

    chatHeights();

    $(window).resize(function () {
        chatHeights();
    });

    /* ========================================================================== */

    $('.control-panel-toggle').on('click', function () {
        var self = $(this);

        if (self.hasClass('open')) {
            self.removeClass('open');
            $('.control-panel').removeClass('open');
        } else {
            self.addClass('open');
            $('.control-panel').addClass('open');
        }
    });

    $('.control-item-header .icon-toggle, .control-item-header .text').on('click', function () {
        var content = $(this).closest('li').find('.control-item-content');

        if (content.hasClass('open')) {
            content.removeClass('open');
        } else {
            $('.control-item-content.open').removeClass('open');
            content.addClass('open');
        }
    });

    $.browser = {};
    $.browser.chrome = /chrome/.test(navigator.userAgent.toLowerCase());
    $.browser.msie = /msie/.test(navigator.userAgent.toLowerCase());
    $.browser.mozilla = /firefox/.test(navigator.userAgent.toLowerCase());

    if ($.browser.chrome) {
        $('body').addClass('chrome-browser');
    } else if ($.browser.msie) {
        $('body').addClass('msie-browser');
    } else if ($.browser.mozilla) {
        $('body').addClass('mozilla-browser');
    }

    $('#show-hide-sidebar-toggle').on('click', function () {
        if (!$('body').hasClass('sidebar-hidden')) {
            $('body').addClass('sidebar-hidden');
        } else {
            $('body').removeClass('sidebar-hidden');
        }
    });
});