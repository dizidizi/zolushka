'use strict';

/**
 * Created by vanhock on 25.03.2017.
 */
var dropzone = require('components/vue2-dropzone.js');

new Vue({
    el: '#photo-gallery',
    components: {
        dropzone: dropzone
    },
    methods: {
        'showSuccess': function showSuccess(file) {
            console.log('A file was successfully uploaded');
        }
    }
});