'use strict';

var _App = require('./dropzone/App.vue');

var _App2 = _interopRequireDefault(_App);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

new Vue({
    el: '#gallery-upload',
    components: {
        Dropzone: _App2.default
    }
});