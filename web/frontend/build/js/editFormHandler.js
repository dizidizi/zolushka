'use strict';

/**
 * Created by JimmDiGriz on 3/18/2017.
 */

$(document).ready(function () {
    $('#edit-submit').on('click', function () {
        var form = $('#edit-form');

        var data = form.serialize();

        $.ajax({
            type: "POST",
            url: form.attr('action'),
            data: data,
            success: function success(response) {
                if (response == 1) {
                    window.history.back();
                }
            }
        });
    });

    $('#upload-form').on('beforeSubmit', function (e) {}).on('submit', function (e) {
        e.preventDefault();

        var form = $(this);
        var formData = new FormData(this);

        $.ajax({
            url: form.attr("action"),
            type: form.attr("method"),
            dataType: "JSON",
            data: formData,
            processData: false,
            contentType: false,
            success: function success(data) {
                console.log(data);
            },
            error: function error(_error) {
                console.log(_error);
            }
        });

        return false;
    });
});