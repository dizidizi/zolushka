"use strict";

$("#age-slider").ionRangeSlider({
	from: 18,
	to: 35,
	min: 18,
	max: 60,
	type: 'double'
});

$("#weight-slider").ionRangeSlider({
	from: 40,
	to: 70,
	min: 40,
	max: 150,
	type: 'double'
});

$("#height-slider").ionRangeSlider({
	from: 150,
	to: 170,
	min: 140,
	max: 200,
	type: 'double'
});

var advancedSearch = $('.advanced-search'),
    searchToggle = $('.search-form__toggle');

searchToggle.click(function () {
	advancedSearch.slideToggle("fast");
});