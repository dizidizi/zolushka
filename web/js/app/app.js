
new Vue({
	el: '#search-form',
	data: {
          isActive: false
      },

      methods: {
        myFilter: function(){
            this.isActive = !this.isActive;
        }
      }
})