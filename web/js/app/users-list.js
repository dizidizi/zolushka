$(document).ready(function(){
	/**@todo: make adapter, rewrite initSlider to prototype*/
	var ageSlider = $("#age-slider"),
		weightSlider = $("#weight-slider"),
		heightSlider = $("#height-slider");
	
	var ageSliderValue = ageSlider.val(),
		weightSliderValue = weightSlider.val(),
		heightSliderValue = heightSlider.val();
	
	function initSlider(slider, config, value) {
		if (value) {
			if (value.includes(';')) {
				value = value.split(';');
				
				config.from = value[0];
				config.to = value[1];
			} else {
				config.from = config.to = value;
			}
		}
		
		slider.ionRangeSlider(config);
	}
	
	initSlider(ageSlider, {
			from: 18,
			to: 35,
			min: 18,
			max: 60,
			type: 'double'
		}, ageSliderValue);

	initSlider(weightSlider, {
		from: 40,
		to: 70,
		min: 40,
		max: 150,
		type: 'double'
	}, weightSliderValue);

	initSlider(heightSlider, {
		from: 150,
		to: 170,
		min: 140,
		max: 200,
		type: 'double'
	}, heightSliderValue);

    checkAdvForm();
});

var advancedSearch = $('.advanced-search'),
	searchToggle = $('.search-form__toggle'),
	advSearchInputs = advancedSearch.find('input');

searchToggle.click(function(){
	advancedSearch.slideToggle('fast');
    checkAdvForm();
});

function checkAdvForm() {
    if(advancedSearch.css('display') === 'block') {
        advSearchInputs.prop('disabled',false);
    }
    else {
        advSearchInputs.prop('disabled',true);
    }
}

$('.gallery-item__image').swipebox();