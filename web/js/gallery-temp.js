/**
 * Created by JimmDiGriz on 3/26/2017.
 */

$(document).ready(function() {
    return;
    $.get('/photos/api/gallery', {
        userId: $('#user-id').val()
    }, function(response) {
        console.log(response);
        
        initGallery(response);
    });
    
    function initGallery(photos) {
        var links = photos.map(function(photo) {
            return photo.filename;
        });
        
        var config = photos.map(function(photo) {
            return {
                url: '/photos/api/delete/?id=' + photo.id,
                key: photo.order
            };
        });
        
        $("#input-pa").fileinput({
            uploadUrl: "/photos/api/upload/",
            uploadAsync: false,
            minFileCount: 1,
            maxFileCount: 10,
            overwriteInitial: false,
            initialPreview: links,
            initialPreviewAsData: true,
            initialPreviewFileType: 'image',
            initialPreviewConfig: config,
            uploadExtraData: {
                user_id: $('#user-id').val(),
            }
        })
            .on('filesorted', onSort)
            .on('fileuploaded', onUpload);
    }
    
    function onSort(e, params) {
        console.log('File sorted params', params);
    }
    
    function onUpload(e, params) {
        console.log('File uploaded params', params);
    }
});